
GITHASH=$(git rev-parse --short HEAD)

for f in \
  ../apktool.yml \
  ../assets/crashlytics-build.properties \
  ../smali/com/navdy/client/app/framework/AppInstance.smali \
  ../smali/com/navdy/client/BuildConfig.smali \
  ../smali/com/navdy/client/debug/AboutFragment.smali \
  ../smali_classes2/com/navdy/client/app/framework/util/SubmitTicketWorker.smali; do

  sed -i "s/1.3.1718-e615013/1.3.1718-alelec-${GITHASH}/g" "$f"
done
