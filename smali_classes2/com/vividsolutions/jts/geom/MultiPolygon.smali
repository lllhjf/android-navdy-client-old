.class public Lcom/vividsolutions/jts/geom/MultiPolygon;
.super Lcom/vividsolutions/jts/geom/GeometryCollection;
.source "MultiPolygon.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/Polygonal;


# static fields
.field private static final serialVersionUID:J = -0x7a5aa1369171983L


# direct methods
.method public constructor <init>([Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 0
    .param p1, "polygons"    # [Lcom/vividsolutions/jts/geom/Polygon;
    .param p2, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/geom/GeometryCollection;-><init>([Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 88
    return-void
.end method

.method public constructor <init>([Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/PrecisionModel;I)V
    .locals 1
    .param p1, "polygons"    # [Lcom/vividsolutions/jts/geom/Polygon;
    .param p2, "precisionModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;
    .param p3, "SRID"    # I

    .prologue
    .line 72
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0, p2, p3}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;I)V

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/geom/MultiPolygon;-><init>([Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 73
    return-void
.end method


# virtual methods
.method public equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z
    .locals 2
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "tolerance"    # D

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->isEquivalentClass(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 134
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/vividsolutions/jts/geom/GeometryCollection;->equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z

    move-result v0

    goto :goto_0
.end method

.method public getBoundary()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 8

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/MultiPolygon;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v6

    .line 127
    :goto_0
    return-object v6

    .line 118
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v0, "allRings":Ljava/util/ArrayList;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/MultiPolygon;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v6, v6

    if-ge v2, v6, :cond_2

    .line 120
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/MultiPolygon;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v4, v6, v2

    check-cast v4, Lcom/vividsolutions/jts/geom/Polygon;

    .line 121
    .local v4, "polygon":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Polygon;->getBoundary()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    .line 122
    .local v5, "rings":Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 123
    invoke-virtual {v5, v3}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 119
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 126
    .end local v3    # "j":I
    .end local v4    # "polygon":Lcom/vividsolutions/jts/geom/Polygon;
    .end local v5    # "rings":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v1, v6, [Lcom/vividsolutions/jts/geom/LineString;

    .line 127
    .local v1, "allRingsArray":[Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v7

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/vividsolutions/jts/geom/LineString;

    check-cast v6, [Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v7, v6}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v6

    goto :goto_0
.end method

.method public getBoundaryDimension()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public getDimension()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x2

    return v0
.end method

.method public getGeometryType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string v0, "MultiPolygon"

    return-object v0
.end method

.method public reverse()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4

    .prologue
    .line 146
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/MultiPolygon;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v3

    .line 147
    .local v1, "n":I
    new-array v2, v1, [Lcom/vividsolutions/jts/geom/Polygon;

    .line 148
    .local v2, "revGeoms":[Lcom/vividsolutions/jts/geom/Polygon;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/MultiPolygon;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 149
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/MultiPolygon;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Geometry;->reverse()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Polygon;

    aput-object v3, v2, v0

    .line 148
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPolygon([Lcom/vividsolutions/jts/geom/Polygon;)Lcom/vividsolutions/jts/geom/MultiPolygon;

    move-result-object v3

    return-object v3
.end method
