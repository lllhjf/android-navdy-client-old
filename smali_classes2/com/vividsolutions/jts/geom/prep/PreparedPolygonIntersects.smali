.class Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;
.super Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;
.source "PreparedPolygonIntersects.java"


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V
    .locals 0
    .param p1, "prepPoly"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V

    .line 76
    return-void
.end method

.method public static intersects(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p0, "prep"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 64
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V

    .line 65
    .local v0, "polyInt":Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;->intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 7
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v4, 0x1

    .line 92
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;->isAnyTestComponentInTarget(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    .line 93
    .local v0, "isInPrepGeomArea":Z
    if-eqz v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v4

    .line 98
    :cond_1
    invoke-static {p1}, Lcom/vividsolutions/jts/noding/SegmentStringUtil;->extractSegmentStrings(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v2

    .line 100
    .local v2, "lineSegStr":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 101
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getIntersectionFinder()Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->intersects(Ljava/util/Collection;)Z

    move-result v3

    .line 102
    .local v3, "segsIntersect":Z
    if-nez v3, :cond_0

    .line 111
    .end local v3    # "segsIntersect":Z
    :cond_2
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 113
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getRepresentativePoints()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0, p1, v5}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;->isAnyTargetComponentInAreaTest(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Z

    move-result v1

    .line 114
    .local v1, "isPrepGeomInArea":Z
    if-nez v1, :cond_0

    .line 117
    .end local v1    # "isPrepGeomInArea":Z
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method
