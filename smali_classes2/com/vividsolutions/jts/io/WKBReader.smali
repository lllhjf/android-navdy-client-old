.class public Lcom/vividsolutions/jts/io/WKBReader;
.super Ljava/lang/Object;
.source "WKBReader.java"


# static fields
.field private static final INVALID_GEOM_TYPE_MSG:Ljava/lang/String; = "Invalid geometry type encountered in "


# instance fields
.field private SRID:I

.field private csFactory:Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

.field private dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

.field private factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private hasSRID:Z

.field private inputDimension:I

.field private isStrict:Z

.field private ordValues:[D

.field private precisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/io/WKBReader;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 113
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 2
    .param p1, "geometryFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    const/4 v1, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const/4 v0, 0x2

    iput v0, p0, Lcom/vividsolutions/jts/io/WKBReader;->inputDimension:I

    .line 101
    iput-boolean v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->hasSRID:Z

    .line 102
    iput v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->SRID:I

    .line 107
    iput-boolean v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->isStrict:Z

    .line 108
    new-instance v0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-direct {v0}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    .line 116
    iput-object p1, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 117
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/io/WKBReader;->precisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 118
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getCoordinateSequenceFactory()Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/io/WKBReader;->csFactory:Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    .line 119
    return-void
.end method

.method public static hexToBytes(Ljava/lang/String;)[B
    .locals 9
    .param p0, "hex"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    div-int/lit8 v1, v7, 0x2

    .line 70
    .local v1, "byteLen":I
    new-array v2, v1, [B

    .line 72
    .local v2, "bytes":[B
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    if-ge v3, v7, :cond_1

    .line 73
    mul-int/lit8 v4, v3, 0x2

    .line 74
    .local v4, "i2":I
    add-int/lit8 v7, v4, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-le v7, v8, :cond_0

    .line 75
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Hex string has odd length"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 77
    :cond_0
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/vividsolutions/jts/io/WKBReader;->hexToInt(C)I

    move-result v6

    .line 78
    .local v6, "nib1":I
    add-int/lit8 v7, v4, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/vividsolutions/jts/io/WKBReader;->hexToInt(C)I

    move-result v5

    .line 79
    .local v5, "nib0":I
    shl-int/lit8 v7, v6, 0x4

    int-to-byte v8, v5

    add-int/2addr v7, v8

    int-to-byte v0, v7

    .line 80
    .local v0, "b":B
    aput-byte v0, v2, v3

    .line 72
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 82
    .end local v0    # "b":B
    .end local v4    # "i2":I
    .end local v5    # "nib0":I
    .end local v6    # "nib1":I
    :cond_1
    return-object v2
.end method

.method private static hexToInt(C)I
    .locals 4
    .param p0, "hex"    # C

    .prologue
    .line 87
    const/16 v1, 0x10

    invoke-static {p0, v1}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    .line 88
    .local v0, "nib":I
    if-gez v0, :cond_0

    .line 89
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid hex digit: \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_0
    return v0
.end method

.method private readCoordinate()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 346
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->inputDimension:I

    if-ge v0, v1, :cond_1

    .line 347
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->ordValues:[D

    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBReader;->precisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    iget-object v3, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readDouble()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(D)D

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 346
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->ordValues:[D

    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readDouble()D

    move-result-wide v2

    aput-wide v2, v1, v0

    goto :goto_1

    .line 355
    :cond_1
    return-void
.end method

.method private readCoordinateSequence(I)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 6
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->csFactory:Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    iget v5, p0, Lcom/vividsolutions/jts/io/WKBReader;->inputDimension:I

    invoke-interface {v4, p1, v5}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;->create(II)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v2

    .line 311
    .local v2, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    invoke-interface {v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getDimension()I

    move-result v3

    .line 312
    .local v3, "targetDim":I
    iget v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->inputDimension:I

    if-le v3, v4, :cond_0

    .line 313
    iget v3, p0, Lcom/vividsolutions/jts/io/WKBReader;->inputDimension:I

    .line 314
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_2

    .line 315
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readCoordinate()V

    .line 316
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 317
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->ordValues:[D

    aget-wide v4, v4, v1

    invoke-interface {v2, v0, v1, v4, v5}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->setOrdinate(IID)V

    .line 316
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 314
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 320
    .end local v1    # "j":I
    :cond_2
    return-object v2
.end method

.method private readCoordinateSequenceLineString(I)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 3
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 325
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/io/WKBReader;->readCoordinateSequence(I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    .line 326
    .local v0, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    iget-boolean v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->isStrict:Z

    if-eqz v1, :cond_1

    .line 328
    .end local v0    # "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    :cond_0
    :goto_0
    return-object v0

    .line 327
    .restart local v0    # "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    :cond_1
    invoke-interface {v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    if-ge v1, v2, :cond_0

    .line 328
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->csFactory:Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    invoke-static {v1, v0, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->extend(Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;Lcom/vividsolutions/jts/geom/CoordinateSequence;I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private readCoordinateSequenceRing(I)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 2
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 333
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/io/WKBReader;->readCoordinateSequence(I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    .line 334
    .local v0, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    iget-boolean v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->isStrict:Z

    if-eqz v1, :cond_1

    .line 336
    .end local v0    # "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    :cond_0
    :goto_0
    return-object v0

    .line 335
    .restart local v0    # "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    :cond_1
    invoke-static {v0}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->isRing(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 336
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->csFactory:Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    invoke-static {v1, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->ensureValidRing(Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private readGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 160
    iget-object v10, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readByte()B

    move-result v2

    .line 162
    .local v2, "byteOrderWKB":B
    if-ne v2, v8, :cond_4

    move v1, v7

    .line 163
    .local v1, "byteOrder":I
    :goto_0
    iget-object v10, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v10, v1}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->setOrder(I)V

    .line 165
    iget-object v10, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readInt()I

    move-result v6

    .line 166
    .local v6, "typeInt":I
    and-int/lit16 v4, v6, 0xff

    .line 168
    .local v4, "geometryType":I
    const/high16 v10, -0x80000000

    and-int/2addr v10, v6

    if-eqz v10, :cond_5

    move v5, v8

    .line 169
    .local v5, "hasZ":Z
    :goto_1
    if-eqz v5, :cond_0

    const/4 v7, 0x3

    :cond_0
    iput v7, p0, Lcom/vividsolutions/jts/io/WKBReader;->inputDimension:I

    .line 171
    const/high16 v7, 0x20000000

    and-int/2addr v7, v6

    if-eqz v7, :cond_6

    :goto_2
    iput-boolean v8, p0, Lcom/vividsolutions/jts/io/WKBReader;->hasSRID:Z

    .line 173
    const/4 v0, 0x0

    .line 174
    .local v0, "SRID":I
    iget-boolean v7, p0, Lcom/vividsolutions/jts/io/WKBReader;->hasSRID:Z

    if-eqz v7, :cond_1

    .line 175
    iget-object v7, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readInt()I

    move-result v0

    .line 179
    :cond_1
    iget-object v7, p0, Lcom/vividsolutions/jts/io/WKBReader;->ordValues:[D

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/vividsolutions/jts/io/WKBReader;->ordValues:[D

    array-length v7, v7

    iget v8, p0, Lcom/vividsolutions/jts/io/WKBReader;->inputDimension:I

    if-ge v7, v8, :cond_3

    .line 180
    :cond_2
    iget v7, p0, Lcom/vividsolutions/jts/io/WKBReader;->inputDimension:I

    new-array v7, v7, [D

    iput-object v7, p0, Lcom/vividsolutions/jts/io/WKBReader;->ordValues:[D

    .line 182
    :cond_3
    const/4 v3, 0x0

    .line 183
    .local v3, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    packed-switch v4, :pswitch_data_0

    .line 206
    new-instance v7, Lcom/vividsolutions/jts/io/ParseException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown WKB type "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/vividsolutions/jts/io/ParseException;-><init>(Ljava/lang/String;)V

    throw v7

    .end local v0    # "SRID":I
    .end local v1    # "byteOrder":I
    .end local v3    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v4    # "geometryType":I
    .end local v5    # "hasZ":Z
    .end local v6    # "typeInt":I
    :cond_4
    move v1, v8

    .line 162
    goto :goto_0

    .restart local v1    # "byteOrder":I
    .restart local v4    # "geometryType":I
    .restart local v6    # "typeInt":I
    :cond_5
    move v5, v9

    .line 168
    goto :goto_1

    .restart local v5    # "hasZ":Z
    :cond_6
    move v8, v9

    .line 171
    goto :goto_2

    .line 185
    .restart local v0    # "SRID":I
    .restart local v3    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :pswitch_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readPoint()Lcom/vividsolutions/jts/geom/Point;

    move-result-object v3

    .line 208
    :goto_3
    invoke-direct {p0, v3, v0}, Lcom/vividsolutions/jts/io/WKBReader;->setSRID(Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    .line 209
    return-object v3

    .line 188
    :pswitch_1
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readLineString()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    .line 189
    goto :goto_3

    .line 191
    :pswitch_2
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v3

    .line 192
    goto :goto_3

    .line 194
    :pswitch_3
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readMultiPoint()Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v3

    .line 195
    goto :goto_3

    .line 197
    :pswitch_4
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readMultiLineString()Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v3

    .line 198
    goto :goto_3

    .line 200
    :pswitch_5
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readMultiPolygon()Lcom/vividsolutions/jts/geom/MultiPolygon;

    move-result-object v3

    .line 201
    goto :goto_3

    .line 203
    :pswitch_6
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readGeometryCollection()Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v3

    .line 204
    goto :goto_3

    .line 183
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private readGeometryCollection()Lcom/vividsolutions/jts/geom/GeometryCollection;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 300
    iget-object v3, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readInt()I

    move-result v2

    .line 301
    .local v2, "numGeom":I
    new-array v0, v2, [Lcom/vividsolutions/jts/geom/Geometry;

    .line 302
    .local v0, "geoms":[Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 303
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    aput-object v3, v0, v1

    .line 302
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 305
    :cond_0
    iget-object v3, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v3, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v3

    return-object v3
.end method

.method private readLineString()Lcom/vividsolutions/jts/geom/LineString;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readInt()I

    move-result v1

    .line 234
    .local v1, "size":I
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/io/WKBReader;->readCoordinateSequenceLineString(I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    .line 235
    .local v0, "pts":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    return-object v2
.end method

.method private readLinearRing()Lcom/vividsolutions/jts/geom/LinearRing;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readInt()I

    move-result v1

    .line 241
    .local v1, "size":I
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/io/WKBReader;->readCoordinateSequenceRing(I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    .line 242
    .local v0, "pts":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v2

    return-object v2
.end method

.method private readMultiLineString()Lcom/vividsolutions/jts/geom/MultiLineString;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 274
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readInt()I

    move-result v3

    .line 275
    .local v3, "numGeom":I
    new-array v1, v3, [Lcom/vividsolutions/jts/geom/LineString;

    .line 276
    .local v1, "geoms":[Lcom/vividsolutions/jts/geom/LineString;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 277
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 278
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    instance-of v4, v0, Lcom/vividsolutions/jts/geom/LineString;

    if-nez v4, :cond_0

    .line 279
    new-instance v4, Lcom/vividsolutions/jts/io/ParseException;

    const-string v5, "Invalid geometry type encountered in MultiLineString"

    invoke-direct {v4, v5}, Lcom/vividsolutions/jts/io/ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 280
    :cond_0
    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    aput-object v0, v1, v2

    .line 276
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 282
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v4, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v4

    return-object v4
.end method

.method private readMultiPoint()Lcom/vividsolutions/jts/geom/MultiPoint;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 261
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readInt()I

    move-result v3

    .line 262
    .local v3, "numGeom":I
    new-array v1, v3, [Lcom/vividsolutions/jts/geom/Point;

    .line 263
    .local v1, "geoms":[Lcom/vividsolutions/jts/geom/Point;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 264
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 265
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    instance-of v4, v0, Lcom/vividsolutions/jts/geom/Point;

    if-nez v4, :cond_0

    .line 266
    new-instance v4, Lcom/vividsolutions/jts/io/ParseException;

    const-string v5, "Invalid geometry type encountered in MultiPoint"

    invoke-direct {v4, v5}, Lcom/vividsolutions/jts/io/ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 267
    :cond_0
    check-cast v0, Lcom/vividsolutions/jts/geom/Point;

    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    aput-object v0, v1, v2

    .line 263
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 269
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v4, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Point;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v4

    return-object v4
.end method

.method private readMultiPolygon()Lcom/vividsolutions/jts/geom/MultiPolygon;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 287
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readInt()I

    move-result v3

    .line 288
    .local v3, "numGeom":I
    new-array v1, v3, [Lcom/vividsolutions/jts/geom/Polygon;

    .line 289
    .local v1, "geoms":[Lcom/vividsolutions/jts/geom/Polygon;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 290
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 291
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    instance-of v4, v0, Lcom/vividsolutions/jts/geom/Polygon;

    if-nez v4, :cond_0

    .line 292
    new-instance v4, Lcom/vividsolutions/jts/io/ParseException;

    const-string v5, "Invalid geometry type encountered in MultiPolygon"

    invoke-direct {v4, v5}, Lcom/vividsolutions/jts/io/ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 293
    :cond_0
    check-cast v0, Lcom/vividsolutions/jts/geom/Polygon;

    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    aput-object v0, v1, v2

    .line 289
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 295
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v4, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPolygon([Lcom/vividsolutions/jts/geom/Polygon;)Lcom/vividsolutions/jts/geom/MultiPolygon;

    move-result-object v4

    return-object v4
.end method

.method private readPoint()Lcom/vividsolutions/jts/geom/Point;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 227
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/io/WKBReader;->readCoordinateSequence(I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    .line 228
    .local v0, "pts":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v1

    return-object v1
.end method

.method private readPolygon()Lcom/vividsolutions/jts/geom/Polygon;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->readInt()I

    move-result v2

    .line 248
    .local v2, "numRings":I
    const/4 v0, 0x0

    .line 249
    .local v0, "holes":[Lcom/vividsolutions/jts/geom/LinearRing;
    const/4 v4, 0x1

    if-le v2, v4, :cond_0

    .line 250
    add-int/lit8 v4, v2, -0x1

    new-array v0, v4, [Lcom/vividsolutions/jts/geom/LinearRing;

    .line 252
    :cond_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readLinearRing()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v3

    .line 253
    .local v3, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    add-int/lit8 v4, v2, -0x1

    if-ge v1, v4, :cond_1

    .line 254
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readLinearRing()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v4

    aput-object v4, v0, v1

    .line 253
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 256
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBReader;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v4, v3, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v4

    return-object v4
.end method

.method private setSRID(Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 0
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "SRID"    # I

    .prologue
    .line 220
    if-eqz p2, :cond_0

    .line 221
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Geometry;->setSRID(I)V

    .line 222
    :cond_0
    return-object p1
.end method


# virtual methods
.method public read(Lcom/vividsolutions/jts/io/InStream;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "is"    # Lcom/vividsolutions/jts/io/InStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 151
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBReader;->dis:Lcom/vividsolutions/jts/io/ByteOrderDataInStream;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->setInStream(Lcom/vividsolutions/jts/io/InStream;)V

    .line 152
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKBReader;->readGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 153
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v0
.end method

.method public read([B)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 133
    :try_start_0
    new-instance v1, Lcom/vividsolutions/jts/io/ByteArrayInStream;

    invoke-direct {v1, p1}, Lcom/vividsolutions/jts/io/ByteArrayInStream;-><init>([B)V

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/io/WKBReader;->read(Lcom/vividsolutions/jts/io/InStream;)Lcom/vividsolutions/jts/geom/Geometry;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "ex":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected IOException caught: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
