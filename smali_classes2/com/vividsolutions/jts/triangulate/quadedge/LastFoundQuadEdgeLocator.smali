.class public Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;
.super Ljava/lang/Object;
.source "LastFoundQuadEdgeLocator.java"

# interfaces
.implements Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;


# instance fields
.field private lastEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

.field private subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;)V
    .locals 1
    .param p1, "subdiv"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->lastEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 50
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .line 51
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->init()V

    .line 52
    return-void
.end method

.method private findEdge()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 2

    .prologue
    .line 59
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getEdges()Ljava/util/Collection;

    move-result-object v0

    .line 61
    .local v0, "edges":Ljava/util/Collection;
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    return-object v1
.end method

.method private init()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->findEdge()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->lastEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 56
    return-void
.end method


# virtual methods
.method public locate(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 3
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 69
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->lastEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->isLive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->init()V

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->lastEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-virtual {v1, p1, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->locateFromEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 74
    .local v0, "e":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;->lastEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 75
    return-object v0
.end method
