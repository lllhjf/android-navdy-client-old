.class public Lcom/vividsolutions/jts/linearref/LengthLocationMap;
.super Ljava/lang/Object;
.source "LengthLocationMap.java"


# instance fields
.field private linearGeom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 100
    return-void
.end method

.method public static getLength(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/linearref/LinearLocation;)D
    .locals 4
    .param p0, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "loc"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 91
    new-instance v0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 92
    .local v0, "locater":Lcom/vividsolutions/jts/linearref/LengthLocationMap;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLength(Lcom/vividsolutions/jts/linearref/LinearLocation;)D

    move-result-wide v2

    return-wide v2
.end method

.method public static getLocation(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 3
    .param p0, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "length"    # D

    .prologue
    .line 60
    new-instance v0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 61
    .local v0, "locater":Lcom/vividsolutions/jts/linearref/LengthLocationMap;
    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLocation(D)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v1

    return-object v1
.end method

.method public static getLocation(Lcom/vividsolutions/jts/geom/Geometry;DZ)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 3
    .param p0, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "length"    # D
    .param p3, "resolveLower"    # Z

    .prologue
    .line 77
    new-instance v0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 78
    .local v0, "locater":Lcom/vividsolutions/jts/linearref/LengthLocationMap;
    invoke-virtual {v0, p1, p2, p3}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLocation(DZ)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v1

    return-object v1
.end method

.method private getLocationForward(D)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 15
    .param p1, "length"    # D

    .prologue
    .line 144
    const-wide/16 v12, 0x0

    cmpg-double v7, p1, v12

    if-gtz v7, :cond_0

    .line 145
    new-instance v7, Lcom/vividsolutions/jts/linearref/LinearLocation;

    invoke-direct {v7}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>()V

    .line 184
    :goto_0
    return-object v7

    .line 147
    :cond_0
    const-wide/16 v10, 0x0

    .line 149
    .local v10, "totalLength":D
    new-instance v1, Lcom/vividsolutions/jts/linearref/LinearIterator;

    iget-object v7, p0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v1, v7}, Lcom/vividsolutions/jts/linearref/LinearIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 150
    .local v1, "it":Lcom/vividsolutions/jts/linearref/LinearIterator;
    :goto_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 160
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->isEndOfLine()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 161
    cmpl-double v7, v10, p1

    if-nez v7, :cond_3

    .line 162
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getComponentIndex()I

    move-result v0

    .line 163
    .local v0, "compIndex":I
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getVertexIndex()I

    move-result v6

    .line 164
    .local v6, "segIndex":I
    new-instance v7, Lcom/vividsolutions/jts/linearref/LinearLocation;

    const-wide/16 v12, 0x0

    invoke-direct {v7, v0, v6, v12, v13}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>(IID)V

    goto :goto_0

    .line 168
    .end local v0    # "compIndex":I
    .end local v6    # "segIndex":I
    :cond_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getSegmentStart()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 169
    .local v4, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getSegmentEnd()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 170
    .local v5, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v5, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    .line 172
    .local v8, "segLen":D
    add-double v12, v10, v8

    cmpl-double v7, v12, p1

    if-lez v7, :cond_2

    .line 173
    sub-double v12, p1, v10

    div-double v2, v12, v8

    .line 174
    .local v2, "frac":D
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getComponentIndex()I

    move-result v0

    .line 175
    .restart local v0    # "compIndex":I
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getVertexIndex()I

    move-result v6

    .line 176
    .restart local v6    # "segIndex":I
    new-instance v7, Lcom/vividsolutions/jts/linearref/LinearLocation;

    invoke-direct {v7, v0, v6, v2, v3}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>(IID)V

    goto :goto_0

    .line 178
    .end local v0    # "compIndex":I
    .end local v2    # "frac":D
    .end local v6    # "segIndex":I
    :cond_2
    add-double/2addr v10, v8

    .line 181
    .end local v4    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v5    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v8    # "segLen":D
    :cond_3
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->next()V

    goto :goto_1

    .line 184
    :cond_4
    iget-object v7, p0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v7}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getEndLocation(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v7

    goto :goto_0
.end method

.method private resolveHigher(Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 6
    .param p1, "loc"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    const-wide/16 v4, 0x0

    .line 189
    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->isEndpoint(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 200
    .end local p1    # "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    :cond_0
    :goto_0
    return-object p1

    .line 191
    .restart local p1    # "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getComponentIndex()I

    move-result v0

    .line 193
    .local v0, "compIndex":I
    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 196
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 198
    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getLength()D

    move-result-wide v2

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_2

    .line 200
    :cond_3
    new-instance p1, Lcom/vividsolutions/jts/linearref/LinearLocation;

    .end local p1    # "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    const/4 v1, 0x0

    invoke-direct {p1, v0, v1, v4, v5}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>(IID)V

    goto :goto_0
.end method


# virtual methods
.method public getLength(Lcom/vividsolutions/jts/linearref/LinearLocation;)D
    .locals 10
    .param p1, "loc"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 205
    const-wide/16 v6, 0x0

    .line 207
    .local v6, "totalLength":D
    new-instance v0, Lcom/vividsolutions/jts/linearref/LinearIterator;

    iget-object v3, p0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v0, v3}, Lcom/vividsolutions/jts/linearref/LinearIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 208
    .local v0, "it":Lcom/vividsolutions/jts/linearref/LinearIterator;
    :goto_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 209
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->isEndOfLine()Z

    move-result v3

    if-nez v3, :cond_2

    .line 210
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getSegmentStart()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 211
    .local v1, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getSegmentEnd()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 212
    .local v2, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    .line 214
    .local v4, "segLen":D
    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getComponentIndex()I

    move-result v3

    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getComponentIndex()I

    move-result v8

    if-ne v3, v8, :cond_1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentIndex()I

    move-result v3

    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getVertexIndex()I

    move-result v8

    if-ne v3, v8, :cond_1

    .line 216
    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentFraction()D

    move-result-wide v8

    mul-double/2addr v8, v4

    add-double/2addr v6, v8

    .line 222
    .end local v1    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v2    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v4    # "segLen":D
    .end local v6    # "totalLength":D
    :cond_0
    return-wide v6

    .line 218
    .restart local v1    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v2    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v4    # "segLen":D
    .restart local v6    # "totalLength":D
    :cond_1
    add-double/2addr v6, v4

    .line 220
    .end local v1    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v2    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v4    # "segLen":D
    :cond_2
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->next()V

    goto :goto_0
.end method

.method public getLocation(D)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1
    .param p1, "length"    # D

    .prologue
    .line 113
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLocation(DZ)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    return-object v0
.end method

.method public getLocation(DZ)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 9
    .param p1, "length"    # D
    .param p3, "resolveLower"    # Z

    .prologue
    .line 128
    move-wide v0, p1

    .line 131
    .local v0, "forwardLength":D
    const-wide/16 v6, 0x0

    cmpg-double v5, p1, v6

    if-gez v5, :cond_0

    .line 132
    iget-object v5, p0, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->getLength()D

    move-result-wide v2

    .line 133
    .local v2, "lineLen":D
    add-double v0, v2, p1

    .line 135
    .end local v2    # "lineLen":D
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLocationForward(D)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v4

    .line 136
    .local v4, "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    if-eqz p3, :cond_1

    .line 139
    .end local v4    # "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    :goto_0
    return-object v4

    .restart local v4    # "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    :cond_1
    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->resolveHigher(Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v4

    goto :goto_0
.end method
