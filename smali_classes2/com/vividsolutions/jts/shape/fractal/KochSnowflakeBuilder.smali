.class public Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;
.super Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;
.source "KochSnowflakeBuilder.java"


# static fields
.field private static HEIGHT_FACTOR:D = 0.0

.field private static final ONE_THIRD:D = 0.3333333333333333

.field private static final THIRD_HEIGHT:D

.field private static final TWO_THIRDS:D = 0.6666666666666666


# instance fields
.field private coordList:Lcom/vividsolutions/jts/geom/CoordinateList;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 69
    const-wide v0, 0x3ff0c152382d7365L    # 1.0471975511965976

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    sput-wide v0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->HEIGHT_FACTOR:D

    .line 71
    sget-wide v0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->HEIGHT_FACTOR:D

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    div-double/2addr v0, v2

    sput-wide v0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->THIRD_HEIGHT:D

    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 1
    .param p1, "geomFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 43
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    .line 48
    return-void
.end method

.method private addSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method private getBoundary(ILcom/vividsolutions/jts/geom/Coordinate;D)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 11
    .param p1, "level"    # I
    .param p2, "origin"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "width"    # D

    .prologue
    .line 76
    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 78
    .local v4, "y":D
    if-lez p1, :cond_0

    .line 79
    sget-wide v6, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->THIRD_HEIGHT:D

    mul-double/2addr v6, p3

    add-double/2addr v4, v6

    .line 82
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-direct {v0, v6, v7, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 83
    .local v0, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double v8, p3, v8

    add-double/2addr v6, v8

    sget-wide v8, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->HEIGHT_FACTOR:D

    mul-double/2addr v8, p3

    add-double/2addr v8, v4

    invoke-direct {v1, v6, v7, v8, v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 84
    .local v1, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v6, p3

    invoke-direct {v2, v6, v7, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 85
    .local v2, "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p0, p1, v0, v1}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->addSide(ILcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 86
    invoke-virtual {p0, p1, v1, v2}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->addSide(ILcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 87
    invoke-virtual {p0, p1, v2, v0}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->addSide(ILcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 88
    iget-object v3, p0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/CoordinateList;->closeRing()V

    .line 89
    iget-object v3, p0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    return-object v3
.end method

.method public static recursionLevelForSize(I)I
    .locals 8
    .param p0, "numPts"    # I

    .prologue
    .line 52
    div-int/lit8 v4, p0, 0x3

    int-to-double v2, v4

    .line 53
    .local v2, "pow4":D
    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4010000000000000L    # 4.0

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double v0, v4, v6

    .line 54
    .local v0, "exp":D
    double-to-int v4, v0

    return v4
.end method


# virtual methods
.method public addSide(ILcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 10
    .param p1, "level"    # I
    .param p2, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 93
    if-nez p1, :cond_0

    .line 94
    invoke-direct {p0, p2, p3}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->addSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 113
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-static {p2, p3}, Lcom/vividsolutions/jts/math/Vector2D;->create(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v0

    .line 97
    .local v0, "base":Lcom/vividsolutions/jts/math/Vector2D;
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    invoke-virtual {v0, v8, v9}, Lcom/vividsolutions/jts/math/Vector2D;->multiply(D)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v8

    invoke-virtual {v8, p2}, Lcom/vividsolutions/jts/math/Vector2D;->translate(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 99
    .local v2, "midPt":Lcom/vividsolutions/jts/geom/Coordinate;
    sget-wide v8, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->THIRD_HEIGHT:D

    invoke-virtual {v0, v8, v9}, Lcom/vividsolutions/jts/math/Vector2D;->multiply(D)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v1

    .line 100
    .local v1, "heightVec":Lcom/vividsolutions/jts/math/Vector2D;
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Lcom/vividsolutions/jts/math/Vector2D;->rotateByQuarterCircle(I)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v5

    .line 101
    .local v5, "offsetVec":Lcom/vividsolutions/jts/math/Vector2D;
    invoke-virtual {v5, v2}, Lcom/vividsolutions/jts/math/Vector2D;->translate(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 103
    .local v4, "offsetPt":Lcom/vividsolutions/jts/geom/Coordinate;
    add-int/lit8 v3, p1, -0x1

    .line 104
    .local v3, "n2":I
    const-wide v8, 0x3fd5555555555555L    # 0.3333333333333333

    invoke-virtual {v0, v8, v9}, Lcom/vividsolutions/jts/math/Vector2D;->multiply(D)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v8

    invoke-virtual {v8, p2}, Lcom/vividsolutions/jts/math/Vector2D;->translate(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    .line 105
    .local v6, "thirdPt":Lcom/vividsolutions/jts/geom/Coordinate;
    const-wide v8, 0x3fe5555555555555L    # 0.6666666666666666

    invoke-virtual {v0, v8, v9}, Lcom/vividsolutions/jts/math/Vector2D;->multiply(D)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v8

    invoke-virtual {v8, p2}, Lcom/vividsolutions/jts/math/Vector2D;->translate(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 108
    .local v7, "twoThirdPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p0, v3, p2, v6}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->addSide(ILcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 109
    invoke-virtual {p0, v3, v6, v4}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->addSide(ILcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 110
    invoke-virtual {p0, v3, v4, v7}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->addSide(ILcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 111
    invoke-virtual {p0, v3, v7, p3}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->addSide(ILcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0
.end method

.method public getGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6

    .prologue
    .line 59
    iget v3, p0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->numPts:I

    invoke-static {v3}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->recursionLevelForSize(I)I

    move-result v1

    .line 60
    .local v1, "level":I
    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->getSquareBaseLine()Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v0

    .line 61
    .local v0, "baseLine":Lcom/vividsolutions/jts/geom/LineSegment;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineSegment;->getLength()D

    move-result-wide v4

    invoke-direct {p0, v1, v3, v4, v5}, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->getBoundary(ILcom/vividsolutions/jts/geom/Coordinate;D)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 62
    .local v2, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v3, p0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v4, p0, Lcom/vividsolutions/jts/shape/fractal/KochSnowflakeBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v4, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v3

    return-object v3
.end method
