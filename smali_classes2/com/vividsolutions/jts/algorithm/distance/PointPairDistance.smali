.class public Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;
.super Ljava/lang/Object;
.source "PointPairDistance.java"


# instance fields
.field private distance:D

.field private isNull:Z

.field private pt:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    aput-object v2, v0, v1

    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 47
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->distance:D

    .line 48
    iput-boolean v3, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->isNull:Z

    .line 52
    return-void
.end method

.method private initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)V
    .locals 3
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "distance"    # D

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 73
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 74
    iput-wide p3, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->distance:D

    .line 75
    iput-boolean v2, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->isNull:Z

    .line 76
    return-void
.end method


# virtual methods
.method public getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->distance:D

    return-wide v0
.end method

.method public initialize()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->isNull:Z

    return-void
.end method

.method public initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 3
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 59
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 60
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->distance:D

    .line 61
    iput-boolean v2, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->isNull:Z

    .line 62
    return-void
.end method

.method public setMaximum(Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V
    .locals 3
    .param p1, "ptDist"    # Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .prologue
    .line 86
    iget-object v0, p1, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p1, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 87
    return-void
.end method

.method public setMaximum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 91
    iget-boolean v2, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->isNull:Z

    if-eqz v2, :cond_1

    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 96
    .local v0, "dist":D
    iget-wide v2, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->distance:D

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 97
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)V

    goto :goto_0
.end method

.method public setMinimum(Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V
    .locals 3
    .param p1, "ptDist"    # Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .prologue
    .line 102
    iget-object v0, p1, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p1, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->setMinimum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 103
    return-void
.end method

.method public setMinimum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 107
    iget-boolean v2, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->isNull:Z

    if-eqz v2, :cond_1

    .line 108
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 112
    .local v0, "dist":D
    iget-wide v2, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->distance:D

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 113
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
