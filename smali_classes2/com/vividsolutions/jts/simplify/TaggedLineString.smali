.class Lcom/vividsolutions/jts/simplify/TaggedLineString;
.super Ljava/lang/Object;
.source "TaggedLineString.java"


# instance fields
.field private minimumSize:I

.field private parentLine:Lcom/vividsolutions/jts/geom/LineString;

.field private resultSegs:Ljava/util/List;

.field private segs:[Lcom/vividsolutions/jts/simplify/TaggedLineSegment;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 1
    .param p1, "parentLine"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 55
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/simplify/TaggedLineString;-><init>(Lcom/vividsolutions/jts/geom/LineString;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/LineString;I)V
    .locals 1
    .param p1, "parentLine"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "minimumSize"    # I

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->resultSegs:Ljava/util/List;

    .line 59
    iput-object p1, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->parentLine:Lcom/vividsolutions/jts/geom/LineString;

    .line 60
    iput p2, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->minimumSize:I

    .line 61
    invoke-direct {p0}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->init()V

    .line 62
    return-void
.end method

.method private static extractCoordinates(Ljava/util/List;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 5
    .param p0, "segs"    # Ljava/util/List;

    .prologue
    .line 106
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    new-array v1, v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 107
    .local v1, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .line 108
    .local v2, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 109
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    check-cast v2, Lcom/vividsolutions/jts/geom/LineSegment;

    .line 110
    .restart local v2    # "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v3, v2, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v3, v1, v0

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_0
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    iget-object v4, v2, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v4, v1, v3

    .line 114
    return-object v1
.end method

.method private init()V
    .locals 6

    .prologue
    .line 79
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->parentLine:Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 80
    .local v1, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    new-array v3, v3, [Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    iput-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->segs:[Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    .line 81
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 82
    new-instance v2, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    aget-object v3, v1, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v1, v4

    iget-object v5, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->parentLine:Lcom/vividsolutions/jts/geom/LineString;

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;I)V

    .line 84
    .local v2, "seg":Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->segs:[Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    aput-object v2, v3, v0

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    .end local v2    # "seg":Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    :cond_0
    return-void
.end method


# virtual methods
.method public addToResult(Lcom/vividsolutions/jts/geom/LineSegment;)V
    .locals 1
    .param p1, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->resultSegs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    return-void
.end method

.method public asLineString()Lcom/vividsolutions/jts/geom/LineString;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->parentLine:Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->resultSegs:Ljava/util/List;

    invoke-static {v1}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->extractCoordinates(Ljava/util/List;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method

.method public asLinearRing()Lcom/vividsolutions/jts/geom/LinearRing;
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->parentLine:Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->resultSegs:Ljava/util/List;

    invoke-static {v1}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->extractCoordinates(Ljava/util/List;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v0

    return-object v0
.end method

.method public getMinimumSize()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->minimumSize:I

    return v0
.end method

.method public getParent()Lcom/vividsolutions/jts/geom/LineString;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->parentLine:Lcom/vividsolutions/jts/geom/LineString;

    return-object v0
.end method

.method public getParentCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->parentLine:Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getResultCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->resultSegs:Ljava/util/List;

    invoke-static {v0}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->extractCoordinates(Ljava/util/List;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getResultSize()I
    .locals 2

    .prologue
    .line 71
    iget-object v1, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->resultSegs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 72
    .local v0, "resultSegsSize":I
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    add-int/lit8 v1, v0, 0x1

    goto :goto_0
.end method

.method public getSegment(I)Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->segs:[Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getSegments()[Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineString;->segs:[Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    return-object v0
.end method
