.class public Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;
.super Ljava/lang/Object;
.source "RelateNodeGraph.java"


# instance fields
.field private nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/NodeMap;

    new-instance v1, Lcom/vividsolutions/jts/operation/relate/RelateNodeFactory;

    invoke-direct {v1}, Lcom/vividsolutions/jts/operation/relate/RelateNodeFactory;-><init>()V

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;-><init>(Lcom/vividsolutions/jts/geomgraph/NodeFactory;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    .line 69
    return-void
.end method


# virtual methods
.method public build(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 3
    .param p1, "geomGraph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-virtual {p0, p1, v2}, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->computeIntersectionNodes(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;I)V

    .line 81
    invoke-virtual {p0, p1, v2}, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->copyNodesAndLabels(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;I)V

    .line 86
    new-instance v0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;-><init>()V

    .line 87
    .local v0, "eeBuilder":Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;->computeEdgeEnds(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object v1

    .line 88
    .local v1, "eeList":Ljava/util/List;
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->insertEdgeEnds(Ljava/util/List;)V

    .line 92
    return-void
.end method

.method public computeIntersectionNodes(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;I)V
    .locals 8
    .param p1, "geomGraph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;
    .param p2, "argIndex"    # I

    .prologue
    .line 105
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "edgeIt":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 106
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 107
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v1

    .line 108
    .local v1, "eLoc":I
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getEdgeIntersectionList()Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "eiIt":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 109
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 110
    .local v3, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    iget-object v7, v3, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/operation/relate/RelateNode;

    .line 111
    .local v5, "n":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    const/4 v6, 0x1

    if-ne v1, v6, :cond_2

    .line 112
    invoke-virtual {v5, p2}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->setLabelBoundary(I)V

    goto :goto_0

    .line 114
    :cond_2
    invoke-virtual {v5}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/vividsolutions/jts/geomgraph/Label;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 115
    const/4 v6, 0x0

    invoke-virtual {v5, p2, v6}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->setLabel(II)V

    goto :goto_0

    .line 120
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "eLoc":I
    .end local v3    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .end local v4    # "eiIt":Ljava/util/Iterator;
    .end local v5    # "n":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    :cond_3
    return-void
.end method

.method public copyNodesAndLabels(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;I)V
    .locals 5
    .param p1, "geomGraph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;
    .param p2, "argIndex"    # I

    .prologue
    .line 133
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getNodeIterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "nodeIt":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 135
    .local v0, "graphNode":Lcom/vividsolutions/jts/geomgraph/Node;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v1

    .line 136
    .local v1, "newNode":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v3

    invoke-virtual {v1, p2, v3}, Lcom/vividsolutions/jts/geomgraph/Node;->setLabel(II)V

    goto :goto_0

    .line 139
    .end local v0    # "graphNode":Lcom/vividsolutions/jts/geomgraph/Node;
    .end local v1    # "newNode":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-void
.end method

.method public getNodeIterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public insertEdgeEnds(Ljava/util/List;)V
    .locals 3
    .param p1, "ee"    # Ljava/util/List;

    .prologue
    .line 143
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .line 145
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->add(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    goto :goto_0

    .line 147
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    :cond_0
    return-void
.end method
