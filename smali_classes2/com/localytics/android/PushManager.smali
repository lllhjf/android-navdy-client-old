.class Lcom/localytics/android/PushManager;
.super Lcom/localytics/android/BasePushManager;
.source "PushManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/localytics/android/PushManager$POSTBodyBuilder;
    }
.end annotation


# static fields
.field private static final DEVICE_INFO_URL_TEMPLATE:Ljava/lang/String; = "https://%s/test_devices"

.field private static final PUSH_API_URL_TEMPLATE:Ljava/lang/String; = "https://%s/push_test?platform=android&type=prod&campaign=%s&creative=%s&token=%s&install_id=%s&client_ts=%s"

.field private static final PUSH_OPENED_EVENT:Ljava/lang/String; = "Localytics Push Opened"

.field private static final PUSH_RECEIVED_EVENT:Ljava/lang/String; = "Localytics Push Received"

.field private static final TEST_PUSH_EVENTS_URL_TEMPLATE:Ljava/lang/String; = "https://%s/test_push_events"


# direct methods
.method constructor <init>(Lcom/localytics/android/LocalyticsDao;Lcom/localytics/android/MarketingHandler;)V
    .locals 0
    .param p1, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;
    .param p2, "marketingHandler"    # Lcom/localytics/android/MarketingHandler;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/localytics/android/BasePushManager;-><init>(Lcom/localytics/android/LocalyticsDao;Lcom/localytics/android/MarketingHandler;)V

    .line 40
    return-void
.end method

.method private handlePushIntegrationOpenedVerification(Ljava/lang/String;)V
    .locals 3
    .param p1, "pip"    # Ljava/lang/String;

    .prologue
    .line 416
    const-string v0, "opened"

    const-string v1, "Localytics Push Opened event was tagged."

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/localytics/android/PushManager;->handleTestPushEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 417
    return-void
.end method

.method private handleTestPushEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .param p1, "pip"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "successMessage"    # Ljava/lang/String;
    .param p4, "canPresentToast"    # Z

    .prologue
    .line 422
    const-string v1, "https://%s/test_push_events"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getTestPushEventsHost()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 423
    .local v0, "url":Ljava/lang/String;
    new-instance v1, Lcom/localytics/android/PushManager$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/localytics/android/PushManager$3;-><init>(Lcom/localytics/android/PushManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, p4, p3, v1}, Lcom/localytics/android/PushManager;->pushIntegrationUpload(Ljava/lang/String;ZLjava/lang/String;Lcom/localytics/android/PushManager$POSTBodyBuilder;)V

    .line 443
    return-void
.end method

.method private pushIntegrationUpload(Ljava/lang/String;ZLjava/lang/String;Lcom/localytics/android/PushManager$POSTBodyBuilder;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "canPresentToast"    # Z
    .param p3, "successMessage"    # Ljava/lang/String;
    .param p4, "builder"    # Lcom/localytics/android/PushManager$POSTBodyBuilder;

    .prologue
    .line 448
    new-instance v0, Lcom/localytics/android/PushManager$4;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/localytics/android/PushManager$4;-><init>(Lcom/localytics/android/PushManager;Lcom/localytics/android/PushManager$POSTBodyBuilder;Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/localytics/android/PushManager$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 518
    return-void
.end method


# virtual methods
.method _convertToPushCampaign(Landroid/os/Bundle;)Lcom/localytics/android/PushCampaign;
    .locals 4
    .param p1, "data"    # Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 95
    :try_start_0
    const-string v3, "message"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    .local v1, "message":Ljava/lang/String;
    const-string v3, "ll_sound_filename"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "soundURI":Ljava/lang/String;
    new-instance v3, Lcom/localytics/android/PushCampaign;

    invoke-direct {v3, v1, v2, p1}, Lcom/localytics/android/PushCampaign;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "soundURI":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 99
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Lorg/json/JSONException;
    const-string v3, "Failed to parse push campaign from payload, ignoring message"

    invoke-static {v3}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 103
    const/4 v3, 0x0

    goto :goto_0
.end method

.method _handlePushIntegrationReceivedVerification(Ljava/lang/String;)V
    .locals 3
    .param p1, "pip"    # Ljava/lang/String;

    .prologue
    .line 411
    const-string v0, "received"

    const-string v1, "Localytics Push Received event was tagged."

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/localytics/android/PushManager;->handleTestPushEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 412
    return-void
.end method

.method _handlePushReceived(Landroid/os/Bundle;)Z
    .locals 5
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 50
    iget-object v3, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v3}, Lcom/localytics/android/LocalyticsDao;->areNotificationsDisabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 52
    const-string v3, "Got push notification while push is disabled."

    invoke-static {v3}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 80
    :cond_0
    const/4 v3, 0x0

    :goto_0
    return v3

    .line 56
    :cond_1
    const/4 v0, 0x1

    .line 57
    .local v0, "allowedByListener":Z
    invoke-virtual {p0, p1}, Lcom/localytics/android/PushManager;->_convertToPushCampaign(Landroid/os/Bundle;)Lcom/localytics/android/PushCampaign;

    move-result-object v1

    .line 58
    .local v1, "campaign":Lcom/localytics/android/PushCampaign;
    if-eqz v1, :cond_0

    .line 60
    iget-object v4, p0, Lcom/localytics/android/PushManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;

    monitor-enter v4

    .line 62
    :try_start_0
    iget-object v3, p0, Lcom/localytics/android/PushManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;

    iget-object v3, v3, Lcom/localytics/android/MarketingHandler;->mListeners:Lcom/localytics/android/ListenersSet;

    invoke-virtual {v3}, Lcom/localytics/android/ListenersSet;->getDevListener()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/localytics/android/MessagingListener;

    .line 63
    .local v2, "devListener":Lcom/localytics/android/MessagingListener;
    if-eqz v2, :cond_2

    .line 65
    invoke-interface {v2, v1}, Lcom/localytics/android/MessagingListener;->localyticsShouldShowPushNotification(Lcom/localytics/android/PushCampaign;)Z

    move-result v0

    .line 67
    :cond_2
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p0, v1}, Lcom/localytics/android/PushManager;->_tagPushReceivedEvent(Lcom/localytics/android/PushCampaign;)Z

    .line 71
    invoke-virtual {v1}, Lcom/localytics/android/PushCampaign;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/localytics/android/PushManager;->_hasMessage(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/localytics/android/PushCampaign;->isControlGroup()Z

    move-result v3

    if-nez v3, :cond_0

    .line 73
    invoke-virtual {p0, v1, p1}, Lcom/localytics/android/PushManager;->_showPushNotification(Lcom/localytics/android/PushCampaign;Landroid/os/Bundle;)V

    .line 74
    const/4 v3, 0x1

    goto :goto_0

    .line 67
    .end local v2    # "devListener":Lcom/localytics/android/MessagingListener;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method _showPushNotification(Lcom/localytics/android/PushCampaign;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "campaign"    # Lcom/localytics/android/PushCampaign;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 126
    invoke-virtual {p1}, Lcom/localytics/android/PushCampaign;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 127
    .local v2, "message":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/localytics/android/PushCampaign;->getCampaignId()J

    move-result-wide v4

    .line 128
    .local v4, "campaignId":J
    invoke-virtual {p1}, Lcom/localytics/android/PushCampaign;->getSoundFilename()Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v6, p1

    move-object v7, p2

    invoke-virtual/range {v1 .. v7}, Lcom/localytics/android/PushManager;->_showPushNotification(Ljava/lang/String;Ljava/lang/String;JLcom/localytics/android/Campaign;Landroid/os/Bundle;)V

    .line 129
    return-void
.end method

.method _tagPushReceivedEvent(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "data"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/localytics/android/PushManager;->_convertToPushCampaign(Landroid/os/Bundle;)Lcom/localytics/android/PushCampaign;

    move-result-object v0

    .line 86
    .local v0, "campaign":Lcom/localytics/android/PushCampaign;
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/localytics/android/PushManager;->_tagPushReceivedEvent(Lcom/localytics/android/PushCampaign;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method _tagPushReceivedEvent(Lcom/localytics/android/PushCampaign;)Z
    .locals 18
    .param p1, "campaign"    # Lcom/localytics/android/PushCampaign;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 108
    invoke-virtual/range {p1 .. p1}, Lcom/localytics/android/PushCampaign;->getCampaignId()J

    move-result-wide v4

    .line 109
    .local v4, "campaignId":J
    invoke-virtual/range {p1 .. p1}, Lcom/localytics/android/PushCampaign;->getCreativeId()J

    move-result-wide v14

    .line 111
    .local v14, "creativeId":J
    invoke-virtual/range {p1 .. p1}, Lcom/localytics/android/PushCampaign;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 112
    .local v3, "message":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/localytics/android/PushCampaign;->getCreativeType()Ljava/lang/String;

    move-result-object v8

    .line 113
    .local v8, "creativeType":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/localytics/android/PushCampaign;->getSchemaVersion()J

    move-result-wide v16

    .line 114
    .local v16, "serverSchemaVersion":J
    invoke-virtual/range {p1 .. p1}, Lcom/localytics/android/PushCampaign;->getKillSwitch()I

    move-result v9

    .line 115
    .local v9, "killSwitch":I
    invoke-virtual/range {p1 .. p1}, Lcom/localytics/android/PushCampaign;->getTestMode()I

    move-result v10

    .line 116
    .local v10, "testMode":I
    invoke-virtual/range {p1 .. p1}, Lcom/localytics/android/PushCampaign;->getPip()Ljava/lang/String;

    move-result-object v11

    .line 118
    .local v11, "pip":Ljava/lang/String;
    const-string v2, "Localytics Push Received"

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    const/4 v12, 0x0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v12}, Lcom/localytics/android/PushManager;->_tagPushReceived(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method handleDeviceInfo(Landroid/net/Uri;)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, 0x1

    .line 331
    iget-object v3, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v3}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 332
    .local v0, "appContext":Landroid/content/Context;
    iget-object v3, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v3}, Lcom/localytics/android/LocalyticsDao;->getCustomerIdFuture()Ljava/util/concurrent/Future;

    move-result-object v1

    .line 333
    .local v1, "customerIdFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/String;>;"
    const-string v3, "https://%s/test_devices"

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->getTestDevicesHost()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 334
    .local v2, "url":Ljava/lang/String;
    const-string v3, "You have successfully paired your device with the Dashboard."

    new-instance v4, Lcom/localytics/android/PushManager$2;

    invoke-direct {v4, p0, p1, v1, v0}, Lcom/localytics/android/PushManager$2;-><init>(Lcom/localytics/android/PushManager;Landroid/net/Uri;Ljava/util/concurrent/Future;Landroid/content/Context;)V

    invoke-direct {p0, v2, v7, v3, v4}, Lcom/localytics/android/PushManager;->pushIntegrationUpload(Ljava/lang/String;ZLjava/lang/String;Lcom/localytics/android/PushManager$POSTBodyBuilder;)V

    .line 406
    return-void
.end method

.method handlePushNotificationOpened(Landroid/content/Intent;)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 135
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    if-nez v10, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "ll"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 142
    .local v6, "llString":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 146
    :try_start_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 147
    .local v5, "llObject":Lorg/json/JSONObject;
    const-string v10, "ca"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 148
    .local v1, "campaignId":Ljava/lang/String;
    const-string v10, "cr"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "creativeId":Ljava/lang/String;
    const-string v10, "v"

    const-string v11, "1"

    invoke-virtual {v5, v10, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 150
    .local v8, "serverSchemaVersion":Ljava/lang/String;
    const-string v10, "test_mode"

    const/4 v11, 0x0

    invoke-virtual {v5, v10, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v9

    .line 152
    .local v9, "testMode":I
    const-string v10, "t"

    const/4 v11, 0x0

    invoke-virtual {v5, v10, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 153
    .local v3, "creativeType":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 155
    const-string v3, "Alert"

    .line 158
    :cond_2
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    if-nez v9, :cond_3

    .line 160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 161
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v10, "Campaign ID"

    invoke-virtual {v0, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    const-string v10, "Creative ID"

    invoke-virtual {v0, v10, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    const-string v10, "Creative Type"

    invoke-virtual {v0, v10, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    const-string v10, "Action"

    const-string v11, "Click"

    invoke-virtual {v0, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    const-string v10, "Schema Version - Client"

    const/4 v11, 0x5

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    const-string v10, "Schema Version - Server"

    invoke-virtual {v0, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v10, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v11, "Localytics Push Opened"

    invoke-interface {v10, v11, v0}, Lcom/localytics/android/LocalyticsDao;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 169
    const-string v10, "pip"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 170
    .local v7, "pip":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 172
    invoke-direct {p0, v7}, Lcom/localytics/android/PushManager;->handlePushIntegrationOpenedVerification(Ljava/lang/String;)V

    .line 177
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "pip":Ljava/lang/String;
    :cond_3
    const-string v10, "ll"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 179
    .end local v1    # "campaignId":Ljava/lang/String;
    .end local v2    # "creativeId":Ljava/lang/String;
    .end local v3    # "creativeType":Ljava/lang/String;
    .end local v5    # "llObject":Lorg/json/JSONObject;
    .end local v8    # "serverSchemaVersion":Ljava/lang/String;
    .end local v9    # "testMode":I
    :catch_0
    move-exception v4

    .line 181
    .local v4, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v10, "Failed to get campaign id or creative id from payload"

    invoke-static {v10}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 185
    .end local v4    # "e":Lorg/json/JSONException;
    .end local v6    # "llString":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 187
    .local v4, "e":Ljava/lang/Exception;
    const-string v10, "Exception while handling opened push"

    invoke-static {v10, v4}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method handlePushTestMode([Ljava/lang/String;)V
    .locals 7
    .param p1, "components"    # [Ljava/lang/String;

    .prologue
    .line 195
    const/4 v0, 0x2

    :try_start_0
    aget-object v3, p1, v0

    .line 196
    .local v3, "campaign":Ljava/lang/String;
    const/4 v0, 0x3

    aget-object v4, p1, v0

    .line 197
    .local v4, "creative":Ljava/lang/String;
    iget-object v0, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getCustomerIdFuture()Ljava/util/concurrent/Future;

    move-result-object v5

    .line 198
    .local v5, "customerIdFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 200
    .local v2, "appContext":Landroid/content/Context;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Lcom/localytics/android/PushManager$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/localytics/android/PushManager$1;-><init>(Lcom/localytics/android/PushManager;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Future;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/localytics/android/PushManager$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    .end local v2    # "appContext":Landroid/content/Context;
    .end local v3    # "campaign":Ljava/lang/String;
    .end local v4    # "creative":Ljava/lang/String;
    .end local v5    # "customerIdFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-void

    .line 323
    :catch_0
    move-exception v6

    .line 325
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "Exception while handling test mode"

    invoke-static {v0, v6}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
