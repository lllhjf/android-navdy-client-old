.class final Lcom/nimbusds/jose/util/Base64Codec;
.super Ljava/lang/Object;
.source "Base64Codec.java"


# static fields
.field private static final CA:[C

.field private static final CA_URL_SAFE:[C

.field private static final IA:[I

.field private static final IA_URL_SAFE:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v3, 0x100

    const/16 v6, 0x3d

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 54
    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    sput-object v2, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    .line 60
    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    sput-object v2, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    .line 66
    new-array v2, v3, [I

    sput-object v2, Lcom/nimbusds/jose/util/Base64Codec;->IA:[I

    .line 72
    new-array v2, v3, [I

    sput-object v2, Lcom/nimbusds/jose/util/Base64Codec;->IA_URL_SAFE:[I

    .line 77
    sget-object v2, Lcom/nimbusds/jose/util/Base64Codec;->IA:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 78
    const/4 v0, 0x0

    .local v0, "i":I
    sget-object v2, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    array-length v1, v2

    .local v1, "iS":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 81
    sget-object v2, Lcom/nimbusds/jose/util/Base64Codec;->IA:[I

    aput v5, v2, v6

    .line 84
    sget-object v2, Lcom/nimbusds/jose/util/Base64Codec;->IA_URL_SAFE:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 85
    const/4 v0, 0x0

    sget-object v2, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    array-length v1, v2

    :goto_1
    if-lt v0, v1, :cond_1

    .line 88
    sget-object v2, Lcom/nimbusds/jose/util/Base64Codec;->IA_URL_SAFE:[I

    aput v5, v2, v6

    .line 89
    return-void

    .line 79
    :cond_0
    sget-object v2, Lcom/nimbusds/jose/util/Base64Codec;->IA:[I

    sget-object v3, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    aget-char v3, v3, v0

    aput v0, v2, v3

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_1
    sget-object v2, Lcom/nimbusds/jose/util/Base64Codec;->IA_URL_SAFE:[I

    sget-object v3, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    aget-char v3, v3, v0

    aput v0, v2, v3

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static computeEncodedLength(IZ)I
    .locals 3
    .param p0, "inputLength"    # I
    .param p1, "urlSafe"    # Z

    .prologue
    .line 103
    if-nez p0, :cond_1

    .line 104
    const/4 v0, 0x0

    .line 119
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    if-eqz p1, :cond_2

    .line 110
    div-int/lit8 v2, p0, 0x3

    shl-int/lit8 v0, v2, 0x2

    .line 113
    .local v0, "fullQuadLength":I
    rem-int/lit8 v1, p0, 0x3

    .line 116
    .local v1, "remainder":I
    if-eqz v1, :cond_0

    add-int v2, v0, v1

    add-int/lit8 v0, v2, 0x1

    goto :goto_0

    .line 119
    .end local v0    # "fullQuadLength":I
    .end local v1    # "remainder":I
    :cond_2
    add-int/lit8 v2, p0, -0x1

    div-int/lit8 v2, v2, 0x3

    add-int/lit8 v2, v2, 0x1

    shl-int/lit8 v0, v2, 0x2

    goto :goto_0
.end method

.method public static countIllegalChars(Ljava/lang/String;)I
    .locals 5
    .param p0, "b64String"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 178
    const/4 v2, 0x0

    .line 180
    .local v2, "illegalCharCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 189
    return v2

    .line 182
    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 184
    .local v0, "c":C
    sget-object v3, Lcom/nimbusds/jose/util/Base64Codec;->IA:[I

    aget v3, v3, v0

    if-ne v3, v4, :cond_1

    sget-object v3, Lcom/nimbusds/jose/util/Base64Codec;->IA_URL_SAFE:[I

    aget v3, v3, v0

    if-ne v3, v4, :cond_1

    .line 185
    add-int/lit8 v2, v2, 0x1

    .line 180
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static decode(Ljava/lang/String;)[B
    .locals 15
    .param p0, "b64String"    # Ljava/lang/String;

    .prologue
    const/4 v14, 0x0

    .line 298
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 299
    :cond_0
    new-array v3, v14, [B

    .line 354
    :cond_1
    :goto_0
    return-object v3

    .line 302
    :cond_2
    invoke-static {p0}, Lcom/nimbusds/jose/util/Base64Codec;->normalizeEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 304
    .local v7, "nStr":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v11

    .line 308
    .local v11, "sLen":I
    invoke-static {v7}, Lcom/nimbusds/jose/util/Base64Codec;->countIllegalChars(Ljava/lang/String;)I

    move-result v12

    .line 311
    .local v12, "sepCnt":I
    sub-int v13, v11, v12

    rem-int/lit8 v13, v13, 0x4

    if-eqz v13, :cond_3

    .line 313
    new-array v3, v14, [B

    goto :goto_0

    .line 317
    :cond_3
    const/4 v8, 0x0

    .line 319
    .local v8, "pad":I
    move v4, v11

    .local v4, "i":I
    :cond_4
    :goto_1
    const/4 v13, 0x1

    if-le v4, v13, :cond_5

    sget-object v13, Lcom/nimbusds/jose/util/Base64Codec;->IA:[I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v14

    aget v13, v13, v14

    if-lez v13, :cond_7

    .line 325
    :cond_5
    sub-int v13, v11, v12

    mul-int/lit8 v13, v13, 0x6

    shr-int/lit8 v13, v13, 0x3

    sub-int v6, v13, v8

    .line 328
    .local v6, "len":I
    new-array v3, v6, [B

    .line 330
    .local v3, "dArr":[B
    const/4 v9, 0x0

    .local v9, "s":I
    const/4 v1, 0x0

    .local v1, "d":I
    move v2, v1

    .end local v1    # "d":I
    .local v2, "d":I
    :goto_2
    if-ge v2, v6, :cond_1

    .line 333
    const/4 v4, 0x0

    .line 335
    const/4 v5, 0x0

    .local v5, "j":I
    move v10, v9

    .end local v9    # "s":I
    .local v10, "s":I
    :goto_3
    const/4 v13, 0x4

    if-lt v5, v13, :cond_8

    .line 345
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "d":I
    .restart local v1    # "d":I
    shr-int/lit8 v13, v4, 0x10

    int-to-byte v13, v13

    aput-byte v13, v3, v2

    .line 346
    if-ge v1, v6, :cond_6

    .line 347
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "d":I
    .restart local v2    # "d":I
    shr-int/lit8 v13, v4, 0x8

    int-to-byte v13, v13

    aput-byte v13, v3, v1

    .line 348
    if-ge v2, v6, :cond_a

    .line 349
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "d":I
    .restart local v1    # "d":I
    int-to-byte v13, v4

    aput-byte v13, v3, v2

    :cond_6
    move v2, v1

    .end local v1    # "d":I
    .restart local v2    # "d":I
    move v9, v10

    .end local v10    # "s":I
    .restart local v9    # "s":I
    goto :goto_2

    .line 320
    .end local v2    # "d":I
    .end local v3    # "dArr":[B
    .end local v5    # "j":I
    .end local v6    # "len":I
    .end local v9    # "s":I
    :cond_7
    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0x3d

    if-ne v13, v14, :cond_4

    .line 321
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 337
    .restart local v2    # "d":I
    .restart local v3    # "dArr":[B
    .restart local v5    # "j":I
    .restart local v6    # "len":I
    .restart local v10    # "s":I
    :cond_8
    sget-object v13, Lcom/nimbusds/jose/util/Base64Codec;->IA:[I

    add-int/lit8 v9, v10, 0x1

    .end local v10    # "s":I
    .restart local v9    # "s":I
    invoke-virtual {v7, v10}, Ljava/lang/String;->charAt(I)C

    move-result v14

    aget v0, v13, v14

    .line 338
    .local v0, "c":I
    if-ltz v0, :cond_9

    .line 339
    mul-int/lit8 v13, v5, 0x6

    rsub-int/lit8 v13, v13, 0x12

    shl-int v13, v0, v13

    or-int/2addr v4, v13

    .line 335
    :goto_4
    add-int/lit8 v5, v5, 0x1

    move v10, v9

    .end local v9    # "s":I
    .restart local v10    # "s":I
    goto :goto_3

    .line 341
    .end local v10    # "s":I
    .restart local v9    # "s":I
    :cond_9
    add-int/lit8 v5, v5, -0x1

    goto :goto_4

    .end local v0    # "c":I
    .end local v9    # "s":I
    .restart local v10    # "s":I
    :cond_a
    move v9, v10

    .end local v10    # "s":I
    .restart local v9    # "s":I
    goto :goto_2
.end method

.method public static encodeToChar([BZ)[C
    .locals 13
    .param p0, "byteArray"    # [B
    .param p1, "urlSafe"    # Z

    .prologue
    .line 206
    if-eqz p0, :cond_1

    array-length v9, p0

    .line 208
    .local v9, "sLen":I
    :goto_0
    if-nez v9, :cond_2

    .line 209
    const/4 v10, 0x0

    new-array v6, v10, [C

    .line 263
    :cond_0
    :goto_1
    return-object v6

    .line 206
    .end local v9    # "sLen":I
    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    .line 212
    .restart local v9    # "sLen":I
    :cond_2
    div-int/lit8 v10, v9, 0x3

    mul-int/lit8 v3, v10, 0x3

    .line 213
    .local v3, "eLen":I
    invoke-static {v9, p1}, Lcom/nimbusds/jose/util/Base64Codec;->computeEncodedLength(IZ)I

    move-result v2

    .line 214
    .local v2, "dLen":I
    new-array v6, v2, [C

    .line 217
    .local v6, "out":[C
    const/4 v7, 0x0

    .local v7, "s":I
    const/4 v0, 0x0

    .local v0, "d":I
    move v1, v0

    .end local v0    # "d":I
    .local v1, "d":I
    move v8, v7

    .end local v7    # "s":I
    .local v8, "s":I
    :goto_2
    if-lt v8, v3, :cond_3

    .line 238
    sub-int v5, v9, v3

    .line 239
    .local v5, "left":I
    if-lez v5, :cond_0

    .line 241
    aget-byte v10, p0, v3

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v11, v10, 0xa

    const/4 v10, 0x2

    if-ne v5, v10, :cond_5

    add-int/lit8 v10, v9, -0x1

    aget-byte v10, p0, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x2

    :goto_3
    or-int v4, v11, v10

    .line 244
    .local v4, "i":I
    if-eqz p1, :cond_7

    .line 246
    const/4 v10, 0x2

    if-ne v5, v10, :cond_6

    .line 247
    add-int/lit8 v10, v2, -0x3

    sget-object v11, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    shr-int/lit8 v12, v4, 0xc

    aget-char v11, v11, v12

    aput-char v11, v6, v10

    .line 248
    add-int/lit8 v10, v2, -0x2

    sget-object v11, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    ushr-int/lit8 v12, v4, 0x6

    and-int/lit8 v12, v12, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v10

    .line 249
    add-int/lit8 v10, v2, -0x1

    sget-object v11, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    and-int/lit8 v12, v4, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v10

    goto :goto_1

    .line 220
    .end local v4    # "i":I
    .end local v5    # "left":I
    :cond_3
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "s":I
    .restart local v7    # "s":I
    aget-byte v10, p0, v8

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x10

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "s":I
    .restart local v8    # "s":I
    aget-byte v11, p0, v7

    and-int/lit16 v11, v11, 0xff

    shl-int/lit8 v11, v11, 0x8

    or-int/2addr v10, v11

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "s":I
    .restart local v7    # "s":I
    aget-byte v11, p0, v8

    and-int/lit16 v11, v11, 0xff

    or-int v4, v10, v11

    .line 223
    .restart local v4    # "i":I
    if-eqz p1, :cond_4

    .line 224
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "d":I
    .restart local v0    # "d":I
    sget-object v10, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    ushr-int/lit8 v11, v4, 0x12

    and-int/lit8 v11, v11, 0x3f

    aget-char v10, v10, v11

    aput-char v10, v6, v1

    .line 225
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "d":I
    .restart local v1    # "d":I
    sget-object v10, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    ushr-int/lit8 v11, v4, 0xc

    and-int/lit8 v11, v11, 0x3f

    aget-char v10, v10, v11

    aput-char v10, v6, v0

    .line 226
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "d":I
    .restart local v0    # "d":I
    sget-object v10, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    ushr-int/lit8 v11, v4, 0x6

    and-int/lit8 v11, v11, 0x3f

    aget-char v10, v10, v11

    aput-char v10, v6, v1

    .line 227
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "d":I
    .restart local v1    # "d":I
    sget-object v10, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    and-int/lit8 v11, v4, 0x3f

    aget-char v10, v10, v11

    aput-char v10, v6, v0

    move v8, v7

    .line 228
    .end local v7    # "s":I
    .restart local v8    # "s":I
    goto/16 :goto_2

    .line 229
    .end local v8    # "s":I
    .restart local v7    # "s":I
    :cond_4
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "d":I
    .restart local v0    # "d":I
    sget-object v10, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    ushr-int/lit8 v11, v4, 0x12

    and-int/lit8 v11, v11, 0x3f

    aget-char v10, v10, v11

    aput-char v10, v6, v1

    .line 230
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "d":I
    .restart local v1    # "d":I
    sget-object v10, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    ushr-int/lit8 v11, v4, 0xc

    and-int/lit8 v11, v11, 0x3f

    aget-char v10, v10, v11

    aput-char v10, v6, v0

    .line 231
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "d":I
    .restart local v0    # "d":I
    sget-object v10, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    ushr-int/lit8 v11, v4, 0x6

    and-int/lit8 v11, v11, 0x3f

    aget-char v10, v10, v11

    aput-char v10, v6, v1

    .line 232
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "d":I
    .restart local v1    # "d":I
    sget-object v10, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    and-int/lit8 v11, v4, 0x3f

    aget-char v10, v10, v11

    aput-char v10, v6, v0

    move v8, v7

    .end local v7    # "s":I
    .restart local v8    # "s":I
    goto/16 :goto_2

    .line 241
    .end local v4    # "i":I
    .restart local v5    # "left":I
    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_3

    .line 251
    .restart local v4    # "i":I
    :cond_6
    add-int/lit8 v10, v2, -0x2

    sget-object v11, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    shr-int/lit8 v12, v4, 0xc

    aget-char v11, v11, v12

    aput-char v11, v6, v10

    .line 252
    add-int/lit8 v10, v2, -0x1

    sget-object v11, Lcom/nimbusds/jose/util/Base64Codec;->CA_URL_SAFE:[C

    ushr-int/lit8 v12, v4, 0x6

    and-int/lit8 v12, v12, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v10

    goto/16 :goto_1

    .line 256
    :cond_7
    add-int/lit8 v10, v2, -0x4

    sget-object v11, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    shr-int/lit8 v12, v4, 0xc

    aget-char v11, v11, v12

    aput-char v11, v6, v10

    .line 257
    add-int/lit8 v10, v2, -0x3

    sget-object v11, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    ushr-int/lit8 v12, v4, 0x6

    and-int/lit8 v12, v12, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v10

    .line 258
    add-int/lit8 v11, v2, -0x2

    const/4 v10, 0x2

    if-ne v5, v10, :cond_8

    sget-object v10, Lcom/nimbusds/jose/util/Base64Codec;->CA:[C

    and-int/lit8 v12, v4, 0x3f

    aget-char v10, v10, v12

    :goto_4
    aput-char v10, v6, v11

    .line 259
    add-int/lit8 v10, v2, -0x1

    const/16 v11, 0x3d

    aput-char v11, v6, v10

    goto/16 :goto_1

    .line 258
    :cond_8
    const/16 v10, 0x3d

    goto :goto_4
.end method

.method public static encodeToString([BZ)Ljava/lang/String;
    .locals 2
    .param p0, "byteArray"    # [B
    .param p1, "urlSafe"    # Z

    .prologue
    .line 281
    new-instance v0, Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/nimbusds/jose/util/Base64Codec;->encodeToChar([BZ)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public static normalizeEncodedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "b64String"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 136
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 139
    .local v2, "inputLen":I
    invoke-static {p0}, Lcom/nimbusds/jose/util/Base64Codec;->countIllegalChars(Ljava/lang/String;)I

    move-result v6

    sub-int v3, v2, v6

    .line 140
    .local v3, "legalLen":I
    rem-int/lit8 v6, v3, 0x4

    if-nez v6, :cond_0

    move v4, v5

    .line 143
    .local v4, "padLength":I
    :goto_0
    add-int v6, v2, v4

    new-array v0, v6, [C

    .line 146
    .local v0, "chars":[C
    invoke-virtual {p0, v5, v2, v0, v5}, Ljava/lang/String;->getChars(II[CI)V

    .line 149
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v4, :cond_1

    .line 154
    const/4 v1, 0x0

    :goto_2
    if-lt v1, v2, :cond_2

    .line 162
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([C)V

    return-object v5

    .line 140
    .end local v0    # "chars":[C
    .end local v1    # "i":I
    .end local v4    # "padLength":I
    :cond_0
    rem-int/lit8 v6, v3, 0x4

    rsub-int/lit8 v4, v6, 0x4

    goto :goto_0

    .line 150
    .restart local v0    # "chars":[C
    .restart local v1    # "i":I
    .restart local v4    # "padLength":I
    :cond_1
    add-int v5, v2, v1

    const/16 v6, 0x3d

    aput-char v6, v0, v5

    .line 149
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 155
    :cond_2
    aget-char v5, v0, v1

    const/16 v6, 0x5f

    if-ne v5, v6, :cond_4

    .line 156
    const/16 v5, 0x2f

    aput-char v5, v0, v1

    .line 154
    :cond_3
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 157
    :cond_4
    aget-char v5, v0, v1

    const/16 v6, 0x2d

    if-ne v5, v6, :cond_3

    .line 158
    const/16 v5, 0x2b

    aput-char v5, v0, v1

    goto :goto_3
.end method
