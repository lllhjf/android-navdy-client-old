.class public Lcom/google/maps/GeocodingApi;
.super Ljava/lang/Object;
.source "GeocodingApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/maps/GeocodingApi$ComponentFilter;,
        Lcom/google/maps/GeocodingApi$Response;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method public static geocode(Lcom/google/maps/GeoApiContext;Ljava/lang/String;)Lcom/google/maps/GeocodingApiRequest;
    .locals 1
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 50
    new-instance v0, Lcom/google/maps/GeocodingApiRequest;

    invoke-direct {v0, p0}, Lcom/google/maps/GeocodingApiRequest;-><init>(Lcom/google/maps/GeoApiContext;)V

    .line 51
    .local v0, "request":Lcom/google/maps/GeocodingApiRequest;
    invoke-virtual {v0, p1}, Lcom/google/maps/GeocodingApiRequest;->address(Ljava/lang/String;)Lcom/google/maps/GeocodingApiRequest;

    .line 52
    return-object v0
.end method

.method public static newRequest(Lcom/google/maps/GeoApiContext;)Lcom/google/maps/GeocodingApiRequest;
    .locals 1
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;

    .prologue
    .line 43
    new-instance v0, Lcom/google/maps/GeocodingApiRequest;

    invoke-direct {v0, p0}, Lcom/google/maps/GeocodingApiRequest;-><init>(Lcom/google/maps/GeoApiContext;)V

    return-object v0
.end method

.method public static reverseGeocode(Lcom/google/maps/GeoApiContext;Lcom/google/maps/model/LatLng;)Lcom/google/maps/GeocodingApiRequest;
    .locals 1
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "location"    # Lcom/google/maps/model/LatLng;

    .prologue
    .line 59
    new-instance v0, Lcom/google/maps/GeocodingApiRequest;

    invoke-direct {v0, p0}, Lcom/google/maps/GeocodingApiRequest;-><init>(Lcom/google/maps/GeoApiContext;)V

    .line 60
    .local v0, "request":Lcom/google/maps/GeocodingApiRequest;
    invoke-virtual {v0, p1}, Lcom/google/maps/GeocodingApiRequest;->latlng(Lcom/google/maps/model/LatLng;)Lcom/google/maps/GeocodingApiRequest;

    .line 61
    return-object v0
.end method
