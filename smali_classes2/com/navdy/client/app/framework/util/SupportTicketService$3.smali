.class final Lcom/navdy/client/app/framework/util/SupportTicketService$3;
.super Ljava/lang/Object;
.source "SupportTicketService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SupportTicketService;->collectLogs(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V
    .locals 0

    .prologue
    .line 557
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 26

    .prologue
    .line 560
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    .line 561
    .local v3, "applicationContext":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/PathManager;->getLogsFolder()Ljava/lang/String;

    move-result-object v5

    .line 563
    .local v5, "logsFolder":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->access$100()Ljava/text/SimpleDateFormat;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    .line 564
    .local v21, "timestamp":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "Database_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ".db"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 565
    .local v11, "deviceDbsFilePath":Ljava/lang/String;
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v0, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 566
    .local v16, "mobileClientDbsFile":Ljava/io/File;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "-SharedPrefs"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 567
    .local v12, "deviceSharedPrefsFolderPath":Ljava/lang/String;
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 568
    .local v17, "mobileClientSharedPrefsFolder":Ljava/io/File;
    const/16 v22, 0x0

    .line 570
    .local v22, "tracesFile":Ljava/io/File;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v10

    .line 571
    .local v10, "context":Landroid/content/Context;
    const v4, 0x7f0804bf

    invoke-virtual {v10, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 572
    .local v18, "path":Ljava/lang/String;
    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .end local v22    # "tracesFile":Ljava/io/File;
    .local v23, "tracesFile":Ljava/io/File;
    move-object/from16 v22, v23

    .line 577
    .end local v10    # "context":Landroid/content/Context;
    .end local v18    # "path":Ljava/lang/String;
    .end local v23    # "tracesFile":Ljava/io/File;
    .restart local v22    # "tracesFile":Ljava/io/File;
    :goto_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 580
    .local v9, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :try_start_1
    invoke-static {v3, v11}, Lcom/navdy/client/app/framework/util/SupportTicketService;->access$200(Landroid/content/Context;Ljava/lang/String;)V

    .line 581
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 582
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "mobile client Dbs successfully dumped"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 583
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    :cond_0
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 587
    invoke-static {v3, v12}, Lcom/navdy/client/app/framework/util/SupportTicketService;->access$300(Landroid/content/Context;Ljava/lang/String;)V

    .line 588
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "mobile client SharedPrefs successfully dumped"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 590
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v13

    .line 591
    .local v13, "listOfSharedPrefsFiles":[Ljava/io/File;
    if-eqz v13, :cond_2

    .line 592
    array-length v6, v13

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v6, :cond_2

    aget-object v19, v13, v4

    .line 593
    .local v19, "sharedPrefsFile":Ljava/io/File;
    if-eqz v19, :cond_1

    .line 594
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 595
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 596
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 602
    .end local v13    # "listOfSharedPrefsFiles":[Ljava/io/File;
    .end local v19    # "sharedPrefsFile":Ljava/io/File;
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getLogFiles()Ljava/util/ArrayList;

    move-result-object v15

    .line 603
    .local v15, "logFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    .line 604
    .local v14, "logFile":Ljava/io/File;
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v24, 0x0

    cmp-long v6, v6, v24

    if-lez v6, :cond_3

    .line 605
    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 662
    .end local v14    # "logFile":Ljava/io/File;
    .end local v15    # "logFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :catch_0
    move-exception v20

    .line 663
    .local v20, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error collecting device logs: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 664
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_6

    .line 665
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/app/framework/util/SupportTicketService$3;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    invoke-interface {v4, v9}, Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;->onLogCollectionSucceeded(Ljava/util/ArrayList;)V

    .line 669
    .end local v20    # "t":Ljava/lang/Throwable;
    :goto_3
    return-void

    .line 609
    .restart local v15    # "logFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_4
    if-eqz v22, :cond_5

    :try_start_2
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v24, 0x0

    cmp-long v4, v6, v24

    if-lez v4, :cond_5

    .line 610
    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 614
    :cond_5
    new-instance v2, Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    sget-object v4, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_LOGS:Lcom/navdy/service/library/events/file/FileType;

    const-wide/16 v6, 0x3a98

    new-instance v8, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v5, v9}, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;-><init>(Lcom/navdy/client/app/framework/util/SupportTicketService$3;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-direct/range {v2 .. v8}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;-><init>(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;JLcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V

    .line 661
    .local v2, "fileTransferManager":Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    invoke-virtual {v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->pullFile()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 667
    .end local v2    # "fileTransferManager":Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    .end local v15    # "logFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v20    # "t":Ljava/lang/Throwable;
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/app/framework/util/SupportTicketService$3;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    invoke-interface {v4}, Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;->onLogCollectionFailed()V

    goto :goto_3

    .line 573
    .end local v9    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v20    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v4

    goto/16 :goto_0
.end method
