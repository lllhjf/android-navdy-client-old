.class public final enum Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;
.super Ljava/lang/Enum;
.source "TTSAudioRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/TTSAudioRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AudioOutput"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

.field public static final enum BEST_AVAILABLE:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

.field public static final enum BLUETOOTH_MEDIA:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

.field public static final enum PHONE_SPEAKER:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 83
    new-instance v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    const-string v1, "BEST_AVAILABLE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->BEST_AVAILABLE:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    .line 84
    new-instance v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    const-string v1, "BLUETOOTH_MEDIA"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->BLUETOOTH_MEDIA:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    .line 85
    new-instance v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    const-string v1, "PHONE_SPEAKER"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->PHONE_SPEAKER:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    .line 82
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->BEST_AVAILABLE:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->BLUETOOTH_MEDIA:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->PHONE_SPEAKER:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->$VALUES:[Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 82
    const-class v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->$VALUES:[Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    return-object v0
.end method
