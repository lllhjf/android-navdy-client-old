.class public Lcom/navdy/client/app/framework/util/SupportTicketService;
.super Landroid/app/IntentService;
.source "SupportTicketService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;,
        Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;
    }
.end annotation


# static fields
.field public static final EXTRA_CREATE_INDETERMINATE_NOTIFICATION:Ljava/lang/String; = "create_indeterminate_notification"

.field public static final EXTRA_EMAIL:Ljava/lang/String; = "extra_email"

.field public static final EXTRA_RESET_COUNTER:Ljava/lang/String; = "reset_retry_counter"

.field public static final EXTRA_TICKET_RANDOM_ID:Ljava/lang/String; = "random_id"

.field private static final INTENT_SERVICE_NAME:Ljava/lang/String; = "SUBMIT_TICKET"

.field private static final MAX_FILE_SIZE:J = 0x200000L

.field public static final NEW_FOLDER_FORMAT:Ljava/lang/String; = "yyyy-MM-dd\'_\'HH:mm:ss.SSS"

.field private static final PULL_LOGS_TIMEOUT:J = 0x3a98L

.field private static final RETRY_INTERVAL_MILLIS:J

.field private static final RETRY_REQUEST_CODE:I = 0x1

.field private static final SHARED_PREF_TICKET_LAST_RETRY_DATE:Ljava/lang/String; = "ticket_last_retry_date"

.field private static final SHARED_PREF_TICKET_RETRY_COUNT:Ljava/lang/String; = "ticket_retry_count"

.field private static final SHARED_PREF_TICKET_RETRY_COUNT_DEFAULT:I = 0x0

.field private static final SHARED_PREF_TICKET_RETRY_DATE_DEFAULT:J = 0x0L

.field private static final SUBMIT_AFTER_INIT_DELAY:I = 0x2710

.field private static final SUPPORT_TICKET_SHARED_PREFS:Ljava/lang/String; = "support_ticket_shared_prefs"

.field private static final TEMP_FILE_TIMESTAMP_FORMAT:Ljava/lang/String; = "yyyy_dd_MM-hh_mm_ss_aa"

.field public static final TICKET_ATTACHMENTS_FILENAME:Ljava/lang/String; = "attachments.zip"

.field public static final TICKET_FILTER_FOR_READY_FOLDERS:Ljava/lang/String; = "_READY"

.field public static final TICKET_FOLDER_README_FILENAME:Ljava/lang/String; = "readme.txt"

.field private static final TIMER_REQUEST_CODE:I = 0x0

.field private static final TRY_MAX_COUNT:J = 0x4L

.field public static final VERBOSE:Ljava/lang/Boolean;

.field private static email:Ljava/lang/String;

.field public static final filterOnReadyFolders:Ljava/io/FilenameFilter;

.field private static final format:Ljava/text/SimpleDateFormat;

.field public static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private lastTicketId:Ljava/lang/String;

.field private submitTicketWorker:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 57
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    .line 58
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    .line 67
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2d

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->RETRY_INTERVAL_MILLIS:J

    .line 69
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy_dd_MM-hh_mm_ss_aa"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->format:Ljava/text/SimpleDateFormat;

    .line 99
    new-instance v0, Lcom/navdy/client/app/framework/util/SupportTicketService$1;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/SupportTicketService$1;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->filterOnReadyFolders:Ljava/io/FilenameFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 144
    const-string v1, "SUBMIT_TICKET"

    invoke-direct {p0, v1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 97
    const-string v1, ""

    iput-object v1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService;->lastTicketId:Ljava/lang/String;

    .line 146
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 148
    new-instance v1, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService;->submitTicketWorker:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    .line 150
    sget-object v1, Lcom/navdy/client/app/framework/util/SupportTicketService;->email:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 152
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v1, "email"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SupportTicketService;->setEmail(Ljava/lang/String;)V

    .line 154
    .end local v0    # "customerPrefs":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method static synthetic access$000()J
    .locals 2

    .prologue
    .line 56
    sget-wide v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->RETRY_INTERVAL_MILLIS:J

    return-wide v0
.end method

.method static synthetic access$100()Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->format:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/util/SupportTicketService;->dumpDbs(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/util/SupportTicketService;->dumpSharedPrefs(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized authenticateZendesk()V
    .locals 8

    .prologue
    .line 161
    monitor-enter p0

    :try_start_0
    sget-object v6, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 162
    sget-object v6, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "authenticateZendesk"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 168
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f08055c

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 169
    .local v4, "zendeskAppId":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f08055d

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 174
    .local v5, "zendeskOAuth":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 175
    .local v1, "context":Landroid/content/Context;
    sget-object v6, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    const v7, 0x7f080514

    .line 176
    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 175
    invoke-virtual {v6, v1, v7, v4, v5}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    sget-object v6, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v6}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isInitialized()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 181
    sget-object v6, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Zendesk isInitialized == true"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 184
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 187
    .local v2, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v6, "fname"

    const-string v7, ""

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 189
    .local v3, "fullName":Ljava/lang/String;
    new-instance v6, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    invoke-direct {v6}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;-><init>()V

    invoke-virtual {v6, v3}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->withNameIdentifier(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/client/app/framework/util/SupportTicketService;->email:Ljava/lang/String;

    .line 190
    invoke-virtual {v6, v7}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->withEmailIdentifier(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    move-result-object v6

    .line 191
    invoke-virtual {v6}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->build()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v0

    .line 192
    .local v0, "anonymousIdentity":Lcom/zendesk/sdk/model/access/Identity;
    sget-object v6, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v6, v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->setIdentity(Lcom/zendesk/sdk/model/access/Identity;)V

    .line 196
    const-wide/16 v6, 0x2710

    invoke-static {v6, v7}, Lcom/navdy/client/app/framework/util/SupportTicketService;->scheduleAlarmForSubmittingPendingTickets(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    .end local v0    # "anonymousIdentity":Lcom/zendesk/sdk/model/access/Identity;
    .end local v2    # "customerPrefs":Landroid/content/SharedPreferences;
    .end local v3    # "fullName":Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return-void

    .line 198
    :cond_1
    :try_start_1
    sget-object v6, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Zendesk initialization failed"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 199
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/framework/util/SupportTicketService;->finishService(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 161
    .end local v1    # "context":Landroid/content/Context;
    .end local v4    # "zendeskAppId":Ljava/lang/String;
    .end local v5    # "zendeskOAuth":Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public static collectHudLog(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V
    .locals 3
    .param p0, "onLogCollectedInterface"    # Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    .prologue
    .line 674
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/util/SupportTicketService$4;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/util/SupportTicketService$4;-><init>(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 741
    return-void
.end method

.method public static collectLogs(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V
    .locals 3
    .param p0, "onLogCollectedInterface"    # Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    .prologue
    .line 554
    sget-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    sget-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "collectLogsAndThen"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 557
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/util/SupportTicketService$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/util/SupportTicketService$3;-><init>(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 671
    return-void
.end method

.method public static createFolderForTicket(Ljava/util/ArrayList;Lorg/json/JSONObject;)Z
    .locals 12
    .param p1, "jsonObjectForTicketContents"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lorg/json/JSONObject;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 517
    .local p0, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    sget-object v9, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "jsonObject: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 518
    if-nez p1, :cond_1

    .line 519
    sget-object v9, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "jsonObject unsuccessfully created"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 520
    const/4 v5, 0x0

    .line 550
    :cond_0
    :goto_0
    return v5

    .line 523
    :cond_1
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v1, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 524
    .local v1, "currentTime":Ljava/util/Date;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v9, "yyyy-MM-dd\'_\'HH:mm:ss.SSS"

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 525
    .local v3, "folderDateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 526
    .local v4, "folderName":Ljava/lang/String;
    sget-object v9, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 527
    sget-object v9, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "new foldername: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 530
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v6

    .line 531
    .local v6, "pathManager":Lcom/navdy/client/app/framework/PathManager;
    new-instance v7, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/PathManager;->getTicketFolderPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 532
    .local v7, "unfininishedDirectory":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    move-result v5

    .line 534
    .local v5, "folderSuccessfullyCreated":Z
    if-eqz v5, :cond_4

    .line 535
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Ljava/io/File;

    invoke-virtual {p0, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/io/File;

    .line 536
    .local v0, "basicAttachmentArray":[Ljava/io/File;
    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 537
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "attachments.zip"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 538
    .local v8, "zipFile":Ljava/io/File;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v0, v10}, Lcom/navdy/service/library/util/IOUtils;->compressFilesToZip(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V

    .line 540
    .end local v8    # "zipFile":Ljava/io/File;
    :cond_3
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-static {p1, v9, v10}, Lcom/navdy/client/app/framework/util/SupportTicketService;->writeJsonObjectToFile(Lorg/json/JSONObject;Ljava/lang/String;Lcom/navdy/service/library/log/Logger;)V

    .line 542
    new-instance v2, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_READY"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 543
    .local v2, "finishedDirectory":Ljava/io/File;
    invoke-virtual {v7, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 544
    sget-object v9, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "rename worked: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 548
    .end local v0    # "basicAttachmentArray":[Ljava/io/File;
    .end local v2    # "finishedDirectory":Ljava/io/File;
    :cond_4
    sget-object v9, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "folder failed"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static declared-synchronized dumpDbs(Landroid/content/Context;Ljava/lang/String;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 745
    const-class v9, Lcom/navdy/client/app/framework/util/SupportTicketService;

    monitor-enter v9

    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 746
    .local v6, "backupDB":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 747
    invoke-static {p0, p1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 749
    :cond_0
    const/4 v1, 0x0

    .line 750
    .local v1, "src":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    .line 752
    .local v0, "dst":Ljava/nio/channels/FileChannel;
    :try_start_1
    const-string v2, "navdy"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    .line 754
    .local v7, "currentDB":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v4, 0x200000

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 755
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DB too big: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 766
    :goto_0
    :try_start_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 767
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 769
    .end local v7    # "currentDB":Ljava/io/File;
    :goto_1
    monitor-exit v9

    return-void

    .line 756
    .restart local v7    # "currentDB":Ljava/io/File;
    :cond_1
    :try_start_3
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 757
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 758
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 759
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 763
    .end local v7    # "currentDB":Ljava/io/File;
    :catch_0
    move-exception v8

    .line 764
    .local v8, "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to pull the DB from the phone: "

    invoke-virtual {v2, v3, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 766
    :try_start_5
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 767
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 745
    .end local v0    # "dst":Ljava/nio/channels/FileChannel;
    .end local v1    # "src":Ljava/nio/channels/FileChannel;
    .end local v6    # "backupDB":Ljava/io/File;
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit v9

    throw v2

    .line 761
    .restart local v0    # "dst":Ljava/nio/channels/FileChannel;
    .restart local v1    # "src":Ljava/nio/channels/FileChannel;
    .restart local v6    # "backupDB":Ljava/io/File;
    .restart local v7    # "currentDB":Ljava/io/File;
    :cond_2
    :try_start_6
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid DB path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 766
    .end local v7    # "currentDB":Ljava/io/File;
    :catchall_1
    move-exception v2

    :try_start_7
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 767
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method private static declared-synchronized dumpSharedPrefs(Landroid/content/Context;Ljava/lang/String;)V
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "destinationFolderPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 772
    const-class v15, Lcom/navdy/client/app/framework/util/SupportTicketService;

    monitor-enter v15

    :try_start_0
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 773
    .local v9, "destinationFolder":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 774
    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_1

    .line 775
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to create the destination folder: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 815
    :cond_0
    :goto_0
    monitor-exit v15

    return-void

    .line 787
    :cond_1
    :try_start_1
    new-instance v13, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/shared_prefs/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v13, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 789
    .local v13, "sharedPrefsFolder":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v11

    .line 791
    .local v11, "listOfSharedPrefsFiles":[Ljava/io/File;
    if-eqz v11, :cond_0

    .line 792
    array-length v0, v11

    move/from16 v16, v0

    const/4 v4, 0x0

    move v14, v4

    :goto_1
    move/from16 v0, v16

    if-ge v14, v0, :cond_0

    aget-object v12, v11, v14

    .line 793
    .local v12, "sharedPrefsFile":Ljava/io/File;
    if-eqz v12, :cond_2

    .line 794
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 795
    invoke-virtual {v12}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 796
    new-instance v8, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SharedPref_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 797
    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v8, v9, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 799
    .local v8, "backupFile":Ljava/io/File;
    const/4 v3, 0x0

    .line 800
    .local v3, "src":Ljava/nio/channels/FileChannel;
    const/4 v2, 0x0

    .line 802
    .local v2, "dst":Ljava/nio/channels/FileChannel;
    :try_start_2
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v4}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 803
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 804
    const-wide/16 v4, 0x0

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 806
    :try_start_3
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 807
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 792
    .end local v2    # "dst":Ljava/nio/channels/FileChannel;
    .end local v3    # "src":Ljava/nio/channels/FileChannel;
    .end local v8    # "backupFile":Ljava/io/File;
    :cond_2
    add-int/lit8 v4, v14, 0x1

    move v14, v4

    goto :goto_1

    .line 806
    .restart local v2    # "dst":Ljava/nio/channels/FileChannel;
    .restart local v3    # "src":Ljava/nio/channels/FileChannel;
    .restart local v8    # "backupFile":Ljava/io/File;
    :catchall_0
    move-exception v4

    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 807
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 812
    .end local v2    # "dst":Ljava/nio/channels/FileChannel;
    .end local v3    # "src":Ljava/nio/channels/FileChannel;
    .end local v8    # "backupFile":Ljava/io/File;
    .end local v11    # "listOfSharedPrefsFiles":[Ljava/io/File;
    .end local v12    # "sharedPrefsFile":Ljava/io/File;
    .end local v13    # "sharedPrefsFolder":Ljava/io/File;
    :catch_0
    move-exception v10

    .line 813
    .local v10, "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Unable to pull the shared prefs files from the phone: "

    invoke-virtual {v4, v5, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 772
    .end local v9    # "destinationFolder":Ljava/io/File;
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v4

    monitor-exit v15

    throw v4
.end method

.method public static declared-synchronized getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 505
    const-class v0, Lcom/navdy/client/app/framework/util/SupportTicketService;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/navdy/client/app/framework/util/SupportTicketService;->email:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getSupportTicketPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 328
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "support_ticket_shared_prefs"

    const/4 v2, 0x0

    .line 329
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static isBelowMaxRetryCount()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 457
    sget-object v3, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 458
    sget-object v3, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "isBelowMaxRetryCount"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 460
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getSupportTicketPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 461
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v3, "ticket_retry_count"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 462
    .local v0, "retryCount":I
    sget-object v3, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "retryCount: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 463
    int-to-long v4, v0

    const-wide/16 v6, 0x4

    cmp-long v3, v4, v6

    if-gtz v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method public static lastRetryDateIsOldEnough()Z
    .locals 8

    .prologue
    .line 486
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getSupportTicketPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 487
    .local v3, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v4, "ticket_last_retry_date"

    const-wide/16 v6, 0x0

    invoke-interface {v3, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 488
    .local v0, "lastRetryDateInMilliseconds":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-wide v6, Lcom/navdy/client/app/framework/util/SupportTicketService;->RETRY_INTERVAL_MILLIS:J

    sub-long/2addr v4, v6

    cmp-long v4, v4, v0

    if-lez v4, :cond_1

    const/4 v2, 0x1

    .line 489
    .local v2, "lastRetryDateIsOldEnough":Z
    :goto_0
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 490
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "lastRetryDateIsOldEnough: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 492
    :cond_0
    return v2

    .line 488
    .end local v2    # "lastRetryDateIsOldEnough":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static resetRetryCount()V
    .locals 6

    .prologue
    .line 446
    sget-object v1, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 447
    sget-object v1, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "resetRetryCount"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 449
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getSupportTicketPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 450
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ticket_retry_count"

    const/4 v3, 0x0

    .line 451
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ticket_last_retry_date"

    const-wide/16 v4, 0x0

    .line 452
    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 453
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 454
    return-void
.end method

.method public static scheduleAlarmForSubmittingPendingTickets(J)V
    .locals 8
    .param p0, "interval"    # J

    .prologue
    .line 333
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isLimitingCellularData()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 334
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "app is limiting cellular data, won\'t schedule alarm for submitting pending tickets"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 346
    :goto_0
    return-void

    .line 338
    :cond_0
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 339
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "scheduleAlarmForSubmittingPendingTickets retry coming in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " minutes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 341
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 342
    .local v1, "context":Landroid/content/Context;
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-direct {v2, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 343
    .local v2, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    const/high16 v6, 0x10000000

    invoke-static {v4, v5, v2, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 344
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    const-string v4, "alarm"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 345
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    const/4 v4, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long/2addr v6, p0

    invoke-virtual {v0, v4, v6, v7, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method static declared-synchronized setEmail(Ljava/lang/String;)V
    .locals 2
    .param p0, "email"    # Ljava/lang/String;

    .prologue
    .line 509
    const-class v0, Lcom/navdy/client/app/framework/util/SupportTicketService;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lcom/navdy/client/app/framework/util/SupportTicketService;->email:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    monitor-exit v0

    return-void

    .line 509
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static startSupportTicketService(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 496
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "pending_tickets_exist"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 498
    .local v1, "pendingTicketsExist":Z
    if-eqz v1, :cond_0

    .line 499
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 500
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 502
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private declared-synchronized submitPendingTickets()V
    .locals 7

    .prologue
    .line 270
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "submitPendingTickets"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 271
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v1

    .line 272
    .local v1, "pathManager":Lcom/navdy/client/app/framework/PathManager;
    new-instance v3, Ljava/io/File;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/PathManager;->getTicketFolderPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 273
    .local v3, "ticketsDirectory":Ljava/io/File;
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->filterOnReadyFolders:Ljava/io/FilenameFilter;

    invoke-virtual {v3, v4}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 275
    .local v2, "readyFolders":[Ljava/io/File;
    if-eqz v2, :cond_0

    array-length v4, v2

    if-gtz v4, :cond_1

    .line 276
    :cond_0
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "There are no pending tickets found"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/SupportTicketService;->endTicketNotifications()V

    .line 278
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/framework/util/SupportTicketService;->finishService(Z)V

    .line 281
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/app/framework/util/SupportTicketService;->submitTicketWorker:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    new-instance v5, Lcom/navdy/client/app/framework/util/SupportTicketService$2;

    invoke-direct {v5, p0, v3}, Lcom/navdy/client/app/framework/util/SupportTicketService$2;-><init>(Lcom/navdy/client/app/framework/util/SupportTicketService;Ljava/io/File;)V

    invoke-virtual {v4, v5, v2}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->start(Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;[Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    .end local v1    # "pathManager":Lcom/navdy/client/app/framework/PathManager;
    .end local v2    # "readyFolders":[Ljava/io/File;
    .end local v3    # "ticketsDirectory":Ljava/io/File;
    :goto_0
    monitor-exit p0

    return-void

    .line 320
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "There was a problem submitting pending tickets: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 270
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public static submitTicketsIfConditionsAreMet()V
    .locals 8

    .prologue
    .line 818
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    .line 819
    .local v2, "isReachable":Z
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "pending_tickets_exist"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 820
    .local v4, "pendingTicketsExist":Z
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->lastRetryDateIsOldEnough()Z

    move-result v3

    .line 821
    .local v3, "lastRetryDateIsOldEnough":Z
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->isBelowMaxRetryCount()Z

    move-result v1

    .line 823
    .local v1, "isBelowMaxRetryCount":Z
    sget-object v5, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 824
    sget-object v5, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isReachable: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n pendingTicketExists: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n lastRetryDateIsOldEnough: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n isBelowMaxRetryCount: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 829
    :cond_0
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    if-eqz v4, :cond_2

    .line 833
    sget-object v5, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 834
    sget-object v5, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Should submit tickets"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 837
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/SupportTicketService;->startSupportTicketService(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 842
    :cond_2
    :goto_0
    return-void

    .line 838
    :catch_0
    move-exception v0

    .line 839
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception found: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static writeJsonObjectToFile(Lorg/json/JSONObject;Ljava/lang/String;Lcom/navdy/service/library/log/Logger;)V
    .locals 5
    .param p0, "jsonObject"    # Lorg/json/JSONObject;
    .param p1, "directory"    # Ljava/lang/String;
    .param p2, "logger"    # Lcom/navdy/service/library/log/Logger;

    .prologue
    .line 107
    const/4 v1, 0x0

    .line 109
    .local v1, "fileWriter":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ticket_contents.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    .end local v1    # "fileWriter":Ljava/io/FileWriter;
    .local v2, "fileWriter":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v2}, Ljava/io/FileWriter;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 116
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 118
    .end local v2    # "fileWriter":Ljava/io/FileWriter;
    .restart local v1    # "fileWriter":Ljava/io/FileWriter;
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to write JSON to file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 116
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    :goto_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3

    .end local v1    # "fileWriter":Ljava/io/FileWriter;
    .restart local v2    # "fileWriter":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/FileWriter;
    .restart local v1    # "fileWriter":Ljava/io/FileWriter;
    goto :goto_2

    .line 113
    .end local v1    # "fileWriter":Ljava/io/FileWriter;
    .restart local v2    # "fileWriter":Ljava/io/FileWriter;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/FileWriter;
    .restart local v1    # "fileWriter":Ljava/io/FileWriter;
    goto :goto_1
.end method


# virtual methods
.method public buildNotificationForIndeterminateProgress()V
    .locals 7

    .prologue
    .line 418
    sget-object v3, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 419
    sget-object v3, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "buildNotificationForIndeterminateProgress"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 421
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 422
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0200fd

    .line 423
    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    const v4, 0x7f080513

    .line 424
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    const/16 v4, 0x64

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 425
    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 427
    .local v1, "notificationBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 428
    .local v2, "notificationManager":Landroid/app/NotificationManager;
    if-eqz v2, :cond_1

    .line 429
    const/4 v3, 0x3

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 431
    :cond_1
    return-void
.end method

.method public buildNotificationForRetry()V
    .locals 18

    .prologue
    .line 349
    sget-object v13, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 350
    sget-object v13, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v14, "buildNotificationForRetry"

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 352
    :cond_0
    const/4 v3, 0x0

    .line 353
    .local v3, "failureCount":I
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 354
    .local v2, "context":Landroid/content/Context;
    new-instance v6, Landroid/content/Intent;

    const-class v13, Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-direct {v6, v2, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 355
    .local v6, "intent":Landroid/content/Intent;
    const-string v13, "reset_retry_counter"

    const/4 v14, 0x1

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 356
    const-string v13, "create_indeterminate_notification"

    const/4 v14, 0x1

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 357
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v13

    const/4 v14, 0x1

    const/high16 v15, 0x8000000

    invoke-static {v13, v14, v6, v15}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    .line 359
    .local v10, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v12, Ljava/io/File;

    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/navdy/client/app/framework/PathManager;->getTicketFolderPath()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 360
    .local v12, "ticketsDirectory":Ljava/io/File;
    sget-object v13, Lcom/navdy/client/app/framework/util/SupportTicketService;->filterOnReadyFolders:Ljava/io/FilenameFilter;

    invoke-virtual {v12, v13}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v5

    .line 362
    .local v5, "folders":[Ljava/io/File;
    if-eqz v5, :cond_3

    .line 363
    array-length v14, v5

    const/4 v13, 0x0

    :goto_0
    if-ge v13, v14, :cond_3

    aget-object v11, v5, v13

    .line 364
    .local v11, "ticketFolder":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "ticket_contents.txt"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 365
    .local v1, "content":Ljava/io/File;
    sget-object v15, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v15}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 366
    sget-object v15, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "content filepath: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "ticket_contents.txt"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 368
    :cond_1
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->getJSONObjectFromFile(Ljava/io/File;)Lorg/json/JSONObject;

    move-result-object v7

    .line 369
    .local v7, "jsonObject":Lorg/json/JSONObject;
    if-eqz v7, :cond_2

    .line 371
    const-string v15, "ticket_action"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "fetch_hud_log_upload_delete"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 372
    add-int/lit8 v3, v3, 0x1

    .line 363
    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 378
    .end local v1    # "content":Ljava/io/File;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .end local v11    # "ticketFolder":Ljava/io/File;
    :cond_3
    if-lez v3, :cond_6

    .line 379
    sget-object v13, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_4

    .line 380
    sget-object v13, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "failureCount: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 382
    :cond_4
    const v13, 0x7f080512

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v2, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 384
    .local v4, "failureTitle":Ljava/lang/String;
    new-instance v13, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v13, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v14, 0x7f0200fd

    .line 385
    invoke-virtual {v13, v14}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v13

    .line 386
    invoke-virtual {v13, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v13

    const v14, 0x7f08050b

    .line 387
    invoke-virtual {v2, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v13

    .line 388
    invoke-virtual {v13, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    .line 390
    .local v8, "notificationBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    const-string v13, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/NotificationManager;

    .line 391
    .local v9, "notificationManager":Landroid/app/NotificationManager;
    if-eqz v9, :cond_5

    .line 392
    const/4 v13, 0x2

    invoke-virtual {v8}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v14

    invoke-virtual {v9, v13, v14}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 399
    .end local v4    # "failureTitle":Ljava/lang/String;
    .end local v8    # "notificationBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v9    # "notificationManager":Landroid/app/NotificationManager;
    :cond_5
    :goto_1
    return-void

    .line 395
    :cond_6
    sget-object v13, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 396
    sget-object v13, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v14, "no failures found. Not building notification for retry"

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public buildNotificationForSuccess()V
    .locals 5

    .prologue
    .line 402
    sget-object v3, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 403
    sget-object v3, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "buildNotificationForSuccess"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 405
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 407
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0200fd

    .line 408
    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    const v4, 0x7f0804a9

    .line 409
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 411
    .local v1, "notificationBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 412
    .local v2, "notificationManager":Landroid/app/NotificationManager;
    if-eqz v2, :cond_1

    .line 413
    const/4 v3, 0x4

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 415
    :cond_1
    return-void
.end method

.method public endTicketNotifications()V
    .locals 3

    .prologue
    .line 434
    sget-object v1, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 435
    sget-object v1, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "endTicketNotifications"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 437
    :cond_0
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 438
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    if-eqz v0, :cond_1

    .line 439
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 440
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 442
    :cond_1
    return-void
.end method

.method public finishService(Z)V
    .locals 2
    .param p1, "isSuccess"    # Z

    .prologue
    .line 848
    new-instance v0, Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService;->lastTicketId:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;-><init>(ZLjava/lang/String;)V

    .line 849
    .local v0, "finishedEvent":Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 850
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/SupportTicketService;->stopSelf()V

    .line 851
    return-void
.end method

.method public incrementTryCount()V
    .locals 5

    .prologue
    .line 468
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getSupportTicketPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 469
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "ticket_retry_count"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 470
    .local v0, "retryCount":I
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "incrementTryCount retryCount: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 471
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ticket_retry_count"

    add-int/lit8 v4, v0, 0x1

    .line 472
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 473
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 474
    return-void
.end method

.method public incrementUpdateDate()V
    .locals 4

    .prologue
    .line 477
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getSupportTicketPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ticket_last_retry_date"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 478
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 479
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 480
    return-void
.end method

.method protected declared-synchronized onHandleIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "SupportTicketService handling intent"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 208
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveStoragePermission()Z

    move-result v2

    if-nez v2, :cond_0

    .line 209
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "We do not have storage permission"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 210
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/framework/util/SupportTicketService;->finishService(Z)V

    .line 213
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 214
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Network is not accessible. Scheduling another alarm for the future..."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 217
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-lt v2, v3, :cond_2

    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isLimitingCellularData()Z

    move-result v2

    if-nez v2, :cond_2

    .line 218
    invoke-static {}, Lcom/navdy/client/app/framework/service/NetworkConnectivityJobService;->scheduleNetworkServiceForNougatOrAbove()V

    .line 220
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/SupportTicketService;->buildNotificationForRetry()V

    .line 221
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/framework/util/SupportTicketService;->finishService(Z)V

    .line 224
    :cond_3
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "pending_tickets_exist"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 225
    .local v1, "pendingTicketsExist":Z
    if-nez v1, :cond_4

    .line 226
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "pending tickets currently do not exist"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 227
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/framework/util/SupportTicketService;->finishService(Z)V

    .line 230
    :cond_4
    const-string v2, "reset_retry_counter"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 231
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 232
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reset boolean extra found"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 234
    :cond_5
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->resetRetryCount()V

    .line 237
    :cond_6
    const-string v2, "create_indeterminate_notification"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 238
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/SupportTicketService;->buildNotificationForIndeterminateProgress()V

    .line 241
    :cond_7
    const-string v2, "random_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 242
    const-string v2, "random_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService;->lastTicketId:Ljava/lang/String;

    .line 248
    :cond_8
    const-string v2, "extra_email"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    .local v0, "extraEmail":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 250
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 251
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "extra intent email found: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 253
    :cond_9
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SupportTicketService;->setEmail(Ljava/lang/String;)V

    .line 256
    :cond_a
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onHandleIntent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 257
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService;->submitTicketWorker:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->isRunning()Z

    move-result v2

    if-nez v2, :cond_c

    .line 258
    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 259
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/SupportTicketService;->submitPendingTickets()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    :goto_0
    monitor-exit p0

    return-void

    .line 261
    :cond_b
    :try_start_1
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/SupportTicketService;->authenticateZendesk()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 206
    .end local v0    # "extraEmail":Ljava/lang/String;
    .end local v1    # "pendingTicketsExist":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 264
    .restart local v0    # "extraEmail":Ljava/lang/String;
    .restart local v1    # "pendingTicketsExist":Z
    :cond_c
    :try_start_2
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "SupportTicketService is already running. Avoiding a duplicate call to intent service."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
