.class Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;
.super Ljava/lang/Object;
.source "HereGeocoder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;-><init>(Lcom/here/android/mpa/search/ResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;

    .prologue
    .line 211
    .local p0, "this":Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;, "Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;"
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;->this$0:Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 214
    .local p0, "this":Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;, "Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;"
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;->this$0:Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;

    monitor-enter v1

    .line 215
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;->this$0:Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;

    iget-boolean v0, v0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->complete:Z

    if-eqz v0, :cond_0

    .line 216
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v2, "Geocode call already complete"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 224
    :goto_0
    monitor-exit v1

    .line 225
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;->this$0:Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->cancelled:Z

    .line 219
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;->this$0:Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;

    invoke-static {v0}, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->access$300(Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;)Lcom/here/android/mpa/search/ResultListener;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Lcom/here/android/mpa/search/ErrorCode;->CANCELLED:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface {v0, v2, v3}, Lcom/here/android/mpa/search/ResultListener;->onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V

    .line 221
    const-string v0, "Debug_Here_Geocode_Timeout"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 222
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v2, "Geocode call timed out"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
