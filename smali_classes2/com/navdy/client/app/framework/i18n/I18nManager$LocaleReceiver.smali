.class Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;
.super Landroid/content/BroadcastReceiver;
.source "I18nManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/i18n/I18nManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocaleReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/i18n/I18nManager;


# direct methods
.method private constructor <init>(Lcom/navdy/client/app/framework/i18n/I18nManager;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;->this$0:Lcom/navdy/client/app/framework/i18n/I18nManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/app/framework/i18n/I18nManager;Lcom/navdy/client/app/framework/i18n/I18nManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/app/framework/i18n/I18nManager;
    .param p2, "x1"    # Lcom/navdy/client/app/framework/i18n/I18nManager$1;

    .prologue
    .line 161
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;-><init>(Lcom/navdy/client/app/framework/i18n/I18nManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 164
    if-nez p2, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.LOCALE_CHANGED"

    invoke-static {v0, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    .line 171
    .local v1, "localeChanged":Z
    if-eqz v1, :cond_0

    .line 172
    invoke-static {}, Lcom/navdy/client/app/framework/i18n/I18nManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Locale has changed, new locale is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 173
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-result-object v2

    .line 175
    .local v2, "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    if-nez v2, :cond_2

    .line 180
    iget-object v3, p0, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;->this$0:Lcom/navdy/client/app/framework/i18n/I18nManager;

    invoke-static {v3}, Lcom/navdy/client/app/framework/i18n/I18nManager;->access$300(Lcom/navdy/client/app/framework/i18n/I18nManager;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 181
    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_IMPERIAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 186
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/framework/i18n/I18nManager;->access$400()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 187
    :try_start_0
    iget-object v3, p0, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;->this$0:Lcom/navdy/client/app/framework/i18n/I18nManager;

    invoke-static {v3, v2}, Lcom/navdy/client/app/framework/i18n/I18nManager;->access$102(Lcom/navdy/client/app/framework/i18n/I18nManager;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 189
    iget-object v3, p0, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;->this$0:Lcom/navdy/client/app/framework/i18n/I18nManager;

    new-instance v5, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver$1;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver$1;-><init>(Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;)V

    invoke-static {v3, v5}, Lcom/navdy/client/app/framework/i18n/I18nManager;->access$500(Lcom/navdy/client/app/framework/i18n/I18nManager;Lcom/navdy/client/app/framework/i18n/I18nManager$CallListener;)V

    .line 195
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :cond_2
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersionAndSendToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    .line 202
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver$2;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver$2;-><init>(Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;)V

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 183
    :cond_3
    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_METRIC:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    goto :goto_1

    .line 195
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method
