.class final Lcom/navdy/client/app/framework/glances/GlanceConstants$1;
.super Ljava/util/HashMap;
.source "GlanceConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/glances/GlanceConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;",
        "Ljava/util/Set",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 101
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->DRIVING_GLANCES:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->WHITE_LIST:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->EMAIL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MESSAGING_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->CALENDAR_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->SOCIAL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MUSIC_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->IGNORE_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    return-void
.end method
