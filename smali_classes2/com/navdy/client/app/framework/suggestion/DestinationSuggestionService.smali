.class public Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;
.super Landroid/app/IntentService;
.source "DestinationSuggestionService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;
    }
.end annotation


# static fields
.field private static final BUCKET_NAME:Ljava/lang/String; = "navdy-trip-data"

.field public static final COMMAND_AUTO_UPLOAD_TRIP_DATA:Ljava/lang/String; = "COMMAND_AUTO_UPLOAD"

.field public static final COMMAND_POPULATE:Ljava/lang/String; = "COMMAND_POPULATE"

.field public static final COMMAND_RESET:Ljava/lang/String; = "COMMAND_RESET"

.field public static final COMMAND_SUGGEST:Ljava/lang/String; = "COMMAND_SUGGEST"

.field public static final COMMAND_UPLOAD_TRIP_DATA:Ljava/lang/String; = "COMMAND_UPLOAD"

.field private static final EXTRA_LOCAL:Ljava/lang/String; = "EXTRA_LOCAL"

.field private static final MINIMUM_INTERVAL_BETWEEN_UPLOADS:J

.field private static NO_SUGGESTION:Lcom/navdy/client/app/framework/models/Destination; = null

.field public static final PLACE_SUGGESTION_LIBRARY_NAME:Ljava/lang/String; = "placesuggestion"

.field private static final PREFERENCE_LAST_UPLOAD_ID:Ljava/lang/String; = "PREF_LAST_UPLOAD_ID"

.field private static final PREFERENCE_LAST_UPLOAD_TIME:Ljava/lang/String; = "PREF_LAST_UPLOAD_TIME"

.field private static final SERVICE_NAME:Ljava/lang/String; = "SERVICE_NAME"

.field public static final SUGGESTED_DESTINATION_TRESHOLD:I = 0xc8

.field public static final TRIP_TRESHOLD_FOR_UPLOAD:I = 0xa

.field public static final TRIP_UPLOAD_SERVICE_REQ:I = 0x100

.field private static sColdStart:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static sMaxIdentifier:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 80
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->MINIMUM_INTERVAL_BETWEEN_UPLOADS:J

    .line 87
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sColdStart:Z

    .line 88
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sMaxIdentifier:J

    .line 89
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 90
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->NO_SUGGESTION:Lcom/navdy/client/app/framework/models/Destination;

    .line 93
    const-string v0, "placesuggestion"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 94
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->NO_SUGGESTION:Lcom/navdy/client/app/framework/models/Destination;

    .line 95
    sget-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->NO_SUGGESTION:Lcom/navdy/client/app/framework/models/Destination;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->setId(I)V

    .line 96
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 104
    const-string v0, "SERVICE_NAME"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Ljava/util/ArrayList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->firstSuggestionHasValidDestination(Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .param p2, "x2"    # Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sendSuggestionToHud(Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->suggestDestinationBasedOnContext(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Lcom/navdy/service/library/events/places/SuggestedDestination;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;
    .param p1, "x1"    # Lcom/navdy/service/library/events/places/SuggestedDestination;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sendSuggestion(Lcom/navdy/service/library/events/places/SuggestedDestination;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->scheduleNextCheckForUpdate()V

    return-void
.end method

.method static synthetic access$500()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private firstSuggestionHasValidDestination(Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    const/4 v1, 0x0

    .line 188
    if-eqz p1, :cond_0

    .line 189
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 190
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasOneValidSetOfCoordinates()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private getTripCursor()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 622
    sget-boolean v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sColdStart:Z

    if-eqz v0, :cond_0

    .line 623
    sget-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "cold start, getting all the trips for learning"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 624
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 627
    :goto_0
    return-object v0

    .line 626
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "warm start, getting trips greater than "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v2, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sMaxIdentifier:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 627
    sget-wide v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sMaxIdentifier:J

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->getTripCursor(J)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method private getTripCursor(J)Landroid/database/Cursor;
    .locals 5
    .param p1, "maxId"    # J

    .prologue
    .line 632
    const-string v1, "_id > ?"

    .line 633
    .local v1, "selection":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 634
    .local v0, "args":[Ljava/lang/String;
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private handleAutoTripUpload()V
    .locals 28

    .prologue
    .line 388
    sget-object v5, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "handleAutoTripUpload : checking if we need an auto upload"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 389
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    .line 390
    .local v7, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v5, "PREF_LAST_UPLOAD_TIME"

    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    invoke-interface {v7, v5, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v18

    .line 391
    .local v18, "lastUploadTimeStamp":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 392
    .local v12, "currentTime":J
    const-string v5, "PREF_LAST_UPLOAD_ID"

    const-wide/16 v24, -0x1

    move-wide/from16 v0, v24

    invoke-interface {v7, v5, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v20

    .line 393
    .local v20, "lastUploadedTripId":J
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getMaxTripId()J

    move-result-wide v8

    .line 394
    .local v8, "maxTripId":J
    cmp-long v5, v8, v20

    if-gez v5, :cond_4

    const/4 v11, 0x1

    .line 395
    .local v11, "dataBaseChanged":Z
    :goto_0
    if-eqz v11, :cond_0

    .line 396
    sget-object v5, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Database has changed"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 398
    :cond_0
    if-nez v11, :cond_2

    sub-long v24, v12, v18

    sget-wide v26, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->MINIMUM_INTERVAL_BETWEEN_UPLOADS:J

    cmp-long v5, v24, v26

    if-lez v5, :cond_1

    const-wide/16 v24, -0x1

    cmp-long v5, v20, v24

    if-eqz v5, :cond_2

    cmp-long v5, v20, v8

    if-ltz v5, :cond_2

    :cond_1
    const-wide/16 v24, 0xa

    add-long v24, v24, v20

    cmp-long v5, v8, v24

    if-lez v5, :cond_3

    .line 399
    :cond_2
    sget-object v5, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Uploading the trip DB"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 400
    const/16 v22, 0x0

    .line 402
    .local v22, "tripCursor":Landroid/database/Cursor;
    const-wide/16 v24, 0x0

    cmp-long v5, v20, v24

    if-lez v5, :cond_5

    :try_start_0
    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->getTripCursor(J)Landroid/database/Cursor;

    move-result-object v22

    .line 403
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 404
    .local v4, "customPreferences":Landroid/content/SharedPreferences;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fname"

    const-string v24, "UNKNOWN"

    move-object/from16 v0, v24

    invoke-interface {v4, v6, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 405
    .local v16, "fullName":Ljava/lang/String;
    const-string v5, "[^a-zA-Z0-9.-]"

    const-string v6, "_"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 406
    new-instance v17, Ljava/text/SimpleDateFormat;

    const-string v5, "_MM_dd_yyyy_HH_mm_ss_SSS"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v17

    invoke-direct {v0, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 407
    .local v17, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v14, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-direct {v14, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 408
    .local v14, "date":Ljava/util/Date;
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    .line 409
    .local v15, "fileNameSuffix":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_Android_V"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x6b6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".csv"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 410
    .local v23, "uniqueName":Ljava/lang/String;
    sget-object v5, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Uploading the file to S3, Key : "

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 411
    new-instance v10, Lcom/squareup/otto/Bus;

    sget-object v5, Lcom/squareup/otto/ThreadEnforcer;->ANY:Lcom/squareup/otto/ThreadEnforcer;

    invoke-direct {v10, v5}, Lcom/squareup/otto/Bus;-><init>(Lcom/squareup/otto/ThreadEnforcer;)V

    .line 412
    .local v10, "bus":Lcom/squareup/otto/Bus;
    new-instance v5, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;

    move-object/from16 v6, p0

    invoke-direct/range {v5 .. v10}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;-><init>(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Landroid/content/SharedPreferences;JLcom/squareup/otto/Bus;)V

    invoke-virtual {v10, v5}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 426
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v10}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->uploadToS3(Ljava/lang/String;Landroid/database/Cursor;Lcom/squareup/otto/Bus;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    invoke-static/range {v22 .. v22}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 430
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->scheduleNextCheckForUpdate()V

    .line 433
    .end local v4    # "customPreferences":Landroid/content/SharedPreferences;
    .end local v10    # "bus":Lcom/squareup/otto/Bus;
    .end local v14    # "date":Ljava/util/Date;
    .end local v15    # "fileNameSuffix":Ljava/lang/String;
    .end local v16    # "fullName":Ljava/lang/String;
    .end local v17    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    .end local v22    # "tripCursor":Landroid/database/Cursor;
    .end local v23    # "uniqueName":Ljava/lang/String;
    :cond_3
    return-void

    .line 394
    .end local v11    # "dataBaseChanged":Z
    :cond_4
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 402
    .restart local v11    # "dataBaseChanged":Z
    .restart local v22    # "tripCursor":Landroid/database/Cursor;
    :cond_5
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsCursor()Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v22

    goto/16 :goto_1

    .line 428
    :catchall_0
    move-exception v5

    invoke-static/range {v22 .. v22}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v5
.end method

.method private handlePopulate()V
    .locals 61

    .prologue
    .line 316
    new-instance v50, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 317
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "data.csv"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v50

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 318
    .local v50, "destinationsFile":Ljava/io/File;
    invoke-virtual/range {v50 .. v50}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 319
    const/16 v58, 0x0

    .line 320
    .local v58, "reader":Ljava/io/FileReader;
    const/16 v37, 0x0

    .line 322
    .local v37, "buffReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v59, Ljava/io/FileReader;

    move-object/from16 v0, v59

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    .end local v58    # "reader":Ljava/io/FileReader;
    .local v59, "reader":Ljava/io/FileReader;
    :try_start_1
    new-instance v45, Ljava/io/BufferedReader;

    move-object/from16 v0, v45

    move-object/from16 v1, v59

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 325
    .end local v37    # "buffReader":Ljava/io/BufferedReader;
    .local v45, "buffReader":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual/range {v45 .. v45}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v56

    .local v56, "line":Ljava/lang/String;
    if-eqz v56, :cond_9

    .line 326
    const-string v3, "Z_PK"

    move-object/from16 v0, v56

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 329
    const-string v3, ","

    move-object/from16 v0, v56

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v57

    .line 330
    .local v57, "parts":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v52

    .line 331
    .local v52, "id":J
    const/4 v3, 0x5

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 332
    .local v4, "tripNumber":J
    const/4 v3, 0x6

    aget-object v3, v57, v3

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x6

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v21

    .line 333
    .local v21, "arrivedDestination":J
    :goto_1
    const/4 v3, 0x7

    aget-object v3, v57, v3

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    const/4 v3, 0x7

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v48

    .line 334
    .local v48, "chosenDestination":J
    :goto_2
    const/16 v3, 0x8

    aget-object v3, v57, v3

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const/16 v3, 0x8

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v54

    .line 335
    .local v54, "inferredDestination":J
    :goto_3
    const-wide/16 v24, -0x1

    cmp-long v3, v21, v24

    if-eqz v3, :cond_6

    move-wide/from16 v28, v21

    .line 336
    .local v28, "destinationId":J
    :goto_4
    const/16 v3, 0x9

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v17

    .line 337
    .local v17, "destLatitude":D
    const/16 v3, 0xa

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v19

    .line 338
    .local v19, "destLongitude":D
    const/16 v3, 0xb

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v24

    const-wide v26, 0x41cd27e440000000L    # 9.783072E8

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-long v14, v0

    .line 339
    .local v14, "endTime":J
    const/16 v3, 0xc

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 340
    .local v10, "startLatitude":D
    const/16 v3, 0xd

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 341
    .local v12, "startLongitude":D
    const/16 v3, 0xe

    aget-object v3, v57, v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v24

    const-wide v26, 0x41cd27e440000000L    # 9.783072E8

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-long v6, v0

    .line 342
    .local v6, "startTime":J
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Start Time "

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 343
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/SystemUtils;->getTimeZoneAndDaylightSavingOffset(Ljava/lang/Long;)I

    move-result v8

    .line 344
    .local v8, "offset":I
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v46

    .line 346
    .local v46, "cal":Ljava/util/Calendar;
    new-instance v3, Ljava/util/Date;

    const-wide/16 v24, 0x3e8

    mul-long v24, v24, v6

    move-wide/from16 v0, v24

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 347
    const/16 v3, 0xb

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v34

    .line 348
    .local v34, "hour":I
    const/16 v3, 0xc

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v35

    .line 349
    .local v35, "minute":I
    const/4 v3, 0x7

    move-object/from16 v0, v46

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v36

    .line 350
    .local v36, "dayOfTheWeek":I
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "MM/DD/YYY "

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v16, 0x2

    move-object/from16 v0, v46

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v16, 0x5

    move-object/from16 v0, v46

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v16, 0x1

    move-object/from16 v0, v46

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, " , Day :"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v36

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 351
    new-instance v2, Lcom/navdy/client/app/framework/models/Trip;

    move-wide/from16 v0, v52

    long-to-int v3, v0

    const/4 v9, 0x0

    const/16 v16, 0x0

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v23, v0

    invoke-direct/range {v2 .. v23}, Lcom/navdy/client/app/framework/models/Trip;-><init>(IJJIIDDJIDDJI)V

    .line 352
    .local v2, "trip":Lcom/navdy/client/app/framework/models/Trip;
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/framework/models/Trip;->saveToDb(Landroid/content/Context;)Landroid/net/Uri;

    move-wide/from16 v24, v17

    move-wide/from16 v26, v19

    move-wide/from16 v30, v10

    move-wide/from16 v32, v12

    .line 353
    invoke-static/range {v24 .. v36}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->learn(DDJDDIII)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto/16 :goto_0

    .line 364
    .end local v2    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    .end local v4    # "tripNumber":J
    .end local v6    # "startTime":J
    .end local v8    # "offset":I
    .end local v10    # "startLatitude":D
    .end local v12    # "startLongitude":D
    .end local v14    # "endTime":J
    .end local v17    # "destLatitude":D
    .end local v19    # "destLongitude":D
    .end local v21    # "arrivedDestination":J
    .end local v28    # "destinationId":J
    .end local v34    # "hour":I
    .end local v35    # "minute":I
    .end local v36    # "dayOfTheWeek":I
    .end local v46    # "cal":Ljava/util/Calendar;
    .end local v48    # "chosenDestination":J
    .end local v52    # "id":J
    .end local v54    # "inferredDestination":J
    .end local v56    # "line":Ljava/lang/String;
    .end local v57    # "parts":[Ljava/lang/String;
    :catch_0
    move-exception v60

    move-object/from16 v37, v45

    .end local v45    # "buffReader":Ljava/io/BufferedReader;
    .restart local v37    # "buffReader":Ljava/io/BufferedReader;
    move-object/from16 v58, v59

    .line 365
    .end local v59    # "reader":Ljava/io/FileReader;
    .restart local v58    # "reader":Ljava/io/FileReader;
    .local v60, "t":Ljava/lang/Throwable;
    :goto_5
    :try_start_3
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Exception "

    move-object/from16 v0, v60

    invoke-virtual {v3, v9, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 368
    if-eqz v37, :cond_1

    .line 369
    :try_start_4
    invoke-virtual/range {v37 .. v37}, Ljava/io/BufferedReader;->close()V

    .line 371
    :cond_1
    if-eqz v58, :cond_2

    .line 372
    invoke-virtual/range {v58 .. v58}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 380
    .end local v37    # "buffReader":Ljava/io/BufferedReader;
    .end local v58    # "reader":Ljava/io/FileReader;
    .end local v60    # "t":Ljava/lang/Throwable;
    :cond_2
    :goto_6
    return-void

    .line 332
    .restart local v4    # "tripNumber":J
    .restart local v45    # "buffReader":Ljava/io/BufferedReader;
    .restart local v52    # "id":J
    .restart local v56    # "line":Ljava/lang/String;
    .restart local v57    # "parts":[Ljava/lang/String;
    .restart local v59    # "reader":Ljava/io/FileReader;
    :cond_3
    const-wide/16 v21, -0x1

    goto/16 :goto_1

    .line 333
    .restart local v21    # "arrivedDestination":J
    :cond_4
    const-wide/16 v48, -0x1

    goto/16 :goto_2

    .line 334
    .restart local v48    # "chosenDestination":J
    :cond_5
    const-wide/16 v54, -0x1

    goto/16 :goto_3

    .line 335
    .restart local v54    # "inferredDestination":J
    :cond_6
    const-wide/16 v24, -0x1

    cmp-long v3, v48, v24

    if-eqz v3, :cond_7

    move-wide/from16 v28, v48

    goto/16 :goto_4

    :cond_7
    const-wide/16 v24, -0x1

    cmp-long v3, v54, v24

    if-eqz v3, :cond_8

    move-wide/from16 v28, v54

    goto/16 :goto_4

    :cond_8
    const-wide/16 v28, -0x1

    goto/16 :goto_4

    .line 355
    .end local v4    # "tripNumber":J
    .end local v21    # "arrivedDestination":J
    .end local v48    # "chosenDestination":J
    .end local v52    # "id":J
    .end local v54    # "inferredDestination":J
    .end local v57    # "parts":[Ljava/lang/String;
    :cond_9
    const/16 v44, 0x1

    .local v44, "day":I
    :goto_7
    const/4 v3, 0x7

    move/from16 v0, v44

    if-gt v0, v3, :cond_c

    .line 356
    const/16 v34, 0x0

    .restart local v34    # "hour":I
    :goto_8
    const/16 v3, 0x18

    move/from16 v0, v34

    if-ge v0, v3, :cond_b

    .line 357
    :try_start_5
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Day :"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v44

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, ", Hour :"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v34

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 358
    const-wide/16 v38, 0x0

    const-wide/16 v40, 0x0

    const/16 v43, 0x0

    move/from16 v42, v34

    invoke-static/range {v38 .. v44}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->suggestDestination(DDIII)Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;

    move-result-object v47

    .line 359
    .local v47, "destination":Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;
    if-eqz v47, :cond_a

    .line 360
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Destination :"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v47

    iget-wide v0, v0, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->latitude:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, ", "

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v47

    iget-wide v0, v0, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->longitude:D

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, " ; ID :"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v47

    iget-wide v0, v0, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->uniqueIdentifier:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 356
    :cond_a
    add-int/lit8 v34, v34, 0x1

    goto/16 :goto_8

    .line 355
    .end local v47    # "destination":Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;
    :cond_b
    add-int/lit8 v44, v44, 0x1

    goto/16 :goto_7

    .line 368
    .end local v34    # "hour":I
    :cond_c
    if-eqz v45, :cond_d

    .line 369
    :try_start_6
    invoke-virtual/range {v45 .. v45}, Ljava/io/BufferedReader;->close()V

    .line 371
    :cond_d
    if-eqz v59, :cond_2

    .line 372
    invoke-virtual/range {v59 .. v59}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_6

    .line 374
    :catch_1
    move-exception v51

    .line 375
    .local v51, "e":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Error closing the buffered reader"

    invoke-virtual {v3, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 374
    .end local v44    # "day":I
    .end local v45    # "buffReader":Ljava/io/BufferedReader;
    .end local v51    # "e":Ljava/io/IOException;
    .end local v56    # "line":Ljava/lang/String;
    .end local v59    # "reader":Ljava/io/FileReader;
    .restart local v37    # "buffReader":Ljava/io/BufferedReader;
    .restart local v58    # "reader":Ljava/io/FileReader;
    .restart local v60    # "t":Ljava/lang/Throwable;
    :catch_2
    move-exception v51

    .line 375
    .restart local v51    # "e":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Error closing the buffered reader"

    invoke-virtual {v3, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 367
    .end local v51    # "e":Ljava/io/IOException;
    .end local v60    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    .line 368
    :goto_9
    if-eqz v37, :cond_e

    .line 369
    :try_start_7
    invoke-virtual/range {v37 .. v37}, Ljava/io/BufferedReader;->close()V

    .line 371
    :cond_e
    if-eqz v58, :cond_f

    .line 372
    invoke-virtual/range {v58 .. v58}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 376
    :cond_f
    :goto_a
    throw v3

    .line 374
    :catch_3
    move-exception v51

    .line 375
    .restart local v51    # "e":Ljava/io/IOException;
    sget-object v9, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "Error closing the buffered reader"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_a

    .line 367
    .end local v51    # "e":Ljava/io/IOException;
    .end local v58    # "reader":Ljava/io/FileReader;
    .restart local v59    # "reader":Ljava/io/FileReader;
    :catchall_1
    move-exception v3

    move-object/from16 v58, v59

    .end local v59    # "reader":Ljava/io/FileReader;
    .restart local v58    # "reader":Ljava/io/FileReader;
    goto :goto_9

    .end local v37    # "buffReader":Ljava/io/BufferedReader;
    .end local v58    # "reader":Ljava/io/FileReader;
    .restart local v45    # "buffReader":Ljava/io/BufferedReader;
    .restart local v59    # "reader":Ljava/io/FileReader;
    :catchall_2
    move-exception v3

    move-object/from16 v37, v45

    .end local v45    # "buffReader":Ljava/io/BufferedReader;
    .restart local v37    # "buffReader":Ljava/io/BufferedReader;
    move-object/from16 v58, v59

    .end local v59    # "reader":Ljava/io/FileReader;
    .restart local v58    # "reader":Ljava/io/FileReader;
    goto :goto_9

    .line 364
    :catch_4
    move-exception v60

    goto/16 :goto_5

    .end local v58    # "reader":Ljava/io/FileReader;
    .restart local v59    # "reader":Ljava/io/FileReader;
    :catch_5
    move-exception v60

    move-object/from16 v58, v59

    .end local v59    # "reader":Ljava/io/FileReader;
    .restart local v58    # "reader":Ljava/io/FileReader;
    goto/16 :goto_5
.end method

.method private handleSuggest(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 144
    const-string v3, "EXTRA_LOCAL"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 145
    .local v0, "forThePhoneSuggestionList":Z
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onHandleIntent : Command suggest, forThePhoneSuggestionList = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 148
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->learnAllTrips()V

    .line 150
    if-eqz v0, :cond_0

    .line 152
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->suggestDestinationBasedOnContext(Z)V

    .line 185
    :goto_0
    return-void

    .line 155
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 156
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v3

    if-nez v3, :cond_2

    .line 157
    :cond_1
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Remote device is not connected, so not making a suggestion"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 182
    .end local v1    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :catch_0
    move-exception v2

    .line 183
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Error during destination suggestion "

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 161
    .end local v2    # "t":Ljava/lang/Throwable;
    .restart local v1    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getCurrentState()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    move-result-object v3

    sget-object v4, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->EN_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v3, v4, :cond_3

    .line 162
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "There is an active trip going on in HUD, so not sending suggestion"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_3
    new-instance v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$1;-><init>(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;)V

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildCalendarSuggestions(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private handleUploadTripData()V
    .locals 8

    .prologue
    .line 538
    sget-object v6, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Uploading the trip data base to the S3"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 539
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 540
    .local v0, "customerPreference":Landroid/content/SharedPreferences;
    const-string v6, "fname"

    const-string v7, "UNKNOWN"

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 541
    .local v3, "fullName":Ljava/lang/String;
    const-string v6, "[^a-zA-Z0-9.-]"

    const-string v7, "_"

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 542
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v6, "_MM_dd_yyyy_HH_mm_ss_SSS"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 543
    .local v4, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 544
    .local v1, "date":Ljava/util/Date;
    invoke-virtual {v4, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 546
    .local v2, "fileNameSuffix":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".csv"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 547
    .local v5, "uniqueName":Ljava/lang/String;
    invoke-direct {p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->getTripCursor()Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->uploadToS3(Ljava/lang/String;Landroid/database/Cursor;Lcom/squareup/otto/Bus;)V

    .line 549
    return-void
.end method

.method public static native learn(DDJDDIII)V
.end method

.method private learn(Lcom/navdy/client/app/framework/models/Trip;)V
    .locals 17
    .param p1, "trip"    # Lcom/navdy/client/app/framework/models/Trip;

    .prologue
    .line 638
    move-object/from16 v0, p1

    iget v2, v0, Lcom/navdy/client/app/framework/models/Trip;->id:I

    int-to-long v2, v2

    sget-wide v4, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sMaxIdentifier:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 639
    move-object/from16 v0, p1

    iget v2, v0, Lcom/navdy/client/app/framework/models/Trip;->id:I

    int-to-long v2, v2

    sput-wide v2, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sMaxIdentifier:J

    .line 641
    :cond_0
    move-object/from16 v0, p1

    iget v2, v0, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    if-lez v2, :cond_1

    move-object/from16 v0, p1

    iget v2, v0, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    int-to-long v6, v2

    .line 642
    .local v6, "destinationIdentifier":J
    :goto_0
    long-to-int v2, v6

    invoke-static {v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v16

    .line 643
    .local v16, "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v16, :cond_2

    move-object/from16 v0, v16

    iget-boolean v2, v0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    if-eqz v2, :cond_2

    .line 652
    :goto_1
    return-void

    .line 641
    .end local v6    # "destinationIdentifier":J
    .end local v16    # "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_1
    const-wide/16 v6, -0x1

    goto :goto_0

    .line 646
    .restart local v6    # "destinationIdentifier":J
    .restart local v16    # "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v15

    .line 647
    .local v15, "cal":Ljava/util/Calendar;
    new-instance v2, Ljava/util/Date;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Trip;->startTime:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v15, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 648
    const/16 v2, 0xb

    invoke-virtual {v15, v2}, Ljava/util/Calendar;->get(I)I

    move-result v12

    .line 649
    .local v12, "hour":I
    const/16 v2, 0xc

    invoke-virtual {v15, v2}, Ljava/util/Calendar;->get(I)I

    move-result v13

    .line 650
    .local v13, "minute":I
    const/4 v2, 0x7

    invoke-virtual {v15, v2}, Ljava/util/Calendar;->get(I)I

    move-result v14

    .line 651
    .local v14, "dayOfTheWeek":I
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/navdy/client/app/framework/models/Trip;->startLat:D

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/navdy/client/app/framework/models/Trip;->startLong:D

    invoke-static/range {v2 .. v14}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->learn(DDJDDIII)V

    goto :goto_1
.end method

.method private learnAllTrips()V
    .locals 4

    .prologue
    .line 552
    const/4 v0, 0x0

    .line 554
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->getTripCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 555
    if-eqz v0, :cond_2

    .line 556
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 557
    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 558
    .local v1, "position":I
    invoke-static {v0, v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Trip;

    move-result-object v2

    .line 559
    .local v2, "trip":Lcom/navdy/client/app/framework/models/Trip;
    if-eqz v2, :cond_0

    .line 560
    invoke-direct {p0, v2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->learn(Lcom/navdy/client/app/framework/models/Trip;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 566
    .end local v1    # "position":I
    .end local v2    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :catchall_0
    move-exception v3

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3

    .line 563
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    sput-boolean v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sColdStart:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 566
    :cond_2
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 568
    return-void
.end method

.method public static native reset()V
.end method

.method public static reset(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 678
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 679
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "COMMAND_RESET"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 680
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 681
    return-void
.end method

.method private scheduleNextCheckForUpdate()V
    .locals 8

    .prologue
    .line 436
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Scheduling for next check for update"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 437
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 438
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "COMMAND_AUTO_UPLOAD"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    const/16 v3, 0x100

    const/high16 v4, 0x10000000

    invoke-static {p0, v3, v1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 440
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 441
    .local v0, "am":Landroid/app/AlarmManager;
    const/4 v3, 0x1

    .line 442
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-wide v6, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->MINIMUM_INTERVAL_BETWEEN_UPLOADS:J

    add-long/2addr v4, v6

    .line 441
    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 444
    return-void
.end method

.method private sendSuggestion(Lcom/navdy/service/library/events/places/SuggestedDestination;)V
    .locals 2
    .param p1, "suggestedDestination"    # Lcom/navdy/service/library/events/places/SuggestedDestination;

    .prologue
    .line 615
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 616
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 617
    invoke-virtual {v0, p1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 619
    :cond_0
    return-void
.end method

.method private sendSuggestionToHud(Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V
    .locals 10
    .param p1, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "suggestionType"    # Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    .prologue
    const-wide/16 v8, 0x0

    .line 264
    .line 265
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Suggestion;->toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v0

    .line 266
    .local v0, "destinationMessage":Lcom/navdy/service/library/events/destination/Destination;
    if-eqz v0, :cond_1

    .line 267
    sget-object v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DestinationEngine , destination message,  Title :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Subtitle :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Type :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Is Recommendation :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Address :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Unique ID: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 278
    iget-object v1, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    cmpl-double v1, v6, v8

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    cmpl-double v1, v6, v8

    if-eqz v1, :cond_2

    .line 279
    :cond_0
    iget-object v1, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 280
    .local v2, "latitude":D
    iget-object v1, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 286
    .local v4, "longitude":D
    :goto_0
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->getInstance()Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    move-result-object v1

    new-instance v6, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$2;

    invoke-direct {v6, p0, v0, p2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$2;-><init>(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Lcom/navdy/service/library/events/destination/Destination;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    invoke-virtual/range {v1 .. v6}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->calculateRoute(DDLcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V

    .line 313
    .end local v2    # "latitude":D
    .end local v4    # "longitude":D
    :cond_1
    return-void

    .line 282
    :cond_2
    iget-object v1, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 283
    .restart local v2    # "latitude":D
    iget-object v1, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .restart local v4    # "longitude":D
    goto :goto_0
.end method

.method public static native suggestDestination(DDIII)Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;
.end method

.method public static suggestDestination(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "local"    # Z

    .prologue
    .line 661
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 662
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "COMMAND_SUGGEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 663
    const-string v1, "EXTRA_LOCAL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 664
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 665
    return-void
.end method

.method private suggestDestinationBasedOnContext(Z)V
    .locals 19
    .param p1, "forThePhoneSuggestionList"    # Z

    .prologue
    .line 196
    sget-object v2, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Suggest Destination based on the context For suggestion list :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 197
    new-instance v12, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v12, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 198
    .local v12, "date":Ljava/util/Date;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v9

    .line 199
    .local v9, "cal":Ljava/util/Calendar;
    invoke-virtual {v9, v12}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 200
    const/16 v2, 0xb

    invoke-virtual {v9, v2}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 201
    .local v6, "hour":I
    const/16 v2, 0xc

    invoke-virtual {v9, v2}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 202
    .local v7, "minute":I
    const/4 v2, 0x7

    invoke-virtual {v9, v2}, Ljava/util/Calendar;->get(I)I

    move-result v8

    .line 203
    .local v8, "dayOfTheWeek":I
    const/4 v13, 0x0

    .line 204
    .local v13, "destination":Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v10

    .line 206
    .local v10, "currentCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz v10, :cond_4

    .line 207
    sget-object v2, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current location is known: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v10, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v10, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 208
    iget-object v2, v10, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, v10, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static/range {v2 .. v8}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->suggestDestination(DDIII)Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;

    move-result-object v13

    .line 215
    :cond_0
    :goto_0
    const/16 v16, 0x0

    .line 216
    .local v16, "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v13, :cond_6

    .line 217
    iget-wide v2, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->uniqueIdentifier:J

    long-to-int v2, v2

    invoke-static {v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v16

    .line 218
    if-eqz v16, :cond_6

    .line 219
    new-instance v11, Landroid/location/Location;

    const-string v2, ""

    invoke-direct {v11, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 220
    .local v11, "currentLocation":Landroid/location/Location;
    if-eqz v10, :cond_1

    .line 221
    iget-object v2, v10, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v11, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 222
    iget-object v2, v10, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v11, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 224
    :cond_1
    new-instance v17, Landroid/location/Location;

    const-string v2, ""

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 225
    .local v17, "suggestedLocation":Landroid/location/Location;
    move-object/from16 v0, v16

    iget-wide v2, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 226
    move-object/from16 v0, v16

    iget-wide v2, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 227
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v2

    float-to-double v14, v2

    .line 228
    .local v14, "distance":D
    const-wide/high16 v2, 0x4069000000000000L    # 200.0

    cmpg-double v2, v14, v2

    if-gtz v2, :cond_5

    .line 229
    if-eqz p1, :cond_2

    .line 230
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->NO_SUGGESTION:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 232
    :cond_2
    sget-object v2, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Not suggesting as the destination with the identifier is very close to the current location"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 233
    sget-object v2, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DestinationEngine , suggested destiantion,  ID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->uniqueIdentifier:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Lat: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->latitude:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Long: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->longitude:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Frequency: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->frequency:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Probability: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->probability:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 260
    .end local v11    # "currentLocation":Landroid/location/Location;
    .end local v14    # "distance":D
    .end local v17    # "suggestedLocation":Landroid/location/Location;
    :cond_3
    :goto_1
    return-void

    .line 210
    .end local v16    # "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_4
    sget-object v2, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Current location is unknown"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 211
    if-eqz p1, :cond_0

    .line 212
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-static/range {v2 .. v8}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->suggestDestination(DDIII)Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;

    move-result-object v13

    goto/16 :goto_0

    .line 241
    .restart local v11    # "currentLocation":Landroid/location/Location;
    .restart local v14    # "distance":D
    .restart local v16    # "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    .restart local v17    # "suggestedLocation":Landroid/location/Location;
    :cond_5
    if-eqz p1, :cond_7

    .line 242
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 257
    .end local v11    # "currentLocation":Landroid/location/Location;
    .end local v14    # "distance":D
    .end local v17    # "suggestedLocation":Landroid/location/Location;
    :cond_6
    :goto_2
    if-eqz p1, :cond_3

    if-nez v16, :cond_3

    .line 258
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->NO_SUGGESTION:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    goto :goto_1

    .line 244
    .restart local v11    # "currentLocation":Landroid/location/Location;
    .restart local v14    # "distance":D
    .restart local v17    # "suggestedLocation":Landroid/location/Location;
    :cond_7
    sget-object v2, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DestinationEngine: suggested destiantion: ID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->uniqueIdentifier:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Lat = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->latitude:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Long = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->longitude:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Frequency = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->frequency:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Probability = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v13, Lcom/navdy/client/app/framework/suggestion/SuggestedDestination;->probability:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 250
    new-instance v18, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECOMMENDATION:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 252
    .local v18, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    sget-object v2, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_RECOMMENDATION:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sendSuggestionToHud(Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    goto :goto_2
.end method

.method private uploadFileToS3(Ljava/io/File;Lcom/squareup/otto/Bus;)V
    .locals 8
    .param p1, "file"    # Ljava/io/File;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 571
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 572
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Length of the file being uploaded: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 573
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 574
    if-eqz p2, :cond_0

    .line 575
    sget-object v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->FAILURE:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    invoke-virtual {p2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 612
    :cond_0
    :goto_0
    return-void

    .line 579
    :cond_1
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->createS3Client()Lcom/amazonaws/services/s3/AmazonS3Client;

    move-result-object v0

    .line 580
    .local v0, "amazonS3Client":Lcom/amazonaws/services/s3/AmazonS3Client;
    new-instance v2, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    invoke-direct {v2, v0, p0}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;-><init>(Lcom/amazonaws/services/s3/AmazonS3;Landroid/content/Context;)V

    .line 581
    .local v2, "transferUtility":Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;
    const-string v3, "navdy-trip-data"

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->upload(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    move-result-object v1

    .line 582
    .local v1, "transferObserver":Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;
    new-instance v3, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;

    invoke-direct {v3, p0, p1, p2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;-><init>(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Ljava/io/File;Lcom/squareup/otto/Bus;)V

    invoke-virtual {v1, v3}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->setTransferListener(Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferListener;)V

    goto :goto_0
.end method

.method private uploadToS3(Ljava/lang/String;Landroid/database/Cursor;Lcom/squareup/otto/Bus;)V
    .locals 16
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "tripDbCursor"    # Landroid/database/Cursor;
    .param p3, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 447
    const/4 v4, 0x0

    .line 449
    .local v4, "fileWriter":Ljava/io/FileWriter;
    :try_start_0
    new-instance v3, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->getFilesDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 450
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 451
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 453
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_c
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v10

    if-nez v10, :cond_3

    .line 520
    if-eqz p2, :cond_1

    .line 522
    :try_start_1
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 527
    :cond_1
    :goto_0
    if-eqz v4, :cond_2

    .line 529
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 535
    .end local v3    # "file":Ljava/io/File;
    :cond_2
    :goto_1
    return-void

    .line 523
    .restart local v3    # "file":Ljava/io/File;
    :catch_0
    move-exception v8

    .line 524
    .local v8, "t":Ljava/lang/Throwable;
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error closing the cursor "

    invoke-virtual {v10, v11, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 530
    .end local v8    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v2

    .line 531
    .local v2, "e":Ljava/io/IOException;
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error closing the file writer "

    invoke-virtual {v10, v11, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 456
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_3
    new-instance v5, Ljava/io/FileWriter;

    const/4 v10, 0x1

    invoke-direct {v5, v3, v10}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_c
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 457
    .end local v4    # "fileWriter":Ljava/io/FileWriter;
    .local v5, "fileWriter":Ljava/io/FileWriter;
    if-eqz p2, :cond_10

    :try_start_4
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-lez v10, :cond_10

    .line 458
    const-string v10, "TRIP_ID,TRIP_NUMBER,TRIP_START_TIME,TRIP_START_TIME_ZONE_N_DST,TRIP_ODOMETER,TRIP_LATITUDE,TRIP_LONGITUDE,TRIP_END_TIME,TRIP_END_ODOMETER,TRIP_END_LATITUDE,TRIP_END_LONGITUDE,TRIP_ARRIVED_AT_DESTINATION,TRIP_DESTINATION_ID,TRIP_DESTINATION_LAT,TRIP_DESTINATION_LONG\n"

    invoke-virtual {v5, v10}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 474
    :cond_4
    :goto_2
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 475
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    .line 476
    .local v6, "position":I
    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Trip;

    move-result-object v9

    .line 477
    .local v9, "trip":Lcom/navdy/client/app/framework/models/Trip;
    if-eqz v9, :cond_4

    .line 479
    iget v10, v9, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    invoke-static {v10}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v7

    .line 480
    .local v7, "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget v11, v9, Lcom/navdy/client/app/framework/models/Trip;->id:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v9, Lcom/navdy/client/app/framework/models/Trip;->tripNumber:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v9, Lcom/navdy/client/app/framework/models/Trip;->startTime:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v9, Lcom/navdy/client/app/framework/models/Trip;->offset:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v9, Lcom/navdy/client/app/framework/models/Trip;->startOdometer:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v9, Lcom/navdy/client/app/framework/models/Trip;->startLat:D

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v9, Lcom/navdy/client/app/framework/models/Trip;->startLong:D

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v9, Lcom/navdy/client/app/framework/models/Trip;->endTime:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v9, Lcom/navdy/client/app/framework/models/Trip;->endOdometer:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v9, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v9, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-wide v12, v9, Lcom/navdy/client/app/framework/models/Trip;->arrivedAtDestination:J

    const-wide/16 v14, 0x0

    cmp-long v10, v12, v14

    if-nez v10, :cond_7

    const-string v10, "false"

    :goto_3
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v10, v9, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    if-gtz v10, :cond_8

    const-string v10, "NA"

    .line 492
    :goto_4
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    if-eqz v7, :cond_9

    iget-wide v10, v7, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    :goto_5
    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    if-eqz v7, :cond_a

    iget-wide v10, v7, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    :goto_6
    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 480
    invoke-virtual {v5, v10}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 514
    .end local v6    # "position":I
    .end local v7    # "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v9    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :catch_2
    move-exception v8

    move-object v4, v5

    .line 515
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fileWriter":Ljava/io/FileWriter;
    .restart local v4    # "fileWriter":Ljava/io/FileWriter;
    .restart local v8    # "t":Ljava/lang/Throwable;
    :goto_7
    if-eqz p3, :cond_5

    .line 516
    :try_start_5
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->FAILURE:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 518
    :cond_5
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error while dumping the trip database to the csv file"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 520
    if-eqz p2, :cond_6

    .line 522
    :try_start_6
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_9

    .line 527
    :cond_6
    :goto_8
    if-eqz v4, :cond_2

    .line 529
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_1

    .line 530
    :catch_3
    move-exception v2

    .line 531
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error closing the file writer "

    invoke-virtual {v10, v11, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 480
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "fileWriter":Ljava/io/FileWriter;
    .end local v8    # "t":Ljava/lang/Throwable;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fileWriter":Ljava/io/FileWriter;
    .restart local v6    # "position":I
    .restart local v7    # "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    .restart local v9    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :cond_7
    :try_start_8
    const-string v10, "true"

    goto :goto_3

    :cond_8
    iget v10, v9, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    .line 492
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v10

    goto :goto_4

    :cond_9
    const-wide/16 v10, 0x0

    goto :goto_5

    :cond_a
    const-wide/16 v10, 0x0

    goto :goto_6

    .line 499
    .end local v6    # "position":I
    .end local v7    # "savedDestination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v9    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :cond_b
    :try_start_9
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 512
    :goto_9
    :try_start_a
    invoke-virtual {v5}, Ljava/io/FileWriter;->flush()V

    .line 513
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v1}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->uploadFileToS3(Ljava/io/File;Lcom/squareup/otto/Bus;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 520
    if-eqz p2, :cond_c

    .line 522
    :try_start_b
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_7

    .line 527
    :cond_c
    :goto_a
    if-eqz v5, :cond_14

    .line 529
    :try_start_c
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    move-object v4, v5

    .line 532
    .end local v5    # "fileWriter":Ljava/io/FileWriter;
    .restart local v4    # "fileWriter":Ljava/io/FileWriter;
    goto/16 :goto_1

    .line 500
    .end local v4    # "fileWriter":Ljava/io/FileWriter;
    .restart local v5    # "fileWriter":Ljava/io/FileWriter;
    :catch_4
    move-exception v8

    .line 501
    .restart local v8    # "t":Ljava/lang/Throwable;
    if-eqz p3, :cond_d

    .line 502
    :try_start_d
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->FAILURE:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 504
    :cond_d
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error closing the cursor "

    invoke-virtual {v10, v11, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_9

    .line 520
    .end local v8    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v10

    move-object v4, v5

    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fileWriter":Ljava/io/FileWriter;
    .restart local v4    # "fileWriter":Ljava/io/FileWriter;
    :goto_b
    if-eqz p2, :cond_e

    .line 522
    :try_start_e
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_a

    .line 527
    :cond_e
    :goto_c
    if-eqz v4, :cond_f

    .line 529
    :try_start_f
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    .line 532
    :cond_f
    :goto_d
    throw v10

    .line 507
    .end local v4    # "fileWriter":Ljava/io/FileWriter;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fileWriter":Ljava/io/FileWriter;
    :cond_10
    if-eqz p3, :cond_11

    .line 508
    :try_start_10
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->FAILURE:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 520
    :cond_11
    if-eqz p2, :cond_12

    .line 522
    :try_start_11
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_5

    .line 527
    :cond_12
    :goto_e
    if-eqz v5, :cond_13

    .line 529
    :try_start_12
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_6

    :cond_13
    :goto_f
    move-object v4, v5

    .line 510
    .end local v5    # "fileWriter":Ljava/io/FileWriter;
    .restart local v4    # "fileWriter":Ljava/io/FileWriter;
    goto/16 :goto_1

    .line 523
    .end local v4    # "fileWriter":Ljava/io/FileWriter;
    .restart local v5    # "fileWriter":Ljava/io/FileWriter;
    :catch_5
    move-exception v8

    .line 524
    .restart local v8    # "t":Ljava/lang/Throwable;
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error closing the cursor "

    invoke-virtual {v10, v11, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_e

    .line 530
    .end local v8    # "t":Ljava/lang/Throwable;
    :catch_6
    move-exception v2

    .line 531
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error closing the file writer "

    invoke-virtual {v10, v11, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_f

    .line 523
    .end local v2    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v8

    .line 524
    .restart local v8    # "t":Ljava/lang/Throwable;
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error closing the cursor "

    invoke-virtual {v10, v11, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_a

    .line 530
    .end local v8    # "t":Ljava/lang/Throwable;
    :catch_8
    move-exception v2

    .line 531
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error closing the file writer "

    invoke-virtual {v10, v11, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v5

    .line 532
    .end local v5    # "fileWriter":Ljava/io/FileWriter;
    .restart local v4    # "fileWriter":Ljava/io/FileWriter;
    goto/16 :goto_1

    .line 523
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "file":Ljava/io/File;
    .restart local v8    # "t":Ljava/lang/Throwable;
    :catch_9
    move-exception v8

    .line 524
    sget-object v10, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Error closing the cursor "

    invoke-virtual {v10, v11, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 523
    .end local v8    # "t":Ljava/lang/Throwable;
    :catch_a
    move-exception v8

    .line 524
    .restart local v8    # "t":Ljava/lang/Throwable;
    sget-object v11, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "Error closing the cursor "

    invoke-virtual {v11, v12, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_c

    .line 530
    .end local v8    # "t":Ljava/lang/Throwable;
    :catch_b
    move-exception v2

    .line 531
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v11, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "Error closing the file writer "

    invoke-virtual {v11, v12, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_d

    .line 520
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v10

    goto :goto_b

    .line 514
    :catch_c
    move-exception v8

    goto/16 :goto_7

    .end local v4    # "fileWriter":Ljava/io/FileWriter;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fileWriter":Ljava/io/FileWriter;
    :cond_14
    move-object v4, v5

    .end local v5    # "fileWriter":Ljava/io/FileWriter;
    .restart local v4    # "fileWriter":Ljava/io/FileWriter;
    goto/16 :goto_1
.end method

.method public static uploadTripDatabase(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manual"    # Z

    .prologue
    .line 668
    if-nez p1, :cond_0

    .line 669
    sget-object v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "UploadTripDatabase :Not a debug build so skipping"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 675
    :goto_0
    return-void

    .line 672
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 673
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_1

    const-string v1, "COMMAND_UPLOAD"

    :goto_1
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 674
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 673
    :cond_1
    const-string v1, "COMMAND_AUTO_UPLOAD"

    goto :goto_1
.end method


# virtual methods
.method public onCreate()V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 101
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 124
    if-nez p1, :cond_1

    .line 125
    sget-object v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "received a call for onHandleIntent with a null intent!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "action":Ljava/lang/String;
    const-string v1, "COMMAND_SUGGEST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 130
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->handleSuggest(Landroid/content/Intent;)V

    goto :goto_0

    .line 131
    :cond_2
    const-string v1, "COMMAND_POPULATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 132
    invoke-direct {p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->handlePopulate()V

    goto :goto_0

    .line 133
    :cond_3
    const-string v1, "COMMAND_UPLOAD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 134
    invoke-direct {p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->handleUploadTripData()V

    goto :goto_0

    .line 135
    :cond_4
    const-string v1, "COMMAND_AUTO_UPLOAD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 136
    invoke-direct {p0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->handleAutoTripUpload()V

    goto :goto_0

    .line 137
    :cond_5
    const-string v1, "COMMAND_RESET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    invoke-static {}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->reset()V

    .line 139
    const/4 v1, 0x1

    sput-boolean v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sColdStart:Z

    goto :goto_0
.end method
