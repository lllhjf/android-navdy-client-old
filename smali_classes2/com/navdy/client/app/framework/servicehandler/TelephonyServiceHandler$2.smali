.class Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$2;
.super Ljava/util/TimerTask;
.source "TelephonyServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->turnOnSpeakerPhoneIfNeeded()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 154
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "HFP is not connected, turning on the speaker phone"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 155
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    invoke-static {v1}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->access$200(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 156
    .local v0, "mAudioManager":Landroid/media/AudioManager;
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 157
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 158
    return-void
.end method
