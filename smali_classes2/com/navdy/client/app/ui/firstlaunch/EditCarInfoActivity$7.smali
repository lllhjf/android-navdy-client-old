.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;
.super Ljava/lang/Object;
.source "EditCarInfoActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->goToNextStep()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

.field final synthetic val$nextStep:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 466
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->val$nextStep:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 469
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->hideProgressDialog()V

    .line 470
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->val$nextStep:Ljava/lang/String;

    const-string v2, "installation_flow"

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 472
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 474
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 484
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 475
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->val$nextStep:Ljava/lang/String;

    const-string v2, "app_setup"

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 476
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->goToAppSetup(Landroid/app/Activity;)V

    goto :goto_0

    .line 478
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 480
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "extra_next_step"

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->val$nextStep:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 481
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 482
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->finish()V

    goto :goto_0
.end method
