.class public Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "MarketingFlowActivity.java"


# static fields
.field private static final BG_IMAGES:[I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private static final DESC_TEXTS:[I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public static final HAND_APPEARANCE_INDEX:I = 0x3

.field private static final HUD_UI_IMAGES:[I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private static final PAGINATIONS:[I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private static final TITLE_TEXTS:[I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# instance fields
.field private hiddenButtonClickCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x5

    .line 42
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->BG_IMAGES:[I

    .line 48
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->HUD_UI_IMAGES:[I

    .line 54
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->TITLE_TEXTS:[I

    .line 61
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->DESC_TEXTS:[I

    .line 68
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->PAGINATIONS:[I

    return-void

    .line 42
    :array_0
    .array-data 4
        0x7f020241
        0x7f020242
        0x7f020247
        0x7f02022c
    .end array-data

    .line 48
    :array_1
    .array-data 4
        0x7f02022f
        0x7f020230
        0x7f020231
        0x7f02022e
    .end array-data

    .line 54
    :array_2
    .array-data 4
        0x7f0802a3
        0x7f0802a5
        0x7f0802a7
        0x7f08029f
        0x7f0802a1
    .end array-data

    .line 61
    :array_3
    .array-data 4
        0x7f0802a4
        0x7f0802a6
        0x7f0802a8
        0x7f0802a0
        0x7f0802a2
    .end array-data

    .line 68
    :array_4
    .array-data 4
        0x7f0202b8
        0x7f0202b9
        0x7f0202ba
        0x7f0202bb
        0x7f0202bc
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    .line 262
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->hiddenButtonClickCount:I

    return-void
.end method

.method static synthetic access$000()[I
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->TITLE_TEXTS:[I

    return-object v0
.end method

.method static synthetic access$100()[I
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->PAGINATIONS:[I

    return-object v0
.end method

.method public static userHasFinishedMarketing()Z
    .locals 3

    .prologue
    .line 258
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 259
    .local v0, "settingsPrefs":Landroid/content/SharedPreferences;
    const-string v1, "finished_marketing"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public callInstallActivity(Z)V
    .locals 3
    .param p1, "wasSkiped"    # Z

    .prologue
    .line 252
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 253
    .local v0, "installIntent":Landroid/content/Intent;
    const-string v1, "extra_was_skipped"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 254
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->startActivity(Landroid/content/Intent;)V

    .line 255
    return-void
.end method

.method public onBuyOneClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 241
    const v0, 0x7f0803b5

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->openBrowserFor(Landroid/net/Uri;)V

    .line 242
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super/range {p0 .. p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 93
    const v0, 0x7f03007b

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->setContentView(I)V

    .line 95
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->userHasFinishedMarketing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->callInstallActivity(Z)V

    .line 99
    :cond_0
    const v0, 0x7f1001e9

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v4/view/ViewPager;

    .line 100
    .local v4, "bgPager":Landroid/support/v4/view/ViewPager;
    const v0, 0x7f1001eb

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/support/v4/view/ViewPager;

    .line 101
    .local v9, "hudUiPager":Landroid/support/v4/view/ViewPager;
    const v0, 0x7f1001ee

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    .line 102
    .local v3, "textPager":Landroid/support/v4/view/ViewPager;
    const v0, 0x7f1001ec

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 103
    .local v10, "pagination":Landroid/widget/ImageView;
    const v0, 0x7f1001ed

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 104
    .local v2, "hand":Landroid/widget/ImageView;
    const v0, 0x7f1001f0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 105
    .local v7, "buyOne":Landroid/widget/TextView;
    const v0, 0x7f1001ef

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 106
    .local v6, "logo":Landroid/widget/ImageView;
    const v0, 0x7f1001f1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 108
    .local v8, "start":Landroid/widget/Button;
    if-eqz v4, :cond_1

    if-eqz v9, :cond_1

    if-eqz v3, :cond_1

    if-eqz v10, :cond_1

    if-eqz v2, :cond_1

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    if-nez v8, :cond_2

    .line 224
    :cond_1
    :goto_0
    return-void

    .line 119
    :cond_2
    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 120
    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f400000    # 0.75f

    mul-float/2addr v0, v1

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 121
    const/high16 v0, 0x43070000    # 135.0f

    invoke-static {v0}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v0

    int-to-float v5, v0

    .line 122
    .local v5, "totalTranslationY":F
    neg-float v0, v5

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 124
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    .line 125
    .local v11, "appContext":Landroid/content/Context;
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowImagePagerAdaper;

    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->BG_IMAGES:[I

    iget-object v14, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v0, v11, v1, v14}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowImagePagerAdaper;-><init>(Landroid/content/Context;[ILcom/navdy/client/app/framework/util/ImageCache;)V

    invoke-virtual {v4, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 126
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowImagePagerAdaper;

    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->HUD_UI_IMAGES:[I

    iget-object v14, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v0, v11, v1, v14}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowImagePagerAdaper;-><init>(Landroid/content/Context;[ILcom/navdy/client/app/framework/util/ImageCache;)V

    invoke-virtual {v9, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 127
    new-instance v13, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;

    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->TITLE_TEXTS:[I

    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->DESC_TEXTS:[I

    new-instance v14, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$2;

    invoke-direct {v14, p0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;)V

    invoke-direct {v13, v11, v0, v1, v14}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;-><init>(Landroid/content/Context;[I[ILandroid/view/View$OnClickListener;)V

    .line 135
    .local v13, "textPagerAdapter":Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;
    const v0, 0x7f0800dc

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v12

    .line 136
    .local v12, "buyOneString":Landroid/text/Spanned;
    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    invoke-virtual {v3, v13}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 140
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;-><init>(Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;Landroid/widget/ImageView;Landroid/support/v4/view/ViewPager;Landroid/support/v4/view/ViewPager;FLandroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/Button;Landroid/support/v4/view/ViewPager;Landroid/widget/ImageView;)V

    invoke-virtual {v3, v0}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 223
    const-string v0, "Meet_Navdy"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onNavdyLogoClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 287
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 79
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onStart()V

    .line 81
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 88
    return-void
.end method

.method public onStartInstallClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 245
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 246
    .local v0, "settingsPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "finished_marketing"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 248
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->callInstallActivity(Z)V

    .line 249
    return-void
.end method

.method public onTextViewPagerClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 227
    const v3, 0x7f1001ee

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    .line 229
    .local v2, "textPager":Landroid/support/v4/view/ViewPager;
    if-nez v2, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v1, v3, 0x1

    .line 234
    .local v1, "nextItem":I
    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    .line 235
    .local v0, "adapter":Landroid/support/v4/view/PagerAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 236
    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method
