.class Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;
.super Ljava/lang/Object;
.source "FavoritesFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/favorites/FavoritesFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 63
    .local v4, "position":I
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    invoke-virtual {v6}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 64
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    const v7, 0x7f100152

    if-ne v6, v7, :cond_1

    .line 65
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    iget-object v6, v6, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    if-eqz v6, :cond_0

    .line 66
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    iget-object v6, v6, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v6, v4}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v3

    .line 67
    .local v3, "favorite":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {v0, v3}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->startFavoriteEditActivity(Landroid/app/Activity;Landroid/os/Parcelable;)V

    .line 98
    .end local v3    # "favorite":Lcom/navdy/client/app/framework/models/Destination;
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    invoke-static {v6}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesFragment;)Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "position clicked: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 71
    const-string v6, "Navigate_Using_Favorites"

    invoke-static {v6}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 72
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    iget-object v6, v6, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    if-nez v6, :cond_2

    .line 73
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    invoke-static {v6}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->access$100(Lcom/navdy/client/app/ui/favorites/FavoritesFragment;)Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t handle click at position "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " without a valid adapter"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 76
    :cond_2
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    iget-object v6, v6, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v6, v4}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    .line 78
    .local v2, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v5

    .line 79
    .local v5, "setDestinationTracker":Lcom/navdy/client/app/tracking/SetDestinationTracker;
    const-string v6, "Favorites_List"

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setSourceValue(Ljava/lang/String;)V

    .line 80
    const-string v6, "Favorite"

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 81
    invoke-static {v2}, Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes$FAVORITE_TYPE_VALUES;->getFavoriteTypeValue(Lcom/navdy/client/app/framework/models/Destination;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setFavoriteType(Ljava/lang/String;)V

    move-object v1, v0

    .line 83
    check-cast v1, Lcom/navdy/client/app/ui/base/BaseActivity;

    .line 84
    .local v1, "baseActivity":Lcom/navdy/client/app/ui/base/BaseActivity;
    new-instance v6, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;

    invoke-direct {v6, p0, v5, v2, v0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;-><init>(Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;Lcom/navdy/client/app/tracking/SetDestinationTracker;Lcom/navdy/client/app/framework/models/Destination;Landroid/support/v4/app/FragmentActivity;)V

    invoke-virtual {v1, v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->showRequestNewRouteDialog(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
