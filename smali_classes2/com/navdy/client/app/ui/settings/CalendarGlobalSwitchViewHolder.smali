.class Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "CalendarGlobalSwitchViewHolder.java"


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "checkedChangeListener"    # Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 20
    const v2, 0x7f1003e9

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    .line 22
    .local v1, "sweetch":Landroid/widget/Switch;
    if-nez v1, :cond_0

    .line 58
    :goto_0
    return-void

    .line 26
    :cond_0
    const v2, 0x7f080479

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setText(I)V

    .line 28
    new-instance v2, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder$1;

    invoke-direct {v2, p0, v1}, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder$1;-><init>(Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;Landroid/widget/Switch;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 36
    .local v0, "sharedPrefs":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_1

    .line 37
    const-string v2, "calendars_enabled"

    const/4 v3, 0x1

    .line 38
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 37
    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 43
    :cond_1
    new-instance v2, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder$2;

    invoke-direct {v2, p0, p2}, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder$2;-><init>(Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method
