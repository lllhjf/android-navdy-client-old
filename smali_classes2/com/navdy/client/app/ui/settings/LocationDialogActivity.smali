.class public Lcom/navdy/client/app/ui/settings/LocationDialogActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "LocationDialogActivity.java"


# static fields
.field public static final PENDING_DESTINATION:Ljava/lang/String; = "pending_destination"


# instance fields
.field private pendingDestination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 56
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/LocationDialogActivity;->pendingDestination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 57
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/LocationDialogActivity;->finish()V

    .line 58
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f030050

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/LocationDialogActivity;->setContentView(I)V

    .line 28
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/LocationDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pending_destination"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/LocationDialogActivity;->pendingDestination:Lcom/navdy/client/app/framework/models/Destination;

    .line 29
    return-void
.end method

.method public onLocationSettingsClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 51
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 52
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/LocationDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 53
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onPause()V

    .line 48
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 33
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 35
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "location_mode"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    .line 36
    .local v1, "locationMode":I
    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/LocationDialogActivity;->finish()V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .end local v1    # "locationMode":I
    :cond_0
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/LocationDialogActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "location setting not found: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
