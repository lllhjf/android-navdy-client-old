.class public Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "FeatureVideosActivity.java"


# instance fields
.field private layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

.field private mFeatureAdapter:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

.field private mFeatureRecycler:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const v0, 0x7f0300f2

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->setContentView(I)V

    .line 24
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v1, 0x7f080179

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 26
    const v0, 0x7f1003a3

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->mFeatureRecycler:Landroid/support/v7/widget/RecyclerView;

    .line 28
    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->mFeatureAdapter:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    .line 29
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->setOrientation(I)V

    .line 31
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->mFeatureRecycler:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 32
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->mFeatureRecycler:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->mFeatureAdapter:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 33
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->mFeatureAdapter:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosActivity;->mFeatureAdapter:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->notifyDataSetChanged()V

    .line 41
    :cond_0
    return-void
.end method
