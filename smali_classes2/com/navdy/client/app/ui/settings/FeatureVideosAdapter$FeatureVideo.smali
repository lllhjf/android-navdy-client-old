.class public Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;
.super Ljava/lang/Object;
.source "FeatureVideosAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FeatureVideo"
.end annotation


# instance fields
.field public duration:Ljava/lang/String;

.field public imageResource:I

.field public link:Ljava/lang/String;

.field public sharedPrefKey:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "sharedPrefKey"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "imageResource"    # I
    .param p4, "duration"    # Ljava/lang/String;
    .param p5, "link"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->sharedPrefKey:Ljava/lang/String;

    .line 171
    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->title:Ljava/lang/String;

    .line 172
    iput p3, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->imageResource:I

    .line 173
    iput-object p4, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->duration:Ljava/lang/String;

    .line 174
    iput-object p5, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->link:Ljava/lang/String;

    .line 175
    return-void
.end method
