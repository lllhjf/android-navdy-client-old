.class Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$2;
.super Ljava/lang/Object;
.source "FeatureVideosAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->initImageCache()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 127
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->access$100(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00ba

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 128
    .local v3, "width":I
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->access$100(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 130
    .local v2, "height":I
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->access$200(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    .line 131
    .local v1, "featureVideo":Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;
    iget v5, v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->imageResource:I

    invoke-static {v5, v3, v2}, Lcom/navdy/client/app/framework/util/ImageUtils;->getBitmap(III)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 132
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->access$300(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bitmap size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/ImageCache;->getBitmapSize(Landroid/graphics/Bitmap;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 133
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->access$400(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Lcom/navdy/client/app/framework/util/ImageCache;

    move-result-object v5

    iget-object v6, v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->title:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Lcom/navdy/client/app/framework/util/ImageCache;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 136
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "featureVideo":Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;
    :cond_0
    return-void
.end method
