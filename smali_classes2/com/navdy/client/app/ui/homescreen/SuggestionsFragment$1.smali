.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCarLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 4
    .param p1, "carLocation"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 160
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$000(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .line 161
    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$000(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)D

    move-result-wide v0

    const-wide v2, 0x407f400000000000L    # 500.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$002(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/location/Coordinate;

    .line 163
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->rebuildSuggestions()V

    .line 165
    :cond_1
    return-void
.end method

.method public onPhoneLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 4
    .param p1, "phoneLocation"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 151
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$000(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .line 152
    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$000(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)D

    move-result-wide v0

    const-wide v2, 0x407f400000000000L    # 500.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$002(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/location/Coordinate;

    .line 154
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->rebuildSuggestions()V

    .line 156
    :cond_1
    return-void
.end method
