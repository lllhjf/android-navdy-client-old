.class public Lcom/navdy/client/app/ui/homescreen/CalendarUtils;
.super Ljava/lang/Object;
.source "CalendarUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;
    }
.end annotation


# static fields
.field public static final EVENTS_STALENESS_LIMIT:J = 0x493e0L

.field private static final MAX_CALENDAR_EVENTS:I = 0x14

.field private static final TIME_LIMIT_AFTER_NOW:J = 0xa4cb800L

.field private static final TIME_LIMIT_BEFORE_NOW:J = 0x1b7740L

.field private static cachedEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/CalendarEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static lastReadTime:J

.field private static logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->cachedEvents:Ljava/util/List;

    .line 55
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->lastReadTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 46
    invoke-static {p0}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->setCachedEvents(Ljava/util/List;)V

    return-void
.end method

.method public static convertAllClendarNameSharedPrefsToCalendarIds()V
    .locals 20

    .prologue
    .line 394
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v12

    .line 396
    .local v12, "context":Landroid/content/Context;
    const-string v4, "android.permission.READ_CALENDAR"

    invoke-static {v12, v4}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 398
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->removeAllSelectedCalendarsFromSharedPrefs()V

    .line 435
    :goto_0
    return-void

    .line 402
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v17

    .line 403
    .local v17, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v8

    .line 404
    .local v8, "all":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v16

    .line 405
    .local v16, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    .line 406
    .local v14, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_1
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 407
    .local v15, "key":Ljava/lang/String;
    const-string v4, "calendar_"

    invoke-virtual {v15, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 411
    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v15, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    .line 412
    .local v18, "value":Z
    const-string v4, "calendar_"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v15, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 413
    .local v9, "calendarName":Ljava/lang/String;
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 414
    .local v2, "cr":Landroid/content/ContentResolver;
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    .line 415
    .local v3, "uri":Landroid/net/Uri;
    const/4 v13, 0x0

    .line 418
    .local v13, "cur":Landroid/database/Cursor;
    const/4 v4, 0x1

    :try_start_0
    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const-string v5, "name=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v9, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 423
    :cond_2
    :goto_2
    if-eqz v13, :cond_3

    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 424
    const-string v4, "_id"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 425
    .local v10, "calendarId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v10, v4

    if-lez v4, :cond_2

    .line 426
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v18

    invoke-interface {v14, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 430
    .end local v10    # "calendarId":J
    :catchall_0
    move-exception v4

    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v4

    :cond_3
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 432
    invoke-interface {v14, v15}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 434
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v9    # "calendarName":Ljava/lang/String;
    .end local v13    # "cur":Landroid/database/Cursor;
    .end local v15    # "key":Ljava/lang/String;
    .end local v18    # "value":Z
    :cond_4
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0
.end method

.method public static declared-synchronized forceCalendarRefresh()V
    .locals 4

    .prologue
    .line 126
    const-class v0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;

    monitor-enter v0

    const-wide/16 v2, 0x0

    :try_start_0
    sput-wide v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->lastReadTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit v0

    return-void

    .line 126
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getDateAndTime(J)Ljava/lang/String;
    .locals 6
    .param p0, "milliSeconds"    # J

    .prologue
    .line 366
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 367
    .local v0, "context":Landroid/content/Context;
    const-wide/16 v4, 0x0

    cmp-long v3, p0, v4

    if-nez v3, :cond_0

    .line 368
    const v3, 0x7f0804da

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 373
    :goto_0
    return-object v3

    .line 370
    :cond_0
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 372
    .local v1, "date":Ljava/util/Date;
    invoke-static {}, Ljava/text/SimpleDateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v2

    .line 373
    .local v2, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private static getSelectionForCalendarInstances(Z)Ljava/lang/String;
    .locals 6
    .param p0, "mustHaveLocation"    # Z

    .prologue
    .line 105
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 106
    .local v0, "now":J
    const-string v2, ""

    .line 108
    .local v2, "selection":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 109
    const-string v2, "eventLocation <> \'\' AND allDay = 0 AND "

    .line 114
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "begin > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/32 v4, 0x1b7740

    sub-long v4, v0, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "begin"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " < "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/32 v4, 0xa4cb800

    add-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "selfAttendeeStatus"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " <> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 122
    return-object v2
.end method

.method public static getTime(J)Ljava/lang/String;
    .locals 4
    .param p0, "milliSeconds"    # J

    .prologue
    .line 383
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 384
    .local v0, "date":Ljava/util/Date;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 385
    .local v1, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static listCalendars(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Calendar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    const-string v5, "android.permission.READ_CALENDAR"

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    .line 263
    const/16 v19, 0x0

    .line 338
    :goto_0
    return-object v19

    .line 266
    :cond_0
    const/4 v5, 0x7

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "calendar_displayName"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "calendar_color"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "account_name"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "account_type"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "ownerAccount"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "visible"

    aput-object v6, v4, v5

    .line 276
    .local v4, "EVENT_PROJECTION":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 277
    .local v2, "cr":Landroid/content/ContentResolver;
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    .line 278
    .local v3, "uri":Landroid/net/Uri;
    const/16 v16, 0x0

    .line 280
    .local v16, "cur":Landroid/database/Cursor;
    const/4 v5, 0x0

    const/4 v6, 0x0

    :try_start_0
    const-string v7, "account_type ASC, visible DESC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v16

    .line 284
    if-nez v16, :cond_1

    .line 285
    const/16 v19, 0x0

    .line 338
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 289
    :cond_1
    :try_start_1
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 290
    .local v19, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Calendar;>;"
    new-instance v5, Lcom/navdy/client/app/framework/models/Calendar;

    sget-object v6, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->GLOBAL_SWITCH:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    invoke-direct {v5, v6}, Lcom/navdy/client/app/framework/models/Calendar;-><init>(Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 293
    const/16 v18, 0x0

    .line 294
    .local v18, "lastAccountType":Ljava/lang/String;
    :goto_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 295
    const-string v5, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 296
    .local v8, "id":J
    const-string v5, "calendar_displayName"

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 297
    .local v10, "calendarDisplayName":Ljava/lang/String;
    const-string v5, "calendar_color"

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 298
    .local v11, "calendarColor":I
    const-string v5, "account_name"

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 299
    .local v12, "accountName":Ljava/lang/String;
    const-string v5, "account_type"

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 300
    .local v13, "accountType":Ljava/lang/String;
    const-string v5, "ownerAccount"

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 301
    .local v14, "ownerAccount":Ljava/lang/String;
    const-string v5, "visible"

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    const/4 v15, 0x1

    .line 315
    .local v15, "visible":Z
    :goto_2
    move-object/from16 v0, v18

    invoke-static {v13, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 316
    const-string v5, "com.google"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 317
    const v5, 0x7f0801e6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 318
    .local v17, "googleCalendar":Ljava/lang/String;
    new-instance v5, Lcom/navdy/client/app/framework/models/Calendar;

    sget-object v6, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->TITLE:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    move-object/from16 v0, v17

    invoke-direct {v5, v6, v0}, Lcom/navdy/client/app/framework/models/Calendar;-><init>(Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    .end local v17    # "googleCalendar":Ljava/lang/String;
    :goto_3
    move-object/from16 v18, v13

    .line 326
    :cond_2
    new-instance v6, Lcom/navdy/client/app/framework/models/Calendar;

    sget-object v7, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->CALENDAR:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    invoke-direct/range {v6 .. v15}, Lcom/navdy/client/app/framework/models/Calendar;-><init>(Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 338
    .end local v8    # "id":J
    .end local v10    # "calendarDisplayName":Ljava/lang/String;
    .end local v11    # "calendarColor":I
    .end local v12    # "accountName":Ljava/lang/String;
    .end local v13    # "accountType":Ljava/lang/String;
    .end local v14    # "ownerAccount":Ljava/lang/String;
    .end local v15    # "visible":Z
    .end local v18    # "lastAccountType":Ljava/lang/String;
    .end local v19    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Calendar;>;"
    :catchall_0
    move-exception v5

    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v5

    .line 301
    .restart local v8    # "id":J
    .restart local v10    # "calendarDisplayName":Ljava/lang/String;
    .restart local v11    # "calendarColor":I
    .restart local v12    # "accountName":Ljava/lang/String;
    .restart local v13    # "accountType":Ljava/lang/String;
    .restart local v14    # "ownerAccount":Ljava/lang/String;
    .restart local v18    # "lastAccountType":Ljava/lang/String;
    .restart local v19    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Calendar;>;"
    :cond_3
    const/4 v15, 0x0

    goto :goto_2

    .line 320
    .restart local v15    # "visible":Z
    :cond_4
    :try_start_2
    new-instance v5, Lcom/navdy/client/app/framework/models/Calendar;

    sget-object v6, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->TITLE:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    invoke-direct {v5, v6, v13}, Lcom/navdy/client/app/framework/models/Calendar;-><init>(Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 338
    .end local v8    # "id":J
    .end local v10    # "calendarDisplayName":Ljava/lang/String;
    .end local v11    # "calendarColor":I
    .end local v12    # "accountName":Ljava/lang/String;
    .end local v13    # "accountType":Ljava/lang/String;
    .end local v14    # "ownerAccount":Ljava/lang/String;
    .end local v15    # "visible":Z
    :cond_5
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0
.end method

.method private static parseLongSafely(Ljava/lang/String;)Ljava/lang/Long;
    .locals 5
    .param p0, "n"    # Ljava/lang/String;

    .prologue
    .line 348
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 349
    .local v1, "ret":Ljava/lang/Long;
    if-eqz p0, :cond_0

    .line 351
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 356
    :cond_0
    :goto_0
    return-object v1

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t parse a long out of this string: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static declared-synchronized readCalendarEvent(Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;Z)V
    .locals 41
    .param p0, "callback"    # Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;
    .param p1, "buildForSuggestions"    # Z

    .prologue
    .line 135
    const-class v40, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;

    monitor-enter v40

    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v24

    .line 136
    .local v24, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v34

    .line 138
    .local v34, "sharedPrefs":Landroid/content/SharedPreferences;
    if-nez p0, :cond_0

    .line 139
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Called readCalendarEvent with a null callback! This should never happen."

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    :goto_0
    monitor-exit v40

    return-void

    .line 143
    :cond_0
    :try_start_1
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 145
    .local v30, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    const-string v2, "android.permission.READ_CALENDAR"

    move-object/from16 v0, v24

    invoke-static {v0, v2}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 147
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Calendar Access not permitted."

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 148
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;->onCalendarEventsRead(Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 135
    .end local v24    # "context":Landroid/content/Context;
    .end local v30    # "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    .end local v34    # "sharedPrefs":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit v40

    throw v2

    .line 152
    .restart local v24    # "context":Landroid/content/Context;
    .restart local v30    # "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    .restart local v34    # "sharedPrefs":Landroid/content/SharedPreferences;
    :cond_1
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v32

    .line 153
    .local v32, "now":J
    sget-wide v10, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->lastReadTime:J

    sub-long v10, v32, v10

    const-wide/32 v12, 0x493e0

    cmp-long v2, v10, v12

    if-gez v2, :cond_2

    .line 154
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "It hasn\'t been long enough. Ignoring request"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 155
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->cachedEvents:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;->onCalendarEventsRead(Ljava/util/List;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 159
    :cond_2
    const/16 v25, 0x0

    .line 161
    .local v25, "cursor":Landroid/database/Cursor;
    :try_start_3
    sget-object v2, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v20

    .line 162
    .local v20, "builder":Landroid/net/Uri$Builder;
    const-wide/32 v10, 0x1b7740

    sub-long v10, v32, v10

    move-object/from16 v0, v20

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 163
    const-wide/32 v10, 0xa4cb800

    add-long v10, v10, v32

    move-object/from16 v0, v20

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 164
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 165
    .local v3, "contentUri":Landroid/net/Uri;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v19

    .line 167
    .local v19, "appContext":Landroid/content/Context;
    invoke-static/range {p1 .. p1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getSelectionForCalendarInstances(Z)Ljava/lang/String;

    move-result-object v5

    .line 169
    .local v5, "selection":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "buildForSuggestions: "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, ", selection: "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 172
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "title"

    aput-object v11, v4, v10

    const/4 v10, 0x1

    const-string v11, "begin"

    aput-object v11, v4, v10

    const/4 v10, 0x2

    const-string v11, "end"

    aput-object v11, v4, v10

    const/4 v10, 0x3

    const-string v11, "allDay"

    aput-object v11, v4, v10

    const/4 v10, 0x4

    const-string v11, "calendar_color"

    aput-object v11, v4, v10

    const/4 v10, 0x5

    const-string v11, "eventLocation"

    aput-object v11, v4, v10

    const/4 v10, 0x6

    const-string v11, "visible"

    aput-object v11, v4, v10

    const/4 v10, 0x7

    const-string v11, "calendar_id"

    aput-object v11, v4, v10

    const/4 v6, 0x0

    const-string v7, "begin ASC"

    .line 173
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 188
    if-eqz v25, :cond_8

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 189
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Found "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->getCount()I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, " Calendar Event(s) in the DB that matches the timebox criteria"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 191
    :cond_3
    const-string v2, "title"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 192
    .local v26, "displayNameColumnIndex":I
    const-string v2, "begin"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    .line 193
    .local v36, "startTimeColumnIndex":I
    const-string v2, "end"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 194
    .local v28, "endTimeColumnIndex":I
    const-string v2, "allDay"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 195
    .local v18, "allDayColumnIndex":I
    const-string v2, "calendar_color"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 196
    .local v23, "colorColumnIndex":I
    const-string v2, "eventLocation"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 197
    .local v31, "locationColumnIndex":I
    const-string v2, "visible"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    .line 198
    .local v39, "visibleColumnIndex":I
    const-string v2, "calendar_id"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 200
    .local v21, "calendarIdColumnIndex":I
    invoke-interface/range {v25 .. v26}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 201
    .local v7, "displayNameString":Ljava/lang/String;
    move-object/from16 v0, v25

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 202
    .local v37, "startTimeString":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->parseLongSafely(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v35

    .line 203
    .local v35, "startDateAndTime":Ljava/lang/Long;
    move-object/from16 v0, v25

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 204
    .local v29, "endTimeString":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->parseLongSafely(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v27

    .line 205
    .local v27, "endDateAndTime":Ljava/lang/Long;
    move-object/from16 v0, v25

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_4

    const/16 v17, 0x1

    .line 206
    .local v17, "allDay":Z
    :goto_1
    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 207
    .local v22, "colorARGB":I
    move-object/from16 v0, v25

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 208
    .local v14, "locationString":Ljava/lang/String;
    move-object/from16 v0, v25

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_5

    const/16 v38, 0x1

    .line 209
    .local v38, "visible":Z
    :goto_2
    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 211
    .local v8, "calendarId":J
    new-instance v6, Lcom/navdy/client/app/framework/models/CalendarEvent;

    .line 214
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 215
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move/from16 v0, v22

    int-to-long v15, v0

    invoke-direct/range {v6 .. v17}, Lcom/navdy/client/app/framework/models/CalendarEvent;-><init>(Ljava/lang/String;JJJLjava/lang/String;JZ)V

    .line 221
    .local v6, "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calendar_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v34

    move/from16 v1, v38

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_6

    .line 224
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ignoring calendar event: "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/CalendarEvent;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 229
    :goto_3
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 231
    if-eqz p1, :cond_7

    .line 232
    new-instance v2, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    new-instance v4, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$2;-><init>(Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;)V

    move-object/from16 v0, v30

    invoke-direct {v2, v0, v4}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;-><init>(Ljava/util/List;Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$NavigableCoordinateWorkerFinishCallback;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 257
    .end local v6    # "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    .end local v7    # "displayNameString":Ljava/lang/String;
    .end local v8    # "calendarId":J
    .end local v14    # "locationString":Ljava/lang/String;
    .end local v17    # "allDay":Z
    .end local v18    # "allDayColumnIndex":I
    .end local v21    # "calendarIdColumnIndex":I
    .end local v22    # "colorARGB":I
    .end local v23    # "colorColumnIndex":I
    .end local v26    # "displayNameColumnIndex":I
    .end local v27    # "endDateAndTime":Ljava/lang/Long;
    .end local v28    # "endTimeColumnIndex":I
    .end local v29    # "endTimeString":Ljava/lang/String;
    .end local v31    # "locationColumnIndex":I
    .end local v35    # "startDateAndTime":Ljava/lang/Long;
    .end local v36    # "startTimeColumnIndex":I
    .end local v37    # "startTimeString":Ljava/lang/String;
    .end local v38    # "visible":Z
    .end local v39    # "visibleColumnIndex":I
    :goto_4
    :try_start_4
    invoke-static/range {v25 .. v25}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 205
    .restart local v7    # "displayNameString":Ljava/lang/String;
    .restart local v18    # "allDayColumnIndex":I
    .restart local v21    # "calendarIdColumnIndex":I
    .restart local v23    # "colorColumnIndex":I
    .restart local v26    # "displayNameColumnIndex":I
    .restart local v27    # "endDateAndTime":Ljava/lang/Long;
    .restart local v28    # "endTimeColumnIndex":I
    .restart local v29    # "endTimeString":Ljava/lang/String;
    .restart local v31    # "locationColumnIndex":I
    .restart local v35    # "startDateAndTime":Ljava/lang/Long;
    .restart local v36    # "startTimeColumnIndex":I
    .restart local v37    # "startTimeString":Ljava/lang/String;
    .restart local v39    # "visibleColumnIndex":I
    :cond_4
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 208
    .restart local v14    # "locationString":Ljava/lang/String;
    .restart local v17    # "allDay":Z
    .restart local v22    # "colorARGB":I
    :cond_5
    const/16 v38, 0x0

    goto :goto_2

    .line 226
    .restart local v6    # "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    .restart local v8    # "calendarId":J
    .restart local v38    # "visible":Z
    :cond_6
    :try_start_5
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Adding calendar event: "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/CalendarEvent;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 227
    move-object/from16 v0, v30

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3

    .line 257
    .end local v3    # "contentUri":Landroid/net/Uri;
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    .end local v7    # "displayNameString":Ljava/lang/String;
    .end local v8    # "calendarId":J
    .end local v14    # "locationString":Ljava/lang/String;
    .end local v17    # "allDay":Z
    .end local v18    # "allDayColumnIndex":I
    .end local v19    # "appContext":Landroid/content/Context;
    .end local v20    # "builder":Landroid/net/Uri$Builder;
    .end local v21    # "calendarIdColumnIndex":I
    .end local v22    # "colorARGB":I
    .end local v23    # "colorColumnIndex":I
    .end local v26    # "displayNameColumnIndex":I
    .end local v27    # "endDateAndTime":Ljava/lang/Long;
    .end local v28    # "endTimeColumnIndex":I
    .end local v29    # "endTimeString":Ljava/lang/String;
    .end local v31    # "locationColumnIndex":I
    .end local v35    # "startDateAndTime":Ljava/lang/Long;
    .end local v36    # "startTimeColumnIndex":I
    .end local v37    # "startTimeString":Ljava/lang/String;
    .end local v38    # "visible":Z
    .end local v39    # "visibleColumnIndex":I
    :catchall_1
    move-exception v2

    :try_start_6
    invoke-static/range {v25 .. v25}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 250
    .restart local v3    # "contentUri":Landroid/net/Uri;
    .restart local v5    # "selection":Ljava/lang/String;
    .restart local v6    # "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    .restart local v7    # "displayNameString":Ljava/lang/String;
    .restart local v8    # "calendarId":J
    .restart local v14    # "locationString":Ljava/lang/String;
    .restart local v17    # "allDay":Z
    .restart local v18    # "allDayColumnIndex":I
    .restart local v19    # "appContext":Landroid/content/Context;
    .restart local v20    # "builder":Landroid/net/Uri$Builder;
    .restart local v21    # "calendarIdColumnIndex":I
    .restart local v22    # "colorARGB":I
    .restart local v23    # "colorColumnIndex":I
    .restart local v26    # "displayNameColumnIndex":I
    .restart local v27    # "endDateAndTime":Ljava/lang/Long;
    .restart local v28    # "endTimeColumnIndex":I
    .restart local v29    # "endTimeString":Ljava/lang/String;
    .restart local v31    # "locationColumnIndex":I
    .restart local v35    # "startDateAndTime":Ljava/lang/Long;
    .restart local v36    # "startTimeColumnIndex":I
    .restart local v37    # "startTimeString":Ljava/lang/String;
    .restart local v38    # "visible":Z
    .restart local v39    # "visibleColumnIndex":I
    :cond_7
    :try_start_7
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;->onCalendarEventsRead(Ljava/util/List;)V

    goto :goto_4

    .line 253
    .end local v6    # "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    .end local v7    # "displayNameString":Ljava/lang/String;
    .end local v8    # "calendarId":J
    .end local v14    # "locationString":Ljava/lang/String;
    .end local v17    # "allDay":Z
    .end local v18    # "allDayColumnIndex":I
    .end local v21    # "calendarIdColumnIndex":I
    .end local v22    # "colorARGB":I
    .end local v23    # "colorColumnIndex":I
    .end local v26    # "displayNameColumnIndex":I
    .end local v27    # "endDateAndTime":Ljava/lang/Long;
    .end local v28    # "endTimeColumnIndex":I
    .end local v29    # "endTimeString":Ljava/lang/String;
    .end local v31    # "locationColumnIndex":I
    .end local v35    # "startDateAndTime":Ljava/lang/Long;
    .end local v36    # "startTimeColumnIndex":I
    .end local v37    # "startTimeString":Ljava/lang/String;
    .end local v38    # "visible":Z
    .end local v39    # "visibleColumnIndex":I
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;->onCalendarEventsRead(Ljava/util/List;)V

    .line 254
    invoke-static/range {v30 .. v30}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->setCachedEvents(Ljava/util/List;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_4
.end method

.method private static removeAllSelectedCalendarsFromSharedPrefs()V
    .locals 7

    .prologue
    .line 441
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 442
    .local v4, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 443
    .local v0, "all":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 444
    .local v3, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 445
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 446
    .local v2, "key":Ljava/lang/String;
    const-string v6, "calendar_"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 447
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 450
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 451
    return-void
.end method

.method public static sendCalendarsToHud()V
    .locals 2

    .prologue
    .line 63
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    .line 64
    .local v0, "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->sendCalendarsToHud(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 65
    return-void
.end method

.method public static sendCalendarsToHud(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 3
    .param p0, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 68
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    :cond_0
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Not sending calendar events because not connected to HUD"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 98
    :goto_0
    return-void

    .line 73
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1;-><init>(Lcom/navdy/service/library/device/RemoteDevice;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static declared-synchronized setCachedEvents(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/CalendarEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "cachedEvents":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    const-class v1, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->lastReadTime:J

    .line 59
    sput-object p0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->cachedEvents:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    monitor-exit v1

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
