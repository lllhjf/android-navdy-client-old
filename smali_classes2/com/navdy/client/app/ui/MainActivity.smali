.class public Lcom/navdy/client/app/ui/MainActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "MainActivity.java"


# static fields
.field private static final SPLASH_TIMEOUT:I = 0x3e8


# instance fields
.field private appInstance:Lcom/navdy/client/app/framework/AppInstance;

.field private launchRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    .line 31
    new-instance v0, Lcom/navdy/client/app/ui/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/MainActivity$1;-><init>(Lcom/navdy/client/app/ui/MainActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/MainActivity;->launchRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/MainActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/MainActivity;->openMarketAppFor(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/MainActivity;J)V
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/MainActivity;
    .param p1, "x1"    # J

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/MainActivity;->initAndStartTheApp(J)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/MainActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/MainActivity;->openMarketAppFor(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/MainActivity;)Lcom/navdy/client/app/framework/AppInstance;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/MainActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/MainActivity;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/MainActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/MainActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/MainActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/MainActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/MainActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/MainActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/MainActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/MainActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/MainActivity;->launchRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/MainActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/MainActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/MainActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private initAndStartTheApp(J)V
    .locals 9
    .param p1, "timeBeforeGmsCheck"    # J

    .prologue
    .line 109
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 110
    .local v2, "now":J
    sub-long v6, v2, p1

    long-to-int v4, v6

    .line 111
    .local v4, "timeElapsedSinceStartedGmsCheck":I
    rsub-int v1, v4, 0x3e8

    .line 112
    .local v1, "remainingTime":I
    if-gez v1, :cond_0

    .line 113
    const/4 v1, 0x0

    .line 116
    :cond_0
    move v0, v1

    .line 117
    .local v0, "finalRemainingTime":I
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v5

    new-instance v6, Lcom/navdy/client/app/ui/MainActivity$4;

    invoke-direct {v6, p0, v0}, Lcom/navdy/client/app/ui/MainActivity$4;-><init>(Lcom/navdy/client/app/ui/MainActivity;I)V

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 140
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x7f080278

    const/4 v6, 0x0

    .line 50
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v0, 0x7f0300b7

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/MainActivity;->setContentView(I)V

    .line 53
    const v0, 0x7f1001ef

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 54
    .local v7, "imageView":Landroid/widget/ImageView;
    if-eqz v7, :cond_0

    .line 55
    const v0, 0x7f0201f4

    invoke-static {v7, v0, v6}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 58
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/MainActivity;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 60
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    .line 63
    .local v8, "timeBeforeGmsCheck":J
    sget-object v0, Lcom/navdy/client/app/ui/MainActivity$5;->$SwitchMap$com$navdy$client$app$framework$util$GmsUtils$GmsState:[I

    invoke-static {}, Lcom/navdy/client/app/framework/util/GmsUtils;->checkIfMissingOrOutOfDate()Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 106
    :goto_0
    return-void

    .line 65
    :pswitch_0
    const v1, 0x7f080300

    const v2, 0x7f080149

    const v3, 0x7f080267

    new-instance v5, Lcom/navdy/client/app/ui/MainActivity$2;

    invoke-direct {v5, p0, v8, v9}, Lcom/navdy/client/app/ui/MainActivity$2;-><init>(Lcom/navdy/client/app/ui/MainActivity;J)V

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/client/app/ui/MainActivity;->showQuestionDialog(IIIILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 84
    :pswitch_1
    const v1, 0x7f080329

    const v2, 0x7f080328

    const v3, 0x7f0804e2

    new-instance v5, Lcom/navdy/client/app/ui/MainActivity$3;

    invoke-direct {v5, p0, v8, v9}, Lcom/navdy/client/app/ui/MainActivity$3;-><init>(Lcom/navdy/client/app/ui/MainActivity;J)V

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/client/app/ui/MainActivity;->showQuestionDialog(IIIILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 103
    :pswitch_2
    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/MainActivity;->initAndStartTheApp(J)V

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/navdy/client/app/ui/MainActivity;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity;->launchRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 145
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onDestroy()V

    .line 146
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 159
    invoke-static {p0, p1}, Lcom/localytics/android/Localytics;->onNewIntent(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 160
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 151
    const-string v0, "Splash"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 152
    return-void
.end method
