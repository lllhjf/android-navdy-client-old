.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnRoute(Lcom/here/android/mpa/common/GeoPolyline;Lcom/here/android/mpa/mapping/Map$Animation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field final synthetic val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

.field final synthetic val$route:Lcom/here/android/mpa/common/GeoPolyline;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/common/GeoPolyline;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 389
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;->val$route:Lcom/here/android/mpa/common/GeoPolyline;

    iput-object p3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 408
    return-void
.end method

.method public onReady(Lcom/here/android/mpa/mapping/Map;)V
    .locals 4
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 392
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;->val$route:Lcom/here/android/mpa/common/GeoPolyline;

    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoPolyline;->getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v0

    .line 393
    .local v0, "geoBoundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    .line 398
    .local v1, "sanitizedAnimation":Lcom/here/android/mpa/mapping/Map$Animation;
    :goto_0
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/Map;->getHeight()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/Map;->getWidth()I

    move-result v2

    if-lez v2, :cond_0

    .line 399
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1700(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/common/ViewRect;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 400
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1700(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/common/ViewRect;

    move-result-object v2

    invoke-virtual {p1, v0, v2, v1, v3}, Lcom/here/android/mpa/mapping/Map;->zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 405
    :cond_0
    :goto_1
    return-void

    .line 393
    .end local v1    # "sanitizedAnimation":Lcom/here/android/mpa/mapping/Map$Animation;
    :cond_1
    sget-object v1, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    goto :goto_0

    .line 402
    .restart local v1    # "sanitizedAnimation":Lcom/here/android/mpa/mapping/Map$Animation;
    :cond_2
    invoke-virtual {p1, v0, v1, v3}, Lcom/here/android/mpa/mapping/Map;->zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    goto :goto_1
.end method
