.class public interface abstract Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnHereMapFragmentInitialized"
.end annotation


# virtual methods
.method public abstract onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .param p1    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onInit(Lcom/here/android/mpa/mapping/Map;)V
    .param p1    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
