.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field final synthetic val$finalView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->val$finalView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    const/4 v5, 0x0

    .line 369
    iget-object v6, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->val$finalView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 370
    .local v3, "width":I
    iget-object v6, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->val$finalView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 372
    .local v0, "height":I
    int-to-double v6, v3

    iget-object v8, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v8}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$600(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v8

    iget-object v9, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v9}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$500(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v9

    add-int/2addr v8, v9

    int-to-double v8, v8

    mul-double/2addr v8, v10

    cmpl-double v6, v6, v8

    if-lez v6, :cond_1

    move v1, v4

    .line 373
    .local v1, "paddingFitsSides":Z
    :goto_0
    int-to-double v6, v0

    iget-object v8, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v8}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$700(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v8

    iget-object v9, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v9}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$800(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v9

    add-int/2addr v8, v9

    int-to-double v8, v8

    mul-double/2addr v8, v10

    cmpl-double v6, v6, v8

    if-lez v6, :cond_2

    move v2, v4

    .line 375
    .local v2, "paddingFitsTopDown":Z
    :goto_1
    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    .line 376
    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v4, v5}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$702(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;I)I

    .line 377
    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v4, v5}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$802(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;I)I

    .line 383
    :cond_0
    :goto_2
    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v5, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7$1;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7$1;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;)V

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 390
    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->val$finalView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 391
    return-void

    .end local v1    # "paddingFitsSides":Z
    .end local v2    # "paddingFitsTopDown":Z
    :cond_1
    move v1, v5

    .line 372
    goto :goto_0

    .restart local v1    # "paddingFitsSides":Z
    :cond_2
    move v2, v5

    .line 373
    goto :goto_1

    .line 378
    .restart local v2    # "paddingFitsTopDown":Z
    :cond_3
    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    .line 379
    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v4, v5}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$502(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;I)I

    .line 380
    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v4, v5}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$602(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;I)I

    goto :goto_2
.end method
