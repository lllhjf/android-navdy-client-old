.class public Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;
.super Ljava/lang/Object;
.source "SerialValueAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;
    }
.end annotation


# instance fields
.field private animationListener:Lcom/navdy/client/app/ui/customviews/DefaultAnimationListener;

.field private mAdapter:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;

.field private mAnimationRunning:Z

.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mDuration:I

.field private mReverseDuration:I

.field private mValueQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;I)V
    .locals 2
    .param p1, "adapter"    # Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;
    .param p2, "animationDuration"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/16 v0, 0x64

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mReverseDuration:I

    .line 22
    new-instance v0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$1;-><init>(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->animationListener:Lcom/navdy/client/app/ui/customviews/DefaultAnimationListener;

    .line 36
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;

    .line 37
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAdapter:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;

    .line 38
    iput p2, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mDuration:I

    .line 39
    if-nez p1, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SerialValueAnimator cannot run without the adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;II)V
    .locals 2
    .param p1, "adapter"    # Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;
    .param p2, "animationDuration"    # I
    .param p3, "reverseAnimationDuration"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/16 v0, 0x64

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mReverseDuration:I

    .line 22
    new-instance v0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$1;-><init>(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->animationListener:Lcom/navdy/client/app/ui/customviews/DefaultAnimationListener;

    .line 45
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;

    .line 46
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAdapter:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;

    .line 47
    iput p2, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mDuration:I

    .line 48
    iput p3, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mReverseDuration:I

    .line 49
    if-nez p1, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SerialValueAnimator cannot run without the adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimationRunning:Z

    return p1
.end method

.method static synthetic access$202(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;F)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;
    .param p1, "x1"    # F

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->animate(F)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;)Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAdapter:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;

    return-object v0
.end method

.method private animate(F)V
    .locals 4
    .param p1, "value"    # F

    .prologue
    .line 73
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAdapter:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;

    invoke-interface {v2}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;->getValue()F

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    aput p1, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    .line 74
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAdapter:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;->getValue()F

    move-result v0

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mReverseDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 79
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 80
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$2;-><init>(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 87
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->animationListener:Lcom/navdy/client/app/ui/customviews/DefaultAnimationListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 88
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 89
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_0
.end method


# virtual methods
.method public release()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 93
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 96
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 98
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimationRunning:Z

    .line 100
    return-void
.end method

.method public setAnimationSpeeds(II)V
    .locals 0
    .param p1, "forwardAnimationDuration"    # I
    .param p2, "reverseAnimationDuration"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mDuration:I

    .line 69
    iput p2, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mReverseDuration:I

    .line 70
    return-void
.end method

.method public setValue(F)V
    .locals 2
    .param p1, "val"    # F

    .prologue
    .line 55
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAdapter:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;->getValue()F

    move-result v0

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_1

    .line 56
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimationRunning:Z

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 65
    :goto_0
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAnimationRunning:Z

    .line 60
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->animate(F)V

    goto :goto_0

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->mAdapter:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;

    invoke-interface {v0, p1}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;->setValue(F)V

    goto :goto_0
.end method
