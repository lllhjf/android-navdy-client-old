.class public Lcom/navdy/client/app/ui/customviews/PendingTripCardView;
.super Lcom/navdy/client/app/ui/customviews/TripCardView;
.source "PendingTripCardView.java"


# static fields
.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private callingActivity:Landroid/content/Context;

.field private final context:Landroid/content/Context;

.field private currentClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/customviews/TripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->callingActivity:Landroid/content/Context;

    .line 38
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->context:Landroid/content/Context;

    .line 39
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->callingActivity:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/PendingTripCardView;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->callingActivity:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->context:Landroid/content/Context;

    return-object v0
.end method

.method private setUpClickListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 1
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pendingRoute"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    if-ne p1, v0, :cond_0

    .line 49
    new-instance v0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;

    invoke-direct {v0, p0, p2}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;-><init>(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->currentClickListener:Landroid/view/View$OnClickListener;

    .line 70
    :goto_0
    return-void

    .line 62
    :cond_0
    new-instance v0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$2;-><init>(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->currentClickListener:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method


# virtual methods
.method protected appendPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080335

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleOnClick()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->currentClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->currentClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 76
    :cond_0
    return-void
.end method

.method public handleOnClick(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->callingActivity:Landroid/content/Context;

    .line 80
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->currentClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->callingActivity:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->currentClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 83
    :cond_0
    return-void
.end method

.method public onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pendingRoute"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lcom/navdy/client/app/ui/customviews/TripCardView;->onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->setUpClickListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    .line 45
    return-void
.end method
