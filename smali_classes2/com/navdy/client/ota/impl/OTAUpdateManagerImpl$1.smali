.class Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;
.super Ljava/lang/Object;
.source "OTAUpdateManagerImpl.java"

# interfaces
.implements Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->download(Lcom/navdy/client/ota/model/UpdateInfo;Ljava/io/File;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mStarted:Z

.field final synthetic this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

.field final synthetic val$info:Lcom/navdy/client/ota/model/UpdateInfo;


# direct methods
.method constructor <init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;Lcom/navdy/client/ota/model/UpdateInfo;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    iput-object p2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->val$info:Lcom/navdy/client/ota/model/UpdateInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->mStarted:Z

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/Exception;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 312
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "OTA on error: "

    invoke-virtual {p2}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 313
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    iget-object v0, v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    :goto_0
    return-void

    .line 316
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$3;-><init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;I)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onProgressChanged(IJJ)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "bytesCurrent"    # J
    .param p4, "bytesTotal"    # J

    .prologue
    .line 295
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    iget-object v0, v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    :goto_0
    return-void

    .line 298
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;-><init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;IJ)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onStateChanged(ILcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "state"    # Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    .prologue
    .line 251
    sget-object v0, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->COMPLETED:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->IN_PROGRESS:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->WAITING:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->PAUSED:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    if-eq p2, v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    iget-object v0, v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mTransferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    invoke-virtual {v0, p1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->cancel(I)Z

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    iget-object v0, v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    :goto_0
    return-void

    .line 257
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$1;-><init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;ILcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
