.class public interface abstract Lcom/navdy/client/ota/OTAUpdateListener;
.super Ljava/lang/Object;
.source "OTAUpdateListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;,
        Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;,
        Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;
    }
.end annotation


# virtual methods
.method public abstract onCheckForUpdateFinished(Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;Lcom/navdy/client/ota/model/UpdateInfo;)V
.end method

.method public abstract onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V
.end method

.method public abstract onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JJLcom/navdy/service/library/events/file/FileTransferError;)V
.end method
