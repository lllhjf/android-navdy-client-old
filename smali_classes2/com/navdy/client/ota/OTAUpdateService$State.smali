.class public final enum Lcom/navdy/client/ota/OTAUpdateService$State;
.super Ljava/lang/Enum;
.source "OTAUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/ota/OTAUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/ota/OTAUpdateService$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/ota/OTAUpdateService$State;

.field public static final enum DOWNLOADING_UPDATE:Lcom/navdy/client/ota/OTAUpdateService$State;

.field public static final enum READY_TO_INSTALL:Lcom/navdy/client/ota/OTAUpdateService$State;

.field public static final enum READY_TO_UPLOAD:Lcom/navdy/client/ota/OTAUpdateService$State;

.field public static final enum UPDATE_AVAILABLE:Lcom/navdy/client/ota/OTAUpdateService$State;

.field public static final enum UPLOADING:Lcom/navdy/client/ota/OTAUpdateService$State;

.field public static final enum UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 613
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateService$State;

    const-string v1, "UPDATE_AVAILABLE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/ota/OTAUpdateService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->UPDATE_AVAILABLE:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 617
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateService$State;

    const-string v1, "UP_TO_DATE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/ota/OTAUpdateService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 621
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateService$State;

    const-string v1, "DOWNLOADING_UPDATE"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/ota/OTAUpdateService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->DOWNLOADING_UPDATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 625
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateService$State;

    const-string v1, "READY_TO_UPLOAD"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/ota/OTAUpdateService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->READY_TO_UPLOAD:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 629
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateService$State;

    const-string v1, "UPLOADING"

    invoke-direct {v0, v1, v7}, Lcom/navdy/client/ota/OTAUpdateService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 633
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateService$State;

    const-string v1, "READY_TO_INSTALL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/ota/OTAUpdateService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->READY_TO_INSTALL:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 607
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/client/ota/OTAUpdateService$State;

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UPDATE_AVAILABLE:Lcom/navdy/client/ota/OTAUpdateService$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->DOWNLOADING_UPDATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->READY_TO_UPLOAD:Lcom/navdy/client/ota/OTAUpdateService$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateService$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateService$State;->READY_TO_INSTALL:Lcom/navdy/client/ota/OTAUpdateService$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->$VALUES:[Lcom/navdy/client/ota/OTAUpdateService$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 607
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/ota/OTAUpdateService$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 607
    const-class v0, Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/ota/OTAUpdateService$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/ota/OTAUpdateService$State;
    .locals 1

    .prologue
    .line 607
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->$VALUES:[Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-virtual {v0}, [Lcom/navdy/client/ota/OTAUpdateService$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/ota/OTAUpdateService$State;

    return-object v0
.end method
