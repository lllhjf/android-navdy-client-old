.class Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;
.super Ljava/lang/Object;
.source "NavAddressPickerFragment.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->doAddressSearch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

.field final synthetic val$queryText:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    .prologue
    .line 410
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->val$queryText:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 5
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 413
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    if-eqz v1, :cond_0

    .line 414
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    invoke-virtual {v1}, Lcom/here/android/mpa/search/GeocodeRequest;->cancel()Z

    .line 416
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    new-instance v2, Lcom/here/android/mpa/search/GeocodeRequest;

    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->val$queryText:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/here/android/mpa/search/GeocodeRequest;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v3, v3, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mCurrentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v3

    const v4, 0x2944a

    invoke-virtual {v2, v3, v4}, Lcom/here/android/mpa/search/GeocodeRequest;->setSearchArea(Lcom/here/android/mpa/common/GeoCoordinate;I)Lcom/here/android/mpa/search/GeocodeRequest;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    .line 418
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    if-nez v1, :cond_2

    .line 419
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    const-string v2, "Unable to search."

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 450
    :cond_1
    :goto_0
    return-void

    .line 423
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    new-instance v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;-><init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;)V

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/GeocodeRequest;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v0

    .line 447
    .local v0, "errorCode":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v1, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v0, v1, :cond_1

    .line 448
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Search error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/here/android/mpa/search/ErrorCode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    goto :goto_0
.end method
