.class Lcom/navdy/client/debug/gesture/GestureControlFragment$3;
.super Ljava/lang/Object;
.source "GestureControlFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/gesture/GestureControlFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/gesture/GestureControlFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/gesture/GestureControlFragment;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 145
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-static {v1}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$200(Lcom/navdy/client/debug/gesture/GestureControlFragment;)Lcom/navdy/service/library/events/input/Gesture;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_TO_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    if-ne v1, v2, :cond_0

    .line 146
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    invoke-static {v1, v2}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$202(Lcom/navdy/client/debug/gesture/GestureControlFragment;Lcom/navdy/service/library/events/input/Gesture;)Lcom/navdy/service/library/events/input/Gesture;

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-static {v1}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$200(Lcom/navdy/client/debug/gesture/GestureControlFragment;)Lcom/navdy/service/library/events/input/Gesture;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_UP:Lcom/navdy/service/library/events/input/Gesture;

    if-ne v1, v2, :cond_1

    .line 149
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER:Lcom/navdy/service/library/events/input/Gesture;

    invoke-static {v1, v2}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$202(Lcom/navdy/client/debug/gesture/GestureControlFragment;Lcom/navdy/service/library/events/input/Gesture;)Lcom/navdy/service/library/events/input/Gesture;

    .line 151
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-static {v1}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$200(Lcom/navdy/client/debug/gesture/GestureControlFragment;)Lcom/navdy/service/library/events/input/Gesture;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PINCH_TO_FINGER:Lcom/navdy/service/library/events/input/Gesture;

    if-ne v1, v2, :cond_2

    .line 152
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER:Lcom/navdy/service/library/events/input/Gesture;

    invoke-static {v1, v2}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$202(Lcom/navdy/client/debug/gesture/GestureControlFragment;Lcom/navdy/service/library/events/input/Gesture;)Lcom/navdy/service/library/events/input/Gesture;

    .line 154
    :cond_2
    new-instance v0, Lcom/navdy/service/library/events/input/GestureEvent;

    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-static {v1}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$200(Lcom/navdy/client/debug/gesture/GestureControlFragment;)Lcom/navdy/service/library/events/input/Gesture;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 155
    .local v0, "event":Lcom/navdy/service/library/events/input/GestureEvent;
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-static {v1, v0}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$100(Lcom/navdy/client/debug/gesture/GestureControlFragment;Lcom/squareup/wire/Message;)V

    .line 156
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 159
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 162
    return-void
.end method
