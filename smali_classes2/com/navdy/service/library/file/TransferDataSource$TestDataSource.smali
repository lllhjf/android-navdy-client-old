.class Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;
.super Lcom/navdy/service/library/file/TransferDataSource;
.source "TransferDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/file/TransferDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TestDataSource"
.end annotation


# static fields
.field static final DATA_BUFFER_LEN:I = 0x1000


# instance fields
.field mDataBuffer:[B

.field mLength:J


# direct methods
.method constructor <init>(J)V
    .locals 3
    .param p1, "len"    # J

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/navdy/service/library/file/TransferDataSource;-><init>()V

    .line 74
    iput-wide p1, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mLength:J

    .line 75
    const/16 v1, 0x1000

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mDataBuffer:[B

    .line 76
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 77
    .local v0, "r":Ljava/util/Random;
    iget-object v1, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mDataBuffer:[B

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextBytes([B)V

    .line 78
    return-void
.end method


# virtual methods
.method public checkSum()Ljava/lang/String;
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mLength:J

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->checkSum(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public checkSum(J)Ljava/lang/String;
    .locals 11
    .param p1, "length"    # J

    .prologue
    .line 100
    const/4 v3, 0x0

    .line 101
    .local v3, "res":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 102
    .local v4, "offset":J
    iget-wide v8, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mLength:J

    cmp-long v8, p1, v8

    if-lez v8, :cond_0

    .line 103
    iget-wide p1, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mLength:J

    .line 106
    :cond_0
    :try_start_0
    const-string v8, "MD5"

    invoke-static {v8}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 107
    .local v2, "msgDigest":Ljava/security/MessageDigest;
    :goto_0
    cmp-long v8, v4, p1

    if-gez v8, :cond_2

    .line 108
    sub-long v6, p1, v4

    .line 109
    .local v6, "remaining":J
    const-wide/16 v8, 0x1000

    cmp-long v8, v6, v8

    if-gez v8, :cond_1

    long-to-int v1, v6

    .line 110
    .local v1, "l":I
    :goto_1
    iget-object v8, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mDataBuffer:[B

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9, v1}, Ljava/security/MessageDigest;->update([BII)V

    .line 111
    int-to-long v8, v1

    add-long/2addr v4, v8

    .line 112
    goto :goto_0

    .line 109
    .end local v1    # "l":I
    :cond_1
    const/16 v1, 0x1000

    goto :goto_1

    .line 113
    .end local v6    # "remaining":J
    :cond_2
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->bytesToHexString([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 117
    .end local v2    # "msgDigest":Ljava/security/MessageDigest;
    :goto_2
    return-object v3

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-static {}, Lcom/navdy/service/library/file/TransferDataSource;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "unable to get MD5 algorithm"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    const-string v0, "<TESTDATA>"

    return-object v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mLength:J

    return-wide v0
.end method

.method public read([B)I
    .locals 10
    .param p1, "b"    # [B

    .prologue
    .line 85
    iget-wide v6, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mLength:J

    const-wide/16 v8, 0x1000

    rem-long/2addr v6, v8

    long-to-int v0, v6

    .line 86
    .local v0, "di":I
    iget-wide v6, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mLength:J

    iget-wide v8, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mCurOffset:J

    sub-long v4, v6, v8

    .line 87
    .local v4, "remaining":J
    array-length v3, p1

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    array-length v2, p1

    .line 88
    .local v2, "len":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 89
    iget-object v3, p0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;->mDataBuffer:[B

    aget-byte v3, v3, v0

    aput-byte v3, p1, v1

    .line 90
    add-int/lit8 v3, v0, 0x1

    rem-int/lit16 v0, v3, 0x1000

    .line 88
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 87
    .end local v1    # "i":I
    .end local v2    # "len":I
    :cond_0
    long-to-int v2, v4

    goto :goto_0

    .line 92
    .restart local v1    # "i":I
    .restart local v2    # "len":I
    :cond_1
    return v2
.end method
