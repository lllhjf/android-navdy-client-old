.class public final Lcom/navdy/service/library/events/debug/DriveRecordingsRequest;
.super Lcom/squareup/wire/Message;
.source "DriveRecordingsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/debug/DriveRecordingsRequest$Builder;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 15
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/debug/DriveRecordingsRequest$Builder;)V
    .locals 0
    .param p1, "builder"    # Lcom/navdy/service/library/events/debug/DriveRecordingsRequest$Builder;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 18
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/debug/DriveRecordingsRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 19
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/debug/DriveRecordingsRequest$Builder;Lcom/navdy/service/library/events/debug/DriveRecordingsRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/debug/DriveRecordingsRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/debug/DriveRecordingsRequest$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/debug/DriveRecordingsRequest;-><init>(Lcom/navdy/service/library/events/debug/DriveRecordingsRequest$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 23
    instance-of v0, p1, Lcom/navdy/service/library/events/debug/DriveRecordingsRequest;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method
