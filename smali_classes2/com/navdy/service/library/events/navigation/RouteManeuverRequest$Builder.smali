.class public final Lcom/navdy/service/library/events/navigation/RouteManeuverRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RouteManeuverRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public routeId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 56
    if-nez p1, :cond_0

    .line 58
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;->routeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest$Builder;->routeId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest$Builder;->checkRequiredFields()V

    .line 71
    new-instance v0, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;-><init>(Lcom/navdy/service/library/events/navigation/RouteManeuverRequest$Builder;Lcom/navdy/service/library/events/navigation/RouteManeuverRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;

    move-result-object v0

    return-object v0
.end method

.method public routeId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/RouteManeuverRequest$Builder;
    .locals 0
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest$Builder;->routeId:Ljava/lang/String;

    .line 65
    return-object p0
.end method
