.class public final enum Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;
.super Ljava/lang/Enum;
.source "SpeechRequestStatus.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/SpeechRequestStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SpeechRequestStatusType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

.field public static final enum SPEECH_REQUEST_STARTING:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

.field public static final enum SPEECH_REQUEST_STOPPED:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 105
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    const-string v1, "SPEECH_REQUEST_STARTING"

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->SPEECH_REQUEST_STARTING:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    .line 109
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    const-string v1, "SPEECH_REQUEST_STOPPED"

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->SPEECH_REQUEST_STOPPED:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    .line 100
    new-array v0, v4, [Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->SPEECH_REQUEST_STARTING:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->SPEECH_REQUEST_STOPPED:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->$VALUES:[Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 114
    iput p3, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->value:I

    .line 115
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 100
    const-class v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->$VALUES:[Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->value:I

    return v0
.end method
