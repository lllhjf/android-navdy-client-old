.class Lcom/navdy/service/library/device/connection/Connection$4;
.super Ljava/lang/Object;
.source "Connection.java"

# interfaces
.implements Lcom/navdy/service/library/device/connection/Connection$EventDispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/device/connection/Connection;->dispatchDisconnectEvent(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/connection/Connection;

.field final synthetic val$cause:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/connection/Connection;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/Connection$4;->this$0:Lcom/navdy/service/library/device/connection/Connection;

    iput-object p2, p0, Lcom/navdy/service/library/device/connection/Connection$4;->val$cause:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchEvent(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$Listener;)V
    .locals 1
    .param p1, "source"    # Lcom/navdy/service/library/device/connection/Connection;
    .param p2, "listener"    # Lcom/navdy/service/library/device/connection/Connection$Listener;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/Connection$4;->val$cause:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    invoke-interface {p2, p1, v0}, Lcom/navdy/service/library/device/connection/Connection$Listener;->onDisconnected(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    .line 174
    return-void
.end method

.method public bridge synthetic dispatchEvent(Lcom/navdy/service/library/util/Listenable;Lcom/navdy/service/library/util/Listenable$Listener;)V
    .locals 0

    .prologue
    .line 170
    check-cast p1, Lcom/navdy/service/library/device/connection/Connection;

    check-cast p2, Lcom/navdy/service/library/device/connection/Connection$Listener;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/service/library/device/connection/Connection$4;->dispatchEvent(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$Listener;)V

    return-void
.end method
