.class public abstract Lcom/navdy/service/library/device/connection/ConnectionListener;
.super Lcom/navdy/service/library/util/Listenable;
.source "ConnectionListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;,
        Lcom/navdy/service/library/device/connection/ConnectionListener$Listener;,
        Lcom/navdy/service/library/device/connection/ConnectionListener$EventDispatcher;
    }
.end annotation


# instance fields
.field protected final logger:Lcom/navdy/service/library/log/Logger;

.field mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;

.field protected final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/service/library/util/Listenable;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mContext:Landroid/content/Context;

    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-direct {v0, p2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->logger:Lcom/navdy/service/library/log/Logger;

    .line 32
    return-void
.end method


# virtual methods
.method public dispatchConnected(Lcom/navdy/service/library/device/connection/Connection;)V
    .locals 1
    .param p1, "connection"    # Lcom/navdy/service/library/device/connection/Connection;

    .prologue
    .line 106
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionListener$4;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionListener$4;-><init>(Lcom/navdy/service/library/device/connection/ConnectionListener;Lcom/navdy/service/library/device/connection/Connection;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionListener;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 112
    return-void
.end method

.method public dispatchConnectionFailed()V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionListener$5;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/ConnectionListener$5;-><init>(Lcom/navdy/service/library/device/connection/ConnectionListener;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionListener;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 121
    return-void
.end method

.method public dispatchStartFailure()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionListener$2;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/ConnectionListener$2;-><init>(Lcom/navdy/service/library/device/connection/ConnectionListener;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionListener;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 94
    return-void
.end method

.method public dispatchStarted()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionListener$1;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/ConnectionListener$1;-><init>(Lcom/navdy/service/library/device/connection/ConnectionListener;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionListener;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 85
    return-void
.end method

.method public dispatchStopped()V
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionListener$3;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/ConnectionListener$3;-><init>(Lcom/navdy/service/library/device/connection/ConnectionListener;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionListener;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 103
    return-void
.end method

.method protected abstract getNewAcceptThread()Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getType()Lcom/navdy/service/library/device/connection/ConnectionType;
.end method

.method public declared-synchronized start()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 36
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;

    if-eqz v2, :cond_1

    .line 37
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Already running"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :goto_0
    monitor-exit p0

    return v1

    .line 41
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "clearing existing accept thread"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 42
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;->cancel()V

    .line 44
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 48
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionListener;->getNewAcceptThread()Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 54
    :try_start_3
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;

    if-eqz v1, :cond_2

    .line 55
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;->start()V

    .line 57
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to start accept thread: "

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionListener;->dispatchStartFailure()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 36
    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized stop()Z
    .locals 2

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Already stopped."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    const/4 v0, 0x0

    .line 67
    :goto_0
    monitor-exit p0

    return v0

    .line 65
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;->cancel()V

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionListener;->mAcceptThread:Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67
    const/4 v0, 0x1

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
