.class final Lcom/navdy/service/library/device/connection/Connection$1;
.super Ljava/lang/Object;
.source "Connection.java"

# interfaces
.implements Lcom/navdy/service/library/device/connection/Connection$ConnectionFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/Connection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build(Landroid/content/Context;Lcom/navdy/service/library/device/connection/ConnectionInfo;)Lcom/navdy/service/library/device/connection/Connection;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 81
    sget-object v2, Lcom/navdy/service/library/device/connection/Connection$5;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    invoke-virtual {p2}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/ConnectionType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 97
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown connection class for type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 86
    :pswitch_0
    invoke-virtual {p2}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getAddress()Lcom/navdy/service/library/device/connection/ServiceAddress;

    move-result-object v0

    .line 87
    .local v0, "address":Lcom/navdy/service/library/device/connection/ServiceAddress;
    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getService()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionService;->ACCESSORY_IAP2:Ljava/util/UUID;

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    new-instance v1, Lcom/navdy/service/library/device/connection/ServiceAddress;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getAddress()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionService;->DEVICE_IAP2:Ljava/util/UUID;

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getProtocol()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/navdy/service/library/device/connection/ServiceAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "address":Lcom/navdy/service/library/device/connection/ServiceAddress;
    .local v1, "address":Lcom/navdy/service/library/device/connection/ServiceAddress;
    move-object v0, v1

    .line 91
    .end local v1    # "address":Lcom/navdy/service/library/device/connection/ServiceAddress;
    .restart local v0    # "address":Lcom/navdy/service/library/device/connection/ServiceAddress;
    :cond_0
    new-instance v2, Lcom/navdy/service/library/device/connection/SocketConnection;

    new-instance v3, Lcom/navdy/service/library/network/BTSocketFactory;

    invoke-direct {v3, v0}, Lcom/navdy/service/library/network/BTSocketFactory;-><init>(Lcom/navdy/service/library/device/connection/ServiceAddress;)V

    invoke-direct {v2, p2, v3}, Lcom/navdy/service/library/device/connection/SocketConnection;-><init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;Lcom/navdy/service/library/network/SocketFactory;)V

    .line 95
    .end local v0    # "address":Lcom/navdy/service/library/device/connection/ServiceAddress;
    :goto_0
    return-object v2

    .line 94
    :pswitch_1
    new-instance v2, Lcom/navdy/service/library/device/connection/SocketConnection;

    new-instance v3, Lcom/navdy/service/library/network/TCPSocketFactory;

    .line 95
    invoke-virtual {p2}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getAddress()Lcom/navdy/service/library/device/connection/ServiceAddress;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/navdy/service/library/network/TCPSocketFactory;-><init>(Lcom/navdy/service/library/device/connection/ServiceAddress;)V

    invoke-direct {v2, p2, v3}, Lcom/navdy/service/library/device/connection/SocketConnection;-><init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;Lcom/navdy/service/library/network/SocketFactory;)V

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
