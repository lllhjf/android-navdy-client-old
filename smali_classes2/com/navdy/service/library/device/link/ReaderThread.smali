.class public Lcom/navdy/service/library/device/link/ReaderThread;
.super Lcom/navdy/service/library/device/link/IOThread;
.source "ReaderThread.java"


# instance fields
.field private final connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

.field private isNetworkLinkReadyByDefault:Z

.field private final linkListener:Lcom/navdy/service/library/device/link/LinkListener;

.field private final mmEventReader:Lcom/navdy/service/library/events/NavdyEventReader;

.field private mmInStream:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionType;Ljava/io/InputStream;Lcom/navdy/service/library/device/link/LinkListener;Z)V
    .locals 2
    .param p1, "connectionType"    # Lcom/navdy/service/library/device/connection/ConnectionType;
    .param p2, "inputStream"    # Ljava/io/InputStream;
    .param p3, "listener"    # Lcom/navdy/service/library/device/link/LinkListener;
    .param p4, "isNetworkLinkReadyWhenConnected"    # Z

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/service/library/device/link/IOThread;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/service/library/device/link/ReaderThread;->isNetworkLinkReadyByDefault:Z

    .line 26
    const-string v0, "ReaderThread"

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/link/ReaderThread;->setName(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "create ReaderThread"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 28
    iput-object p2, p0, Lcom/navdy/service/library/device/link/ReaderThread;->mmInStream:Ljava/io/InputStream;

    .line 29
    new-instance v0, Lcom/navdy/service/library/events/NavdyEventReader;

    iget-object v1, p0, Lcom/navdy/service/library/device/link/ReaderThread;->mmInStream:Ljava/io/InputStream;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/NavdyEventReader;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/link/ReaderThread;->mmEventReader:Lcom/navdy/service/library/events/NavdyEventReader;

    .line 30
    iput-object p3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    .line 31
    iput-object p1, p0, Lcom/navdy/service/library/device/link/ReaderThread;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 32
    iput-boolean p4, p0, Lcom/navdy/service/library/device/link/ReaderThread;->isNetworkLinkReadyByDefault:Z

    .line 33
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/navdy/service/library/device/link/IOThread;->cancel()V

    .line 89
    iget-object v0, p0, Lcom/navdy/service/library/device/link/ReaderThread;->mmInStream:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/device/link/ReaderThread;->mmInStream:Ljava/io/InputStream;

    .line 91
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 36
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "BEGIN"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 38
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->NORMAL:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .line 40
    .local v0, "cause":Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    iget-object v4, p0, Lcom/navdy/service/library/device/link/ReaderThread;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-interface {v3, v4}, Lcom/navdy/service/library/device/link/LinkListener;->linkEstablished(Lcom/navdy/service/library/device/connection/ConnectionType;)V

    .line 41
    iget-boolean v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->isNetworkLinkReadyByDefault:Z

    if-eqz v3, :cond_0

    .line 42
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    invoke-interface {v3}, Lcom/navdy/service/library/device/link/LinkListener;->onNetworkLinkReady()V

    .line 46
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->closing:Z

    if-nez v3, :cond_2

    .line 49
    :try_start_0
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 50
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "before read"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 52
    :cond_1
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->mmEventReader:Lcom/navdy/service/library/events/NavdyEventReader;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/NavdyEventReader;->readBytes()[B

    move-result-object v2

    .line 54
    .local v2, "eventData":[B
    if-eqz v2, :cond_3

    array-length v3, v2

    const/high16 v4, 0x80000

    if-le v3, v4, :cond_3

    .line 55
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reader Max packet size exceeded ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] bytes["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x32

    .line 57
    invoke-static {v2, v5, v6}, Lcom/navdy/service/library/util/IOUtils;->bytesToHexString([BII)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v2    # "eventData":[B
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Ljava/lang/Exception;
    iget-boolean v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->closing:Z

    if-nez v3, :cond_2

    .line 73
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "disconnected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 74
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->ABORTED:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .line 79
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Signaling connection lost cause:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 80
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    iget-object v4, p0, Lcom/navdy/service/library/device/link/ReaderThread;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-interface {v3, v4, v0}, Lcom/navdy/service/library/device/link/LinkListener;->linkLost(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    .line 81
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exiting thread"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 83
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->mmInStream:Ljava/io/InputStream;

    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 84
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->mmInStream:Ljava/io/InputStream;

    .line 85
    return-void

    .line 60
    .restart local v2    # "eventData":[B
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 61
    iget-object v4, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "after read len:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v2, :cond_5

    const/4 v3, -0x1

    :goto_2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 63
    :cond_4
    if-nez v2, :cond_6

    .line 64
    iget-boolean v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->closing:Z

    if-nez v3, :cond_2

    .line 65
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "no event data parsed. assuming disconnect."

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 66
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->ABORTED:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    goto :goto_1

    .line 61
    :cond_5
    array-length v3, v2

    goto :goto_2

    .line 70
    :cond_6
    iget-object v3, p0, Lcom/navdy/service/library/device/link/ReaderThread;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    invoke-interface {v3, v2}, Lcom/navdy/service/library/device/link/LinkListener;->onNavdyEventReceived([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
