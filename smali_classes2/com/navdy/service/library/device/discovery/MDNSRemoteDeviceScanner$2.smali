.class Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;
.super Ljava/lang/Object;
.source "MDNSRemoteDeviceScanner.java"

# interfaces
.implements Landroid/net/nsd/NsdManager$DiscoveryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->initializeDiscoveryListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDiscoveryStarted(Ljava/lang/String;)V
    .locals 2
    .param p1, "regType"    # Ljava/lang/String;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryRunning:Z

    .line 124
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->discoveryTimer:Ljava/util/Timer;

    .line 125
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Service discovery started"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->dispatchOnScanStarted()V

    .line 127
    return-void
.end method

.method public onDiscoveryStopped(Ljava/lang/String;)V
    .locals 3
    .param p1, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryRunning:Z

    .line 149
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Discovery stopped: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->dispatchOnScanStopped()V

    .line 151
    return-void
.end method

.method public onServiceFound(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 3
    .param p1, "service"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    .line 132
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Service discovery success "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 134
    invoke-static {p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->isValidNavdyServiceInfo(Landroid/net/nsd/NsdServiceInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    invoke-virtual {v0, p1}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->resolveService(Landroid/net/nsd/NsdServiceInfo;)V

    .line 137
    :cond_0
    return-void
.end method

.method public onServiceLost(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 3
    .param p1, "service"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    .line 143
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "service lost"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method public onStartDiscoveryFailed(Ljava/lang/String;I)V
    .locals 3
    .param p1, "serviceType"    # Ljava/lang/String;
    .param p2, "errorCode"    # I

    .prologue
    .line 155
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start Discovery failed: Error code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method public onStopDiscoveryFailed(Ljava/lang/String;I)V
    .locals 3
    .param p1, "serviceType"    # Ljava/lang/String;
    .param p2, "errorCode"    # I

    .prologue
    .line 161
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stop Discovery failed: Error code:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 163
    return-void
.end method
