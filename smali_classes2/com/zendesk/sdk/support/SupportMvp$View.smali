.class public interface abstract Lcom/zendesk/sdk/support/SupportMvp$View;
.super Ljava/lang/Object;
.source "SupportMvp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/SupportMvp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation


# virtual methods
.method public abstract clearSearchResults()V
.end method

.method public abstract dismissError()V
.end method

.method public abstract exitActivity()V
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract hideLoadingState()V
.end method

.method public abstract isShowingHelp()Z
.end method

.method public abstract showContactUsButton()V
.end method

.method public abstract showContactZendesk()V
.end method

.method public abstract showError(I)V
.end method

.method public abstract showErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V
.end method

.method public abstract showHelp(Lcom/zendesk/sdk/support/SupportUiConfig;)V
.end method

.method public abstract showLoadingState()V
.end method

.method public abstract showRequestList()V
.end method

.method public abstract showSearchResults(Ljava/util/List;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract startWithHelp()V
.end method
