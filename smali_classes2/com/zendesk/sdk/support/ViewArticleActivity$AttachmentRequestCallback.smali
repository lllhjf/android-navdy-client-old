.class Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ViewArticleActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/ViewArticleActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AttachmentRequestCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/ViewArticleActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;

    .prologue
    .line 528
    iput-object p1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 2
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    sget-object v1, Lcom/zendesk/sdk/ui/LoadingState;->ERRORED:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 542
    invoke-static {}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Lcom/zendesk/service/ErrorResponse;)V

    .line 543
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 528
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 532
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Attachment;>;"
    new-instance v0, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentAdapter;

    iget-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-direct {v0, v1, p1}, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 533
    .local v0, "adapter":Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentAdapter;
    iget-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$600(Lcom/zendesk/sdk/support/ViewArticleActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 534
    iget-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$600(Lcom/zendesk/sdk/support/ViewArticleActivity;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 535
    iget-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$600(Lcom/zendesk/sdk/support/ViewArticleActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$700(Landroid/widget/ListView;)V

    .line 536
    iget-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    sget-object v2, Lcom/zendesk/sdk/ui/LoadingState;->DISPLAYING:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/support/ViewArticleActivity;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 537
    return-void
.end method
