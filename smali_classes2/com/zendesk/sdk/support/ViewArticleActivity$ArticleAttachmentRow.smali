.class Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;
.super Landroid/widget/RelativeLayout;
.source "ViewArticleActivity.java"

# interfaces
.implements Lcom/zendesk/sdk/ui/ListRowView;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/ViewArticleActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArticleAttachmentRow"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/RelativeLayout;",
        "Lcom/zendesk/sdk/ui/ListRowView",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mFileName:Landroid/widget/TextView;

.field private mFileSize:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 586
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 587
    iput-object p1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->mContext:Landroid/content/Context;

    .line 588
    invoke-direct {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->initialise()V

    .line 589
    return-void
.end method

.method private initialise()V
    .locals 3

    .prologue
    .line 593
    iget-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$layout;->row_article_attachment:I

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 595
    .local v0, "container":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 596
    sget v1, Lcom/zendesk/sdk/R$id;->article_attachment_row_filename_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->mFileName:Landroid/widget/TextView;

    .line 597
    sget v1, Lcom/zendesk/sdk/R$id;->article_attachment_row_filesize_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->mFileSize:Landroid/widget/TextView;

    .line 599
    :cond_0
    return-void
.end method


# virtual methods
.method public bind(Lcom/zendesk/sdk/model/helpcenter/Attachment;)V
    .locals 2
    .param p1, "attachment"    # Lcom/zendesk/sdk/model/helpcenter/Attachment;

    .prologue
    .line 605
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->mFileName:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/Attachment;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 606
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->mFileSize:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/Attachment;->getSize()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/FileUtils;->humanReadableFileSize(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 608
    return-void
.end method

.method public bridge synthetic bind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 579
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/Attachment;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->bind(Lcom/zendesk/sdk/model/helpcenter/Attachment;)V

    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 612
    return-object p0
.end method
