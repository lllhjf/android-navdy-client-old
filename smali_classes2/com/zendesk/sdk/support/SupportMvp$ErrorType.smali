.class public final enum Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
.super Ljava/lang/Enum;
.source "SupportMvp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/SupportMvp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/support/SupportMvp$ErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

.field public static final enum ARTICLES_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

.field public static final enum CATEGORY_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

.field public static final enum SECTION_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    const-string v1, "CATEGORY_LOAD"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->CATEGORY_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    .line 24
    new-instance v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    const-string v1, "SECTION_LOAD"

    invoke-direct {v0, v1, v3}, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->SECTION_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    .line 25
    new-instance v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    const-string v1, "ARTICLES_LOAD"

    invoke-direct {v0, v1, v4}, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->ARTICLES_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    sget-object v1, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->CATEGORY_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->SECTION_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->ARTICLES_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->$VALUES:[Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->$VALUES:[Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    return-object v0
.end method
