.class public Lcom/zendesk/sdk/storage/StorageModule;
.super Ljava/lang/Object;
.source "StorageModule.java"


# instance fields
.field private final helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

.field private final identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

.field private final requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

.field private final sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

.field private final sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/storage/SdkStorage;Lcom/zendesk/sdk/storage/IdentityStorage;Lcom/zendesk/sdk/storage/RequestStorage;Lcom/zendesk/sdk/storage/SdkSettingsStorage;Lcom/zendesk/sdk/storage/HelpCenterSessionCache;)V
    .locals 0
    .param p1, "sdkStorage"    # Lcom/zendesk/sdk/storage/SdkStorage;
    .param p2, "identityStorage"    # Lcom/zendesk/sdk/storage/IdentityStorage;
    .param p3, "requestStorage"    # Lcom/zendesk/sdk/storage/RequestStorage;
    .param p4, "sdkSettingsStorage"    # Lcom/zendesk/sdk/storage/SdkSettingsStorage;
    .param p5, "sessionCache"    # Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/zendesk/sdk/storage/StorageModule;->sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;

    .line 29
    iput-object p2, p0, Lcom/zendesk/sdk/storage/StorageModule;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    .line 30
    iput-object p3, p0, Lcom/zendesk/sdk/storage/StorageModule;->requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

    .line 31
    iput-object p4, p0, Lcom/zendesk/sdk/storage/StorageModule;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    .line 32
    iput-object p5, p0, Lcom/zendesk/sdk/storage/StorageModule;->helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    .line 33
    return-void
.end method


# virtual methods
.method public getHelpCenterSessionCache()Lcom/zendesk/sdk/storage/HelpCenterSessionCache;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StorageModule;->helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    return-object v0
.end method

.method public getIdentityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StorageModule;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    return-object v0
.end method

.method public getRequestStorage()Lcom/zendesk/sdk/storage/RequestStorage;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StorageModule;->requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

    return-object v0
.end method

.method public getSdkSettingsStorage()Lcom/zendesk/sdk/storage/SdkSettingsStorage;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StorageModule;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    return-object v0
.end method

.method public getSdkStorage()Lcom/zendesk/sdk/storage/SdkStorage;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StorageModule;->sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;

    return-object v0
.end method
