.class public final enum Lcom/zendesk/sdk/model/access/AuthenticationType;
.super Ljava/lang/Enum;
.source "AuthenticationType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/model/access/AuthenticationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/model/access/AuthenticationType;

.field public static final enum ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

.field public static final enum JWT:Lcom/zendesk/sdk/model/access/AuthenticationType;


# instance fields
.field private authenticationType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/zendesk/sdk/model/access/AuthenticationType;

    const-string v1, "JWT"

    const-string v2, "jwt"

    invoke-direct {v0, v1, v3, v2}, Lcom/zendesk/sdk/model/access/AuthenticationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->JWT:Lcom/zendesk/sdk/model/access/AuthenticationType;

    .line 15
    new-instance v0, Lcom/zendesk/sdk/model/access/AuthenticationType;

    const-string v1, "ANONYMOUS"

    const-string v2, "anonymous"

    invoke-direct {v0, v1, v4, v2}, Lcom/zendesk/sdk/model/access/AuthenticationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/zendesk/sdk/model/access/AuthenticationType;

    sget-object v1, Lcom/zendesk/sdk/model/access/AuthenticationType;->JWT:Lcom/zendesk/sdk/model/access/AuthenticationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->$VALUES:[Lcom/zendesk/sdk/model/access/AuthenticationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "authenticationType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput-object p3, p0, Lcom/zendesk/sdk/model/access/AuthenticationType;->authenticationType:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public static getAuthType(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AuthenticationType;
    .locals 1
    .param p0, "authenticationType"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->JWT:Lcom/zendesk/sdk/model/access/AuthenticationType;

    iget-object v0, v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->authenticationType:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    sget-object v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->JWT:Lcom/zendesk/sdk/model/access/AuthenticationType;

    .line 37
    :goto_0
    return-object v0

    .line 33
    :cond_0
    sget-object v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    iget-object v0, v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->authenticationType:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    sget-object v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    goto :goto_0

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AuthenticationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/zendesk/sdk/model/access/AuthenticationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/access/AuthenticationType;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/model/access/AuthenticationType;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->$VALUES:[Lcom/zendesk/sdk/model/access/AuthenticationType;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/model/access/AuthenticationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/model/access/AuthenticationType;

    return-object v0
.end method


# virtual methods
.method public getAuthenticationType()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/zendesk/sdk/model/access/AuthenticationType;->authenticationType:Ljava/lang/String;

    return-object v0
.end method
