.class public Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;
.super Ljava/lang/Object;
.source "UpdateRequestWrapper.java"


# instance fields
.field private request:Lcom/zendesk/sdk/model/request/Request;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRequest()Lcom/zendesk/sdk/model/request/Request;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;->request:Lcom/zendesk/sdk/model/request/Request;

    return-object v0
.end method

.method public setRequest(Lcom/zendesk/sdk/model/request/Request;)V
    .locals 0
    .param p1, "request"    # Lcom/zendesk/sdk/model/request/Request;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;->request:Lcom/zendesk/sdk/model/request/Request;

    .line 19
    return-void
.end method
