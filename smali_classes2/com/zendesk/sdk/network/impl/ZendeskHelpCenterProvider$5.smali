.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->listArticles(Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 11
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 169
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheckHelpCenterSettings(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    .line 171
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getInclude()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "categories"

    aput-object v1, v0, v8

    const-string v1, "sections"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    const-string v2, "users"

    aput-object v2, v0, v1

    .line 172
    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 175
    .local v4, "include":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getLocale()Ljava/util/Locale;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->getBestLocale(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Ljava/util/Locale;

    move-result-object v3

    .line 177
    .local v3, "queryLocale":Ljava/util/Locale;
    :goto_1
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getSortBy()Lcom/zendesk/sdk/model/helpcenter/SortBy;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;->CREATED_AT:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    :goto_2
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/SortBy;->getApiValue()Ljava/lang/String;

    move-result-object v5

    .line 178
    .local v5, "sortBy":Ljava/lang/String;
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getSortOrder()Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->DESCENDING:Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    :goto_3
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->getApiValue()Ljava/lang/String;

    move-result-object v6

    .line 180
    .local v6, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    move-result-object v0

    .line 181
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    iget-object v7, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    .line 182
    invoke-virtual {v7}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getLabelNames()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v8

    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    .line 184
    invoke-virtual {v7}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getPage()Ljava/lang/Integer;

    move-result-object v7

    iget-object v8, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    invoke-virtual {v8}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getResultsPerPage()Ljava/lang/Integer;

    move-result-object v8

    new-instance v9, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5$1;

    iget-object v10, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v9, p0, v10}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;Lcom/zendesk/service/ZendeskCallback;)V

    .line 180
    invoke-virtual/range {v0 .. v9}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->listArticles(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/zendesk/service/ZendeskCallback;)V

    .line 196
    .end local v3    # "queryLocale":Ljava/util/Locale;
    .end local v4    # "include":Ljava/lang/String;
    .end local v5    # "sortBy":Ljava/lang/String;
    .end local v6    # "sortOrder":Ljava/lang/String;
    :cond_0
    return-void

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    .line 173
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getInclude()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 175
    .restart local v4    # "include":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getLocale()Ljava/util/Locale;

    move-result-object v3

    goto :goto_1

    .line 177
    .restart local v3    # "queryLocale":Ljava/util/Locale;
    :cond_3
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getSortBy()Lcom/zendesk/sdk/model/helpcenter/SortBy;

    move-result-object v0

    goto :goto_2

    .line 178
    .restart local v5    # "sortBy":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->val$query:Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->getSortOrder()Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    move-result-object v0

    goto :goto_3
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 166
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
