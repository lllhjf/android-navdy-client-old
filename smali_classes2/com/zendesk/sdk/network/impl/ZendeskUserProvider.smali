.class Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;
.super Ljava/lang/Object;
.source "ZendeskUserProvider.java"

# interfaces
.implements Lcom/zendesk/sdk/network/UserProvider;


# instance fields
.field private final baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

.field private final userService:Lcom/zendesk/sdk/network/impl/ZendeskUserService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskUserService;)V
    .locals 0
    .param p1, "baseProvider"    # Lcom/zendesk/sdk/network/BaseProvider;
    .param p2, "userService"    # Lcom/zendesk/sdk/network/impl/ZendeskUserService;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    .line 31
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->userService:Lcom/zendesk/sdk/network/impl/ZendeskUserService;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/sdk/model/request/UserResponse;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;
    .param p1, "x1"    # Lcom/zendesk/sdk/model/request/UserResponse;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->getTags(Lcom/zendesk/sdk/model/request/UserResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;)Lcom/zendesk/sdk/network/impl/ZendeskUserService;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->userService:Lcom/zendesk/sdk/network/impl/ZendeskUserService;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/sdk/model/request/UserResponse;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;
    .param p1, "x1"    # Lcom/zendesk/sdk/model/request/UserResponse;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->getUserFields(Lcom/zendesk/sdk/model/request/UserResponse;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private getTags(Lcom/zendesk/sdk/model/request/UserResponse;)Ljava/util/List;
    .locals 1
    .param p1, "userResponse"    # Lcom/zendesk/sdk/model/request/UserResponse;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/request/UserResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UserResponse;->getUser()Lcom/zendesk/sdk/model/request/User;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UserResponse;->getUser()Lcom/zendesk/sdk/model/request/User;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/request/User;->getTags()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method private getUserFields(Lcom/zendesk/sdk/model/request/UserResponse;)Ljava/util/Map;
    .locals 1
    .param p1, "userResponse"    # Lcom/zendesk/sdk/model/request/UserResponse;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/request/UserResponse;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UserResponse;->getUser()Lcom/zendesk/sdk/model/request/User;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UserResponse;->getUser()Lcom/zendesk/sdk/model/request/User;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/request/User;->getUserFields()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public addTags(Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$1;

    invoke-direct {v1, p0, p2, p1, p2}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 52
    return-void
.end method

.method public deleteTags(Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;

    invoke-direct {v1, p0, p2, p1, p2}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 72
    return-void
.end method

.method public getUser(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/User;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;

    invoke-direct {v1, p0, p1, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 127
    return-void
.end method

.method public getUserFields(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/UserField;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/request/UserField;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$3;

    invoke-direct {v1, p0, p1, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$3;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 90
    return-void
.end method

.method public setUserFields(Ljava/util/Map;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "userFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;

    invoke-direct {v1, p0, p2, p1, p2}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/util/Map;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 109
    return-void
.end method
