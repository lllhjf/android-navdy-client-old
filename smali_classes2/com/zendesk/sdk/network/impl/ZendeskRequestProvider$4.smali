.class Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskRequestProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->getComments(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$requestId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->val$requestId:Ljava/lang/String;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 4
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->areConversationsEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->val$requestId:Ljava/lang/String;

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-static {v0, v1, v2, v3}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->access$100(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 218
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->answerCallbackOnConversationsDisabled(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 208
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
