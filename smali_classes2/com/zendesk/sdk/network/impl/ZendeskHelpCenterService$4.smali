.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$4;
.super Ljava/lang/Object;
.source "ZendeskHelpCenterService.java"

# interfaces
.implements Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->getArticle(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/ArticleResponse;",
        "Lcom/zendesk/sdk/model/helpcenter/Article;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/zendesk/sdk/model/helpcenter/ArticleResponse;)Lcom/zendesk/sdk/model/helpcenter/Article;
    .locals 3
    .param p1, "articleResponse"    # Lcom/zendesk/sdk/model/helpcenter/ArticleResponse;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticleResponse;->getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticleResponse;->getUsers()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->matchArticleWithUsers(Lcom/zendesk/sdk/model/helpcenter/Article;Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic extract(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 198
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/ArticleResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$4;->extract(Lcom/zendesk/sdk/model/helpcenter/ArticleResponse;)Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v0

    return-object v0
.end method
