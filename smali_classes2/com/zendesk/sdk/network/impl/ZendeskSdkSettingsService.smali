.class Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;
.super Ljava/lang/Object;
.source "ZendeskSdkSettingsService.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskSdkSettingsService"


# instance fields
.field private final sdkSettingsService:Lcom/zendesk/sdk/network/SdkSettingsService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/SdkSettingsService;)V
    .locals 0
    .param p1, "sdkSettingsService"    # Lcom/zendesk/sdk/network/SdkSettingsService;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;->sdkSettingsService:Lcom/zendesk/sdk/network/SdkSettingsService;

    .line 20
    return-void
.end method


# virtual methods
.method public getSettings(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "deviceLocale"    # Ljava/lang/String;
    .param p2, "applicationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/settings/MobileSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/settings/MobileSettings;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;->sdkSettingsService:Lcom/zendesk/sdk/network/SdkSettingsService;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/SdkSettingsService;->getSettings(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 38
    return-void
.end method
