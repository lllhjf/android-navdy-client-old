.class Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;
.super Ljava/lang/Object;
.source "ContactZendeskFragment.java"

# interfaces
.implements Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactZendeskFeedbackConfiguration"
.end annotation


# instance fields
.field private final mConfiguration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V
    .locals 0
    .param p2, "configuration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .prologue
    .line 700
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 701
    iput-object p2, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;->mConfiguration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .line 702
    return-void
.end method


# virtual methods
.method public getAdditionalInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;->mConfiguration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    invoke-interface {v0}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;->mConfiguration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    invoke-interface {v0}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getRequestSubject()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTags()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 706
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;->mConfiguration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    invoke-interface {v0}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getTags()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
