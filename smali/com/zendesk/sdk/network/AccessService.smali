.class public interface abstract Lcom/zendesk/sdk/network/AccessService;
.super Ljava/lang/Object;
.source "AccessService.java"


# virtual methods
.method public abstract getAuthToken(Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;)Lretrofit2/Call;
    .param p1    # Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/zendesk/sdk/model/access/AuthenticationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/access/sdk/jwt"
    .end annotation
.end method

.method public abstract getAuthTokenForAnonymous(Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;)Lretrofit2/Call;
    .param p1    # Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/zendesk/sdk/model/access/AuthenticationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/access/sdk/anonymous"
    .end annotation
.end method
