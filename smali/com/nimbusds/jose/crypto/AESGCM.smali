.class Lcom/nimbusds/jose/crypto/AESGCM;
.super Ljava/lang/Object;
.source "AESGCM.java"


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field public static final AUTH_TAG_BIT_LENGTH:I = 0x80

.field public static final IV_BIT_LENGTH:I = 0x60


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decrypt(Ljavax/crypto/SecretKey;[B[B[B[BLjava/security/Provider;)[B
    .locals 6
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "cipherText"    # [B
    .param p3, "authData"    # [B
    .param p4, "authTag"    # [B
    .param p5, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 147
    if-eqz p5, :cond_0

    .line 148
    :try_start_0
    const-string v3, "AES/GCM/NoPadding"

    invoke-static {v3, p5}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 153
    .local v0, "cipher":Ljavax/crypto/Cipher;
    :goto_0
    new-instance v2, Ljavax/crypto/spec/GCMParameterSpec;

    const/16 v3, 0x80

    invoke-direct {v2, v3, p1}, Ljavax/crypto/spec/GCMParameterSpec;-><init>(I[B)V

    .line 154
    .local v2, "gcmSpec":Ljavax/crypto/spec/GCMParameterSpec;
    const/4 v3, 0x2

    invoke-virtual {v0, v3, p0, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 166
    invoke-virtual {v0, p3}, Ljavax/crypto/Cipher;->updateAAD([B)V

    .line 169
    const/4 v3, 0x2

    :try_start_1
    new-array v3, v3, [[B

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object p4, v3, v4

    invoke-static {v3}, Lcom/nimbusds/jose/util/ByteUtils;->concat([[B)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_1
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v2    # "gcmSpec":Ljavax/crypto/spec/GCMParameterSpec;
    :goto_1
    return-object v3

    .line 150
    :cond_0
    :try_start_2
    const-string v3, "AES/GCM/NoPadding"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    .restart local v0    # "cipher":Ljavax/crypto/Cipher;
    goto :goto_0

    .line 156
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/security/GeneralSecurityException;
    :goto_2
    new-instance v3, Lcom/nimbusds/jose/JOSEException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Couldn\'t create AES/GCM/NoPadding cipher: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 163
    .end local v1    # "e":Ljava/security/GeneralSecurityException;
    :catch_1
    move-exception v3

    invoke-static {p0, p1, p2, p3, p4}, Lcom/nimbusds/jose/crypto/LegacyAESGCM;->decrypt(Ljavax/crypto/SecretKey;[B[B[B[B)[B

    move-result-object v3

    goto :goto_1

    .line 171
    .restart local v0    # "cipher":Ljavax/crypto/Cipher;
    .restart local v2    # "gcmSpec":Ljavax/crypto/spec/GCMParameterSpec;
    :catch_2
    move-exception v1

    .line 173
    .restart local v1    # "e":Ljava/security/GeneralSecurityException;
    :goto_3
    new-instance v3, Lcom/nimbusds/jose/JOSEException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AES/GCM/NoPadding decryption failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 171
    .end local v1    # "e":Ljava/security/GeneralSecurityException;
    :catch_3
    move-exception v1

    goto :goto_3

    .line 156
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v2    # "gcmSpec":Ljavax/crypto/spec/GCMParameterSpec;
    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_2
.end method

.method public static encrypt(Ljavax/crypto/SecretKey;[B[B[BLjava/security/Provider;)Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;
    .locals 10
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "plainText"    # [B
    .param p3, "authData"    # [B
    .param p4, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x80

    .line 82
    if-eqz p4, :cond_0

    .line 83
    :try_start_0
    const-string v7, "AES/GCM/NoPadding"

    invoke-static {v7, p4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 88
    .local v1, "cipher":Ljavax/crypto/Cipher;
    :goto_0
    new-instance v5, Ljavax/crypto/spec/GCMParameterSpec;

    const/16 v7, 0x80

    invoke-direct {v5, v7, p1}, Ljavax/crypto/spec/GCMParameterSpec;-><init>(I[B)V

    .line 89
    .local v5, "gcmSpec":Ljavax/crypto/spec/GCMParameterSpec;
    const/4 v7, 0x1

    invoke-virtual {v1, v7, p0, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 101
    invoke-virtual {v1, p3}, Ljavax/crypto/Cipher;->updateAAD([B)V

    .line 106
    :try_start_1
    invoke-virtual {v1, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_1
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v2

    .line 113
    .local v2, "cipherOutput":[B
    array-length v7, v2

    invoke-static {v9}, Lcom/nimbusds/jose/util/ByteUtils;->byteLength(I)I

    move-result v8

    sub-int v6, v7, v8

    .line 115
    .local v6, "tagPos":I
    const/4 v7, 0x0

    invoke-static {v2, v7, v6}, Lcom/nimbusds/jose/util/ByteUtils;->subArray([BII)[B

    move-result-object v3

    .line 116
    .local v3, "cipherText":[B
    invoke-static {v9}, Lcom/nimbusds/jose/util/ByteUtils;->byteLength(I)I

    move-result v7

    invoke-static {v2, v6, v7}, Lcom/nimbusds/jose/util/ByteUtils;->subArray([BII)[B

    move-result-object v0

    .line 118
    .local v0, "authTag":[B
    new-instance v7, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;

    invoke-direct {v7, v3, v0}, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;-><init>([B[B)V

    .end local v0    # "authTag":[B
    .end local v1    # "cipher":Ljavax/crypto/Cipher;
    .end local v2    # "cipherOutput":[B
    .end local v3    # "cipherText":[B
    .end local v5    # "gcmSpec":Ljavax/crypto/spec/GCMParameterSpec;
    .end local v6    # "tagPos":I
    :goto_1
    return-object v7

    .line 85
    :cond_0
    :try_start_2
    const-string v7, "AES/GCM/NoPadding"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    .restart local v1    # "cipher":Ljavax/crypto/Cipher;
    goto :goto_0

    .line 91
    .end local v1    # "cipher":Ljavax/crypto/Cipher;
    :catch_0
    move-exception v4

    .line 93
    .local v4, "e":Ljava/security/GeneralSecurityException;
    :goto_2
    new-instance v7, Lcom/nimbusds/jose/JOSEException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Couldn\'t create AES/GCM/NoPadding cipher: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v4}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 98
    .end local v4    # "e":Ljava/security/GeneralSecurityException;
    :catch_1
    move-exception v7

    invoke-static {p0, p1, p2, p3}, Lcom/nimbusds/jose/crypto/LegacyAESGCM;->encrypt(Ljavax/crypto/SecretKey;[B[B[B)Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;

    move-result-object v7

    goto :goto_1

    .line 108
    .restart local v1    # "cipher":Ljavax/crypto/Cipher;
    .restart local v5    # "gcmSpec":Ljavax/crypto/spec/GCMParameterSpec;
    :catch_2
    move-exception v4

    .line 110
    .restart local v4    # "e":Ljava/security/GeneralSecurityException;
    :goto_3
    new-instance v7, Lcom/nimbusds/jose/JOSEException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Couldn\'t encrypt with AES/GCM/NoPadding: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v4}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 108
    .end local v4    # "e":Ljava/security/GeneralSecurityException;
    :catch_3
    move-exception v4

    goto :goto_3

    .line 91
    .end local v1    # "cipher":Ljavax/crypto/Cipher;
    .end local v5    # "gcmSpec":Ljavax/crypto/spec/GCMParameterSpec;
    :catch_4
    move-exception v4

    goto :goto_2

    :catch_5
    move-exception v4

    goto :goto_2

    :catch_6
    move-exception v4

    goto :goto_2
.end method

.method public static generateIV(Ljava/security/SecureRandom;)[B
    .locals 2
    .param p0, "randomGen"    # Ljava/security/SecureRandom;

    .prologue
    .line 53
    const/16 v1, 0xc

    new-array v0, v1, [B

    .line 54
    .local v0, "bytes":[B
    invoke-virtual {p0, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 55
    return-object v0
.end method
