.class Lcom/nimbusds/jose/crypto/LegacyAESGCM;
.super Ljava/lang/Object;
.source "LegacyAESGCM.java"


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field public static final AUTH_TAG_BIT_LENGTH:I = 0x80


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createAESCipher(Ljavax/crypto/SecretKey;Z)Lorg/bouncycastle/crypto/engines/AESEngine;
    .locals 3
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "forEncryption"    # Z

    .prologue
    .line 50
    new-instance v0, Lorg/bouncycastle/crypto/engines/AESEngine;

    invoke-direct {v0}, Lorg/bouncycastle/crypto/engines/AESEngine;-><init>()V

    .line 52
    .local v0, "cipher":Lorg/bouncycastle/crypto/engines/AESEngine;
    new-instance v1, Lorg/bouncycastle/crypto/params/KeyParameter;

    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/bouncycastle/crypto/params/KeyParameter;-><init>([B)V

    .line 54
    .local v1, "cipherParams":Lorg/bouncycastle/crypto/CipherParameters;
    invoke-virtual {v0, p1, v1}, Lorg/bouncycastle/crypto/engines/AESEngine;->init(ZLorg/bouncycastle/crypto/CipherParameters;)V

    .line 56
    return-object v0
.end method

.method private static createAESGCMCipher(Ljavax/crypto/SecretKey;Z[B[B)Lorg/bouncycastle/crypto/modes/GCMBlockCipher;
    .locals 5
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "forEncryption"    # Z
    .param p2, "iv"    # [B
    .param p3, "authData"    # [B

    .prologue
    .line 79
    invoke-static {p0, p1}, Lcom/nimbusds/jose/crypto/LegacyAESGCM;->createAESCipher(Ljavax/crypto/SecretKey;Z)Lorg/bouncycastle/crypto/engines/AESEngine;

    move-result-object v1

    .line 82
    .local v1, "cipher":Lorg/bouncycastle/crypto/BlockCipher;
    new-instance v2, Lorg/bouncycastle/crypto/modes/GCMBlockCipher;

    invoke-direct {v2, v1}, Lorg/bouncycastle/crypto/modes/GCMBlockCipher;-><init>(Lorg/bouncycastle/crypto/BlockCipher;)V

    .line 84
    .local v2, "gcm":Lorg/bouncycastle/crypto/modes/GCMBlockCipher;
    new-instance v0, Lorg/bouncycastle/crypto/params/AEADParameters;

    new-instance v3, Lorg/bouncycastle/crypto/params/KeyParameter;

    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/bouncycastle/crypto/params/KeyParameter;-><init>([B)V

    .line 85
    const/16 v4, 0x80

    .line 84
    invoke-direct {v0, v3, v4, p2, p3}, Lorg/bouncycastle/crypto/params/AEADParameters;-><init>(Lorg/bouncycastle/crypto/params/KeyParameter;I[B[B)V

    .line 88
    .local v0, "aeadParams":Lorg/bouncycastle/crypto/params/AEADParameters;
    invoke-virtual {v2, p1, v0}, Lorg/bouncycastle/crypto/modes/GCMBlockCipher;->init(ZLorg/bouncycastle/crypto/CipherParameters;)V

    .line 90
    return-object v2
.end method

.method public static decrypt(Ljavax/crypto/SecretKey;[B[B[B[B)[B
    .locals 9
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "cipherText"    # [B
    .param p3, "authData"    # [B
    .param p4, "authTag"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 170
    invoke-static {p0, v2, p1, p3}, Lcom/nimbusds/jose/crypto/LegacyAESGCM;->createAESGCMCipher(Ljavax/crypto/SecretKey;Z[B[B)Lorg/bouncycastle/crypto/modes/GCMBlockCipher;

    move-result-object v0

    .line 174
    .local v0, "cipher":Lorg/bouncycastle/crypto/modes/GCMBlockCipher;
    array-length v3, p2

    array-length v5, p4

    add-int/2addr v3, v5

    new-array v1, v3, [B

    .line 176
    .local v1, "input":[B
    array-length v3, p2

    invoke-static {p2, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 177
    array-length v3, p2

    array-length v5, p4

    invoke-static {p4, v2, v1, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 179
    array-length v3, v1

    invoke-virtual {v0, v3}, Lorg/bouncycastle/crypto/modes/GCMBlockCipher;->getOutputSize(I)I

    move-result v7

    .line 181
    .local v7, "outputLength":I
    new-array v4, v7, [B

    .line 185
    .local v4, "output":[B
    array-length v3, v1

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lorg/bouncycastle/crypto/modes/GCMBlockCipher;->processBytes([BII[BI)I

    move-result v8

    .line 189
    .local v8, "outputOffset":I
    :try_start_0
    invoke-virtual {v0, v4, v8}, Lorg/bouncycastle/crypto/modes/GCMBlockCipher;->doFinal([BI)I
    :try_end_0
    .catch Lorg/bouncycastle/crypto/InvalidCipherTextException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    add-int/2addr v8, v2

    .line 196
    return-object v4

    .line 191
    :catch_0
    move-exception v6

    .line 193
    .local v6, "e":Lorg/bouncycastle/crypto/InvalidCipherTextException;
    new-instance v2, Lcom/nimbusds/jose/JOSEException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Couldn\'t validate GCM authentication tag: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lorg/bouncycastle/crypto/InvalidCipherTextException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v6}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static encrypt(Ljavax/crypto/SecretKey;[B[B[B)Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;
    .locals 12
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "plainText"    # [B
    .param p3, "authData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 114
    const/4 v1, 0x1

    invoke-static {p0, v1, p1, p3}, Lcom/nimbusds/jose/crypto/LegacyAESGCM;->createAESGCMCipher(Ljavax/crypto/SecretKey;Z[B[B)Lorg/bouncycastle/crypto/modes/GCMBlockCipher;

    move-result-object v0

    .line 118
    .local v0, "cipher":Lorg/bouncycastle/crypto/modes/GCMBlockCipher;
    array-length v1, p2

    invoke-virtual {v0, v1}, Lorg/bouncycastle/crypto/modes/GCMBlockCipher;->getOutputSize(I)I

    move-result v10

    .line 119
    .local v10, "outputLength":I
    new-array v4, v10, [B

    .line 123
    .local v4, "output":[B
    array-length v3, p2

    move-object v1, p2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lorg/bouncycastle/crypto/modes/GCMBlockCipher;->processBytes([BII[BI)I

    move-result v11

    .line 128
    .local v11, "outputOffset":I
    :try_start_0
    invoke-virtual {v0, v4, v11}, Lorg/bouncycastle/crypto/modes/GCMBlockCipher;->doFinal([BI)I
    :try_end_0
    .catch Lorg/bouncycastle/crypto/InvalidCipherTextException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    add-int/2addr v11, v1

    .line 136
    const/16 v7, 0x10

    .line 138
    .local v7, "authTagLength":I
    sub-int v1, v11, v7

    new-array v8, v1, [B

    .line 139
    .local v8, "cipherText":[B
    new-array v6, v7, [B

    .line 141
    .local v6, "authTag":[B
    array-length v1, v8

    invoke-static {v4, v2, v8, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    sub-int v1, v11, v7

    array-length v3, v6

    invoke-static {v4, v1, v6, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 144
    new-instance v1, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;

    invoke-direct {v1, v8, v6}, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;-><init>([B[B)V

    return-object v1

    .line 130
    .end local v6    # "authTag":[B
    .end local v7    # "authTagLength":I
    .end local v8    # "cipherText":[B
    :catch_0
    move-exception v9

    .line 132
    .local v9, "e":Lorg/bouncycastle/crypto/InvalidCipherTextException;
    new-instance v1, Lcom/nimbusds/jose/JOSEException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t generate GCM authentication tag: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lorg/bouncycastle/crypto/InvalidCipherTextException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v9}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
