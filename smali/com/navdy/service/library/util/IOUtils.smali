.class public final Lcom/navdy/service/library/util/IOUtils;
.super Ljava/lang/Object;
.source "IOUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/util/IOUtils$OnFileTraversal;
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0x4000

.field private static final BUFFER_SIZE_FOR_DOWNLOADS:I = 0x400

.field private static final CHECKSUM_FILE_NAME:Ljava/lang/String; = ".checksum"

.field private static final CHECKSUM_UPDATE_MODE:Z = false

.field private static final DIGEST_BUFFER_SIZE:I = 0x100000

.field private static final END_OF_STREAM:I = -0x1

.field private static final INPUT_BUFFER_SIZE:I = 0x4000

.field private static final TAG:Ljava/lang/String;

.field private static final TRASH_DIR_NAME:Ljava/lang/String; = ".trash"

.field public static final UTF_8:Ljava/lang/String; = "UTF-8"

.field private static final sCounter:Ljava/util/concurrent/atomic/AtomicLong;

.field private static sExternalTrashDir:Ljava/io/File;

.field private static final sLock:Ljava/lang/Object;

.field private static sLogger:Lcom/navdy/service/library/log/Logger;

.field private static volatile sTrashDir:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 50
    const-class v0, Lcom/navdy/service/library/util/IOUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/util/IOUtils;->TAG:Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/util/IOUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/navdy/service/library/util/IOUtils;->sCounter:Ljava/util/concurrent/atomic/AtomicLong;

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/service/library/util/IOUtils;->sLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static bitmap2ByteBuffer(Landroid/graphics/Bitmap;)[B
    .locals 4
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 594
    if-nez p0, :cond_0

    .line 595
    const/4 v0, 0x0

    .line 600
    :goto_0
    return-object v0

    .line 597
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 598
    .local v1, "stream":Ljava/io/ByteArrayOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 599
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 600
    .local v0, "byteArray":[B
    goto :goto_0
.end method

.method public static bytesToHexString([B)Ljava/lang/String;
    .locals 2
    .param p0, "bytes"    # [B

    .prologue
    .line 617
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lcom/navdy/service/library/util/IOUtils;->bytesToHexString([BII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static bytesToHexString([BII)Ljava/lang/String;
    .locals 5
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 605
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 606
    .local v2, "sb":Ljava/lang/StringBuilder;
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 607
    aget-byte v3, p0, v1

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 608
    .local v0, "hex":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 609
    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 611
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 613
    .end local v0    # "hex":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static checkIntegrity(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "md5ReferenceRes"    # I

    .prologue
    .line 539
    sget-object v17, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "integrity check for "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " starting"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 540
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 542
    .local v12, "l1":J
    const/4 v6, 0x0

    .line 543
    .local v6, "buildChecksum":Ljava/lang/String;
    const/4 v9, 0x0

    .line 545
    .local v9, "cachedChecksum":Ljava/lang/String;
    const/4 v7, 0x0

    .line 548
    .local v7, "buildChecksumInputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v7

    .line 549
    const-string v17, "UTF-8"

    move-object/from16 v0, v17

    invoke-static {v7, v0}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 550
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    sget-object v18, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ".checksum"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 554
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 557
    :goto_0
    if-eqz v6, :cond_0

    if-eqz v9, :cond_0

    .line 558
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    const/4 v8, 0x1

    .line 560
    .local v8, "cachedAndBuildChecksumsAreSame":Z
    :goto_1
    if-eqz v8, :cond_1

    .line 561
    sget-object v17, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "checksum for "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " is fine, no-op"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 585
    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 586
    .local v14, "l2":J
    sget-object v17, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "integrity check for "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " took "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sub-long v20, v14, v12

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " ms"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 587
    return-void

    .line 551
    .end local v8    # "cachedAndBuildChecksumsAreSame":Z
    .end local v14    # "l2":J
    :catch_0
    move-exception v10

    .line 552
    .local v10, "e":Ljava/lang/Throwable;
    :try_start_1
    sget-object v17, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v18, "error while retrieving integrity checksum from filesystem, might not be present"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v10    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v17

    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v17

    .line 558
    :cond_0
    const/4 v8, 0x0

    goto :goto_1

    .line 563
    .restart local v8    # "cachedAndBuildChecksumsAreSame":Z
    :cond_1
    const/16 v17, 0x1

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, ".checksum"

    aput-object v20, v18, v19

    move-object/from16 v0, p1

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/navdy/service/library/util/IOUtils;->hashForPath(Ljava/lang/String;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 564
    .local v5, "actualChecksum":Ljava/lang/String;
    if-eqz v5, :cond_2

    if-eqz v6, :cond_2

    .line 565
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/4 v4, 0x1

    .line 567
    .local v4, "actualAndBuildChecksumsAreSame":Z
    :goto_3
    if-nez v4, :cond_3

    .line 580
    sget-object v17, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "files on "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " are out of date or corrupted, redoing"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 581
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 582
    invoke-static/range {p1 .. p1}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/lang/String;)Z

    goto/16 :goto_2

    .line 565
    .end local v4    # "actualAndBuildChecksumsAreSame":Z
    :cond_2
    const/4 v4, 0x0

    goto :goto_3

    .line 568
    .restart local v4    # "actualAndBuildChecksumsAreSame":Z
    :cond_3
    sget-object v17, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "checksum for "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " is fine, writing integrity checksum on filesystem"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 569
    const/4 v11, 0x0

    .line 572
    .local v11, "printWriter":Ljava/io/PrintWriter;
    :try_start_2
    new-instance v16, Ljava/io/PrintWriter;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    sget-object v18, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ".checksum"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/io/PrintWriter;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 573
    .end local v11    # "printWriter":Ljava/io/PrintWriter;
    .local v16, "printWriter":Ljava/io/PrintWriter;
    :try_start_3
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 577
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object/from16 v11, v16

    .line 578
    .end local v16    # "printWriter":Ljava/io/PrintWriter;
    .restart local v11    # "printWriter":Ljava/io/PrintWriter;
    goto/16 :goto_2

    .line 574
    :catch_1
    move-exception v10

    .line 575
    .restart local v10    # "e":Ljava/lang/Throwable;
    :goto_4
    :try_start_4
    sget-object v17, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v18, "could not write integrity checksum on filesystem"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 577
    invoke-static {v11}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_2

    .end local v10    # "e":Ljava/lang/Throwable;
    :catchall_1
    move-exception v17

    :goto_5
    invoke-static {v11}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v17

    .end local v11    # "printWriter":Ljava/io/PrintWriter;
    .restart local v16    # "printWriter":Ljava/io/PrintWriter;
    :catchall_2
    move-exception v17

    move-object/from16 v11, v16

    .end local v16    # "printWriter":Ljava/io/PrintWriter;
    .restart local v11    # "printWriter":Ljava/io/PrintWriter;
    goto :goto_5

    .line 574
    .end local v11    # "printWriter":Ljava/io/PrintWriter;
    .restart local v16    # "printWriter":Ljava/io/PrintWriter;
    :catch_2
    move-exception v10

    move-object/from16 v11, v16

    .end local v16    # "printWriter":Ljava/io/PrintWriter;
    .restart local v11    # "printWriter":Ljava/io/PrintWriter;
    goto :goto_4
.end method

.method private static cleanDirectory(Landroid/content/Context;Ljava/io/File;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "directory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 213
    :cond_0
    return-void

    .line 195
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 198
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 199
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 202
    const/4 v0, 0x0

    .line 203
    .local v0, "exception":Ljava/io/IOException;
    array-length v5, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v1, v2, v4

    .line 205
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-static {p0, v1}, Lcom/navdy/service/library/util/IOUtils;->forceDelete(Landroid/content/Context;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 206
    :catch_0
    move-exception v3

    .line 207
    .local v3, "ioe":Ljava/io/IOException;
    move-object v0, v3

    goto :goto_1

    .line 210
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "ioe":Ljava/io/IOException;
    :cond_2
    if-eqz v0, :cond_0

    .line 211
    throw v0
.end method

.method public static closeFD(I)V
    .locals 3
    .param p0, "fd"    # I

    .prologue
    .line 310
    const/4 v2, -0x1

    if-eq p0, v2, :cond_0

    .line 311
    :try_start_0
    invoke-static {p0}, Landroid/os/ParcelFileDescriptor;->adoptFd(I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 312
    .local v0, "pFD":Landroid/os/ParcelFileDescriptor;
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    .end local v0    # "pFD":Landroid/os/ParcelFileDescriptor;
    :cond_0
    :goto_0
    return-void

    .line 314
    :catch_0
    move-exception v1

    .line 315
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static closeObject(Ljava/io/Closeable;)V
    .locals 2
    .param p0, "obj"    # Ljava/io/Closeable;

    .prologue
    .line 81
    if-eqz p0, :cond_0

    .line 82
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static closeStream(Ljava/io/Closeable;)V
    .locals 0
    .param p0, "stream"    # Ljava/io/Closeable;

    .prologue
    .line 90
    invoke-static {p0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 91
    return-void
.end method

.method public static compressFilesToZip(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "zipEntryFiles"    # [Ljava/io/File;
    .param p2, "zipFilePath"    # Ljava/lang/String;

    .prologue
    .line 688
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 689
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 690
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 692
    :cond_0
    const/4 v3, 0x0

    .line 694
    .local v3, "created":Z
    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 695
    if-nez v3, :cond_2

    .line 732
    :cond_1
    :goto_0
    return-void

    .line 698
    :catch_0
    move-exception v4

    .line 699
    .local v4, "e":Ljava/io/IOException;
    sget-object v15, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "IO Exception while creating new file"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 702
    .end local v4    # "e":Ljava/io/IOException;
    :cond_2
    if-eqz p1, :cond_1

    .line 703
    const/4 v9, 0x0

    .line 704
    .local v9, "fos":Ljava/io/FileOutputStream;
    const/4 v7, 0x0

    .line 705
    .local v7, "fis":Ljava/io/FileInputStream;
    const/4 v13, 0x0

    .line 707
    .local v13, "zos":Ljava/util/zip/ZipOutputStream;
    const/16 v15, 0x4000

    :try_start_1
    new-array v2, v15, [B

    .line 708
    .local v2, "buffer":[B
    new-instance v10, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 709
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .local v10, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v14, Ljava/util/zip/ZipOutputStream;

    invoke-direct {v14, v10}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 710
    .end local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    .local v14, "zos":Ljava/util/zip/ZipOutputStream;
    :try_start_3
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v16, v0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    const/4 v15, 0x0

    move-object v8, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v8, "fis":Ljava/lang/Object;
    :goto_1
    move/from16 v0, v16

    if-ge v15, v0, :cond_5

    :try_start_4
    aget-object v5, p1, v15

    .line 711
    .local v5, "entryFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-virtual {v5}, Ljava/io/File;->canRead()Z

    move-result v17

    if-nez v17, :cond_3

    move-object v7, v8

    .line 710
    .end local v8    # "fis":Ljava/lang/Object;
    :goto_2
    add-int/lit8 v15, v15, 0x1

    move-object v8, v7

    .restart local v8    # "fis":Ljava/lang/Object;
    goto :goto_1

    .line 714
    :cond_3
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 715
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :try_start_5
    new-instance v17, Ljava/util/zip/ZipEntry;

    .end local v8    # "fis":Ljava/lang/Object;
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 717
    :goto_3
    invoke-virtual {v7, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v11

    .local v11, "length":I
    if-lez v11, :cond_4

    .line 718
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v2, v0, v11}, Ljava/util/zip/ZipOutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_3

    .line 724
    .end local v5    # "entryFile":Ljava/io/File;
    .end local v11    # "length":I
    :catch_1
    move-exception v12

    move-object v13, v14

    .end local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    move-object v9, v10

    .line 725
    .end local v2    # "buffer":[B
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .local v12, "t":Ljava/lang/Throwable;
    :goto_4
    :try_start_6
    sget-object v15, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "Error while compressing files "

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 727
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 728
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 729
    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 720
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v12    # "t":Ljava/lang/Throwable;
    .end local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v5    # "entryFile":Ljava/io/File;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "length":I
    .restart local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    :cond_4
    :try_start_7
    invoke-virtual {v14}, Ljava/util/zip/ZipOutputStream;->closeEntry()V

    .line 721
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 722
    const/4 v7, 0x0

    goto :goto_2

    .line 727
    .end local v5    # "entryFile":Ljava/io/File;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v11    # "length":I
    .restart local v8    # "fis":Ljava/lang/Object;
    :cond_5
    invoke-static {v14}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 728
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 729
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 727
    .end local v2    # "buffer":[B
    .end local v8    # "fis":Ljava/lang/Object;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .end local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    :catchall_0
    move-exception v15

    :goto_5
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 728
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 729
    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v15

    .line 727
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v15

    move-object v9, v10

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    :catchall_2
    move-exception v15

    move-object v13, v14

    .end local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    move-object v9, v10

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v8    # "fis":Ljava/lang/Object;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    :catchall_3
    move-exception v15

    move-object v13, v14

    .end local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    move-object v7, v8

    .restart local v7    # "fis":Ljava/io/FileInputStream;
    move-object v9, v10

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 724
    .end local v2    # "buffer":[B
    .end local v8    # "fis":Ljava/lang/Object;
    :catch_2
    move-exception v12

    goto :goto_4

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v12

    move-object v9, v10

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v8    # "fis":Ljava/lang/Object;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    :catch_4
    move-exception v12

    move-object v13, v14

    .end local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    move-object v7, v8

    .restart local v7    # "fis":Ljava/io/FileInputStream;
    move-object v9, v10

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v13    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v5    # "entryFile":Ljava/io/File;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "zos":Ljava/util/zip/ZipOutputStream;
    :cond_6
    move-object v7, v8

    .end local v8    # "fis":Ljava/lang/Object;
    .local v7, "fis":Ljava/lang/Object;
    goto :goto_2
.end method

.method public static convertFileToString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 278
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 280
    .local v0, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 282
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
.end method

.method public static convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "charSet"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 268
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 269
    .local v1, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x4000

    new-array v0, v3, [B

    .line 271
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "n":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 272
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 274
    :cond_0
    invoke-virtual {v1, p1}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static copyFile(Ljava/lang/String;Ljava/io/InputStream;)I
    .locals 6
    .param p0, "targetPath"    # Ljava/lang/String;
    .param p1, "srcStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 364
    const/4 v3, 0x0

    .line 365
    .local v3, "targetFile":Ljava/io/FileOutputStream;
    const/4 v0, 0x0

    .line 367
    .local v0, "bytesCopied":I
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    .end local v3    # "targetFile":Ljava/io/FileOutputStream;
    .local v4, "targetFile":Ljava/io/FileOutputStream;
    const/16 v5, 0x4000

    :try_start_1
    new-array v2, v5, [B

    .line 370
    .local v2, "data":[B
    :goto_0
    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "count":I
    if-lez v1, :cond_0

    .line 371
    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 372
    add-int/2addr v0, v1

    goto :goto_0

    .line 376
    :cond_0
    invoke-static {p1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 377
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 378
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    return v0

    .line 376
    .end local v1    # "count":I
    .end local v2    # "data":[B
    .end local v4    # "targetFile":Ljava/io/FileOutputStream;
    .restart local v3    # "targetFile":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v5

    :goto_1
    invoke-static {p1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 377
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 378
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v5

    .line 376
    .end local v3    # "targetFile":Ljava/io/FileOutputStream;
    .restart local v4    # "targetFile":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "targetFile":Ljava/io/FileOutputStream;
    .restart local v3    # "targetFile":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method public static copyFile(Ljava/lang/String;[B)I
    .locals 1
    .param p0, "targetPath"    # Ljava/lang/String;
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 383
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;[BZ)I

    move-result v0

    return v0
.end method

.method public static copyFile(Ljava/lang/String;[BZ)I
    .locals 5
    .param p0, "targetPath"    # Ljava/lang/String;
    .param p1, "data"    # [B
    .param p2, "sync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    const/4 v1, 0x0

    .line 388
    .local v1, "targetFile":Ljava/io/FileOutputStream;
    const/4 v0, 0x0

    .line 390
    .local v0, "bytesCopied":I
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    .end local v1    # "targetFile":Ljava/io/FileOutputStream;
    .local v2, "targetFile":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    :try_start_1
    array-length v4, p1

    invoke-virtual {v2, p1, v3, v4}, Ljava/io/FileOutputStream;->write([BII)V

    .line 392
    array-length v3, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/2addr v0, v3

    .line 395
    if-eqz p2, :cond_0

    .line 396
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 398
    :cond_0
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    return v0

    .line 395
    .end local v2    # "targetFile":Ljava/io/FileOutputStream;
    .restart local v1    # "targetFile":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v3

    :goto_0
    if-eqz p2, :cond_1

    .line 396
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 398
    :cond_1
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3

    .line 395
    .end local v1    # "targetFile":Ljava/io/FileOutputStream;
    .restart local v2    # "targetFile":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "targetFile":Ljava/io/FileOutputStream;
    .restart local v1    # "targetFile":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method public static copyFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "srcPath"    # Ljava/lang/String;
    .param p1, "targetPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    const/4 v1, 0x0

    .line 350
    .local v1, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 351
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 352
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .local v2, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-static {p1, v2}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/io/InputStream;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 354
    const/4 v1, 0x0

    .line 355
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "fileInputStream":Ljava/io/FileInputStream;
    const/4 v3, 0x1

    .line 358
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 360
    :goto_0
    return v3

    .line 358
    :cond_0
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 360
    const/4 v3, 0x0

    goto :goto_0

    .line 358
    .end local v0    # "file":Ljava/io/File;
    :catchall_0
    move-exception v3

    :goto_1
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3

    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v0    # "file":Ljava/io/File;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static createDirectory(Ljava/io/File;)Z
    .locals 4
    .param p0, "dir"    # Ljava/io/File;

    .prologue
    .line 155
    const/4 v0, 0x0

    .line 156
    .local v0, "worked":Z
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 157
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    .line 163
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 164
    sget-object v2, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to create directory: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 168
    :goto_2
    return v0

    .line 158
    :cond_1
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    const/4 v0, 0x1

    .line 161
    sget-object v1, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Directory already exists: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 164
    :cond_2
    const-string v1, "null"

    goto :goto_1

    .line 166
    :cond_3
    sget-object v1, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Successfully created directory: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static createDirectory(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 151
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method public static deleteDirectory(Landroid/content/Context;Ljava/io/File;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "oldDir"    # Ljava/io/File;

    .prologue
    .line 126
    if-nez p1, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 131
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/navdy/service/library/util/IOUtils;->isAppsInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 132
    new-instance v1, Ljava/io/File;

    invoke-static {p0}, Lcom/navdy/service/library/util/IOUtils;->getNewTrashEntryPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 138
    .local v1, "newDir":Ljava/io/File;
    :goto_1
    invoke-virtual {p1, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    .line 139
    .local v2, "worked":Z
    if-nez v2, :cond_2

    .line 140
    sget-object v3, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to rename dir "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 143
    :cond_2
    if-eqz v2, :cond_5

    .end local v1    # "newDir":Ljava/io/File;
    :goto_2
    :try_start_0
    invoke-static {p0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectoryInternal(Landroid/content/Context;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "iex":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 133
    .end local v0    # "iex":Ljava/io/IOException;
    .end local v2    # "worked":Z
    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/navdy/service/library/util/IOUtils;->isAppsExternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 134
    new-instance v1, Ljava/io/File;

    invoke-static {p0}, Lcom/navdy/service/library/util/IOUtils;->getNewExternalTrashEntryPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v1    # "newDir":Ljava/io/File;
    goto :goto_1

    .line 136
    .end local v1    # "newDir":Ljava/io/File;
    :cond_4
    new-instance v1, Ljava/io/File;

    invoke-static {p1}, Lcom/navdy/service/library/util/IOUtils;->getTrashEntryPathInSameFolder(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v1    # "newDir":Ljava/io/File;
    goto :goto_1

    .restart local v2    # "worked":Z
    :cond_5
    move-object v1, p1

    .line 143
    goto :goto_2
.end method

.method private static deleteDirectoryInternal(Landroid/content/Context;Ljava/io/File;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "directory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 188
    :cond_0
    return-void

    .line 183
    :cond_1
    invoke-static {p0, p1}, Lcom/navdy/service/library/util/IOUtils;->cleanDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 184
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to delete directory "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static deleteFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 105
    if-nez p1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v3

    .line 108
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    .local v1, "deleteFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    invoke-static {p0, p1}, Lcom/navdy/service/library/util/IOUtils;->isAppsInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 114
    new-instance v2, Ljava/io/File;

    invoke-static {p0}, Lcom/navdy/service/library/util/IOUtils;->getNewTrashEntryPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v2, "newFile":Ljava/io/File;
    :goto_1
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    .line 121
    .local v0, "b":Z
    sget-object v3, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleted file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v3

    goto :goto_0

    .line 115
    .end local v0    # "b":Z
    .end local v2    # "newFile":Ljava/io/File;
    :cond_2
    invoke-static {p0, p1}, Lcom/navdy/service/library/util/IOUtils;->isAppsExternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 116
    new-instance v2, Ljava/io/File;

    invoke-static {p0}, Lcom/navdy/service/library/util/IOUtils;->getNewExternalTrashEntryPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v2    # "newFile":Ljava/io/File;
    goto :goto_1

    .line 118
    .end local v2    # "newFile":Ljava/io/File;
    :cond_3
    new-instance v2, Ljava/io/File;

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->getTrashEntryPathInSameFolder(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v2    # "newFile":Ljava/io/File;
    goto :goto_1
.end method

.method public static downloadImage(Ljava/lang/String;)[B
    .locals 12
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 652
    const/4 v8, 0x0

    .line 653
    .local v8, "urlConnection":Ljava/net/HttpURLConnection;
    const/4 v3, 0x0

    .line 654
    .local v3, "input":Ljava/io/BufferedInputStream;
    const/4 v6, 0x0

    .line 657
    .local v6, "output":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v9, Ljava/net/URL;

    invoke-direct {v9, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 658
    .local v9, "urlObject":Ljava/net/URL;
    invoke-virtual {v9}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v8, v0

    .line 659
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v4, v10}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 660
    .end local v3    # "input":Ljava/io/BufferedInputStream;
    .local v4, "input":Ljava/io/BufferedInputStream;
    :try_start_1
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 662
    .end local v6    # "output":Ljava/io/ByteArrayOutputStream;
    .local v7, "output":Ljava/io/ByteArrayOutputStream;
    const/16 v10, 0x400

    :try_start_2
    new-array v1, v10, [B

    .line 664
    .local v1, "buf":[B
    :goto_0
    invoke-virtual {v4, v1}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v5

    .local v5, "n":I
    if-ltz v5, :cond_1

    .line 665
    const/4 v10, 0x0

    invoke-virtual {v7, v1, v10, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 669
    .end local v1    # "buf":[B
    .end local v5    # "n":I
    :catch_0
    move-exception v2

    move-object v6, v7

    .end local v7    # "output":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "output":Ljava/io/ByteArrayOutputStream;
    move-object v3, v4

    .line 670
    .end local v4    # "input":Ljava/io/BufferedInputStream;
    .end local v9    # "urlObject":Ljava/net/URL;
    .local v2, "e":Ljava/lang/Throwable;
    .restart local v3    # "input":Ljava/io/BufferedInputStream;
    :goto_1
    :try_start_3
    sget-object v10, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Exception while downloading binary resource"

    invoke-virtual {v10, v11, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 671
    const/4 v10, 0x0

    .line 673
    if-eqz v8, :cond_0

    .line 674
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 676
    :cond_0
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 677
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .end local v2    # "e":Ljava/lang/Throwable;
    :goto_2
    return-object v10

    .line 668
    .end local v3    # "input":Ljava/io/BufferedInputStream;
    .end local v6    # "output":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buf":[B
    .restart local v4    # "input":Ljava/io/BufferedInputStream;
    .restart local v5    # "n":I
    .restart local v7    # "output":Ljava/io/ByteArrayOutputStream;
    .restart local v9    # "urlObject":Ljava/net/URL;
    :cond_1
    :try_start_4
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v10

    .line 673
    if-eqz v8, :cond_2

    .line 674
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 676
    :cond_2
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 677
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v6, v7

    .end local v7    # "output":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "output":Ljava/io/ByteArrayOutputStream;
    move-object v3, v4

    .end local v4    # "input":Ljava/io/BufferedInputStream;
    .restart local v3    # "input":Ljava/io/BufferedInputStream;
    goto :goto_2

    .line 673
    .end local v1    # "buf":[B
    .end local v5    # "n":I
    .end local v9    # "urlObject":Ljava/net/URL;
    :catchall_0
    move-exception v10

    :goto_3
    if-eqz v8, :cond_3

    .line 674
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 676
    :cond_3
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 677
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v10

    .line 673
    .end local v3    # "input":Ljava/io/BufferedInputStream;
    .restart local v4    # "input":Ljava/io/BufferedInputStream;
    .restart local v9    # "urlObject":Ljava/net/URL;
    :catchall_1
    move-exception v10

    move-object v3, v4

    .end local v4    # "input":Ljava/io/BufferedInputStream;
    .restart local v3    # "input":Ljava/io/BufferedInputStream;
    goto :goto_3

    .end local v3    # "input":Ljava/io/BufferedInputStream;
    .end local v6    # "output":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "input":Ljava/io/BufferedInputStream;
    .restart local v7    # "output":Ljava/io/ByteArrayOutputStream;
    :catchall_2
    move-exception v10

    move-object v6, v7

    .end local v7    # "output":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "output":Ljava/io/ByteArrayOutputStream;
    move-object v3, v4

    .end local v4    # "input":Ljava/io/BufferedInputStream;
    .restart local v3    # "input":Ljava/io/BufferedInputStream;
    goto :goto_3

    .line 669
    .end local v9    # "urlObject":Ljava/net/URL;
    :catch_1
    move-exception v2

    goto :goto_1

    .end local v3    # "input":Ljava/io/BufferedInputStream;
    .restart local v4    # "input":Ljava/io/BufferedInputStream;
    .restart local v9    # "urlObject":Ljava/net/URL;
    :catch_2
    move-exception v2

    move-object v3, v4

    .end local v4    # "input":Ljava/io/BufferedInputStream;
    .restart local v3    # "input":Ljava/io/BufferedInputStream;
    goto :goto_1
.end method

.method public static fileSync(Ljava/io/FileOutputStream;)V
    .locals 2
    .param p0, "fileOutputStream"    # Ljava/io/FileOutputStream;

    .prologue
    .line 95
    if-eqz p0, :cond_0

    .line 96
    :try_start_0
    invoke-virtual {p0}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static forceDelete(Landroid/content/Context;Ljava/io/File;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217
    invoke-static {p0, p1}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 220
    .local v0, "worked":Z
    if-nez v0, :cond_0

    .line 221
    sget-object v1, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete kernel crash file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getExternalStorageFolderPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 790
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 791
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 792
    .local v0, "externalStorageDirectory":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 793
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 796
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getFreeSpace(Ljava/lang/String;)J
    .locals 8
    .param p0, "directory"    # Ljava/lang/String;

    .prologue
    .line 621
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 622
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_1

    .line 623
    :cond_0
    const-wide/16 v2, -0x1

    .line 627
    :goto_0
    return-wide v2

    .line 625
    :cond_1
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 626
    .local v1, "statFs":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v6, v6

    mul-long v2, v4, v6

    .line 627
    .local v2, "freeSpace":J
    goto :goto_0
.end method

.method public static getJSONFromURL(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 9
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 631
    const/4 v5, 0x0

    .line 632
    .local v5, "urlConnection":Ljava/net/HttpURLConnection;
    const/4 v3, 0x0

    .line 634
    .local v3, "input":Ljava/io/InputStreamReader;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 635
    .local v6, "urlObject":Ljava/net/URL;
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v5, v0

    .line 636
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    const-string v8, "UTF-8"

    invoke-direct {v4, v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638
    .end local v3    # "input":Ljava/io/InputStreamReader;
    .local v4, "input":Ljava/io/InputStreamReader;
    :try_start_1
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    const-string v8, "UTF-8"

    .line 637
    invoke-static {v7, v8}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 639
    .local v1, "JSONString":Ljava/lang/String;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 644
    if-eqz v5, :cond_0

    .line 645
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 647
    :cond_0
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v3, v4

    .end local v1    # "JSONString":Ljava/lang/String;
    .end local v4    # "input":Ljava/io/InputStreamReader;
    .end local v6    # "urlObject":Ljava/net/URL;
    .restart local v3    # "input":Ljava/io/InputStreamReader;
    :goto_0
    return-object v7

    .line 640
    :catch_0
    move-exception v2

    .line 641
    .local v2, "e":Ljava/lang/Throwable;
    :goto_1
    :try_start_2
    sget-object v7, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Exception while downloading/parsing json"

    invoke-virtual {v7, v8, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 642
    const/4 v7, 0x0

    .line 644
    if-eqz v5, :cond_1

    .line 645
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 647
    :cond_1
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 644
    .end local v2    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v7

    :goto_2
    if-eqz v5, :cond_2

    .line 645
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 647
    :cond_2
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v7

    .line 644
    .end local v3    # "input":Ljava/io/InputStreamReader;
    .restart local v4    # "input":Ljava/io/InputStreamReader;
    .restart local v6    # "urlObject":Ljava/net/URL;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "input":Ljava/io/InputStreamReader;
    .restart local v3    # "input":Ljava/io/InputStreamReader;
    goto :goto_2

    .line 640
    .end local v3    # "input":Ljava/io/InputStreamReader;
    .restart local v4    # "input":Ljava/io/InputStreamReader;
    :catch_1
    move-exception v2

    move-object v3, v4

    .end local v4    # "input":Ljava/io/InputStreamReader;
    .restart local v3    # "input":Ljava/io/InputStreamReader;
    goto :goto_1
.end method

.method public static getNewExternalTrashEntryPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 239
    if-nez p0, :cond_0

    .line 256
    :goto_0
    return-object v2

    .line 243
    :cond_0
    sget-object v4, Lcom/navdy/service/library/util/IOUtils;->sLock:Ljava/lang/Object;

    monitor-enter v4

    .line 244
    :try_start_0
    sget-object v3, Lcom/navdy/service/library/util/IOUtils;->sExternalTrashDir:Ljava/io/File;

    if-nez v3, :cond_1

    .line 245
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 246
    .local v1, "externalFilesDir":Ljava/io/File;
    if-eqz v1, :cond_2

    .line 247
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".trash"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/navdy/service/library/util/IOUtils;->sExternalTrashDir:Ljava/io/File;

    .line 249
    sget-object v3, Lcom/navdy/service/library/util/IOUtils;->sExternalTrashDir:Ljava/io/File;

    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    .line 254
    .end local v0    # "absolutePath":Ljava/lang/String;
    .end local v1    # "externalFilesDir":Ljava/io/File;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/navdy/service/library/util/IOUtils;->sExternalTrashDir:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "_"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/navdy/service/library/util/IOUtils;->sCounter:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 255
    .local v2, "newExternalTrashEntryPath":Ljava/lang/String;
    monitor-exit v4

    goto :goto_0

    .end local v2    # "newExternalTrashEntryPath":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 251
    .restart local v1    # "externalFilesDir":Ljava/io/File;
    :cond_2
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private static getNewTrashEntryPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 227
    sget-object v0, Lcom/navdy/service/library/util/IOUtils;->sTrashDir:Ljava/io/File;

    if-nez v0, :cond_1

    .line 228
    sget-object v1, Lcom/navdy/service/library/util/IOUtils;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 229
    :try_start_0
    sget-object v0, Lcom/navdy/service/library/util/IOUtils;->sTrashDir:Ljava/io/File;

    if-nez v0, :cond_0

    .line 230
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".trash"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/service/library/util/IOUtils;->sTrashDir:Ljava/io/File;

    .line 231
    sget-object v0, Lcom/navdy/service/library/util/IOUtils;->sTrashDir:Ljava/io/File;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    .line 233
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/navdy/service/library/util/IOUtils;->sTrashDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/util/IOUtils;->sCounter:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 233
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getSocketFD(Landroid/bluetooth/BluetoothSocket;)I
    .locals 7
    .param p0, "bluetoothSocket"    # Landroid/bluetooth/BluetoothSocket;

    .prologue
    .line 321
    if-eqz p0, :cond_0

    .line 322
    :try_start_0
    const-class v5, Landroid/bluetooth/BluetoothSocket;

    const-string v6, "mSocket"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 323
    .local v0, "field":Ljava/lang/reflect/Field;
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 324
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/LocalSocket;

    .line 325
    .local v2, "localSocket":Landroid/net/LocalSocket;
    if-eqz v2, :cond_0

    .line 326
    const-class v5, Landroid/net/LocalSocket;

    const-string v6, "impl"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 327
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 328
    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 329
    .local v3, "localSocketImpl":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 330
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "fd"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 331
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 332
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/FileDescriptor;

    .line 333
    .local v1, "fileDescriptor":Ljava/io/FileDescriptor;
    if-eqz v1, :cond_0

    .line 334
    const-class v5, Ljava/io/FileDescriptor;

    const-string v6, "descriptor"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 335
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 336
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 344
    .end local v0    # "field":Ljava/lang/reflect/Field;
    .end local v1    # "fileDescriptor":Ljava/io/FileDescriptor;
    .end local v2    # "localSocket":Landroid/net/LocalSocket;
    .end local v3    # "localSocketImpl":Ljava/lang/Object;
    :goto_0
    return v5

    .line 341
    :catch_0
    move-exception v4

    .line 342
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 344
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_0
    const/4 v5, -0x1

    goto :goto_0
.end method

.method public static getTempFile(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)Ljava/io/File;
    .locals 6
    .param p0, "logger"    # Lcom/navdy/service/library/log/Logger;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 751
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mounted"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 754
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 755
    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-static {}, Lcom/navdy/service/library/util/IOUtils;->getTempFilename()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 760
    .local v2, "file":Ljava/io/File;
    :goto_0
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v0

    .line 761
    .local v0, "created":Z
    if-eqz v0, :cond_0

    .line 762
    if-eqz p0, :cond_2

    .line 763
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Created "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 786
    .end local v0    # "created":Z
    .end local v2    # "file":Ljava/io/File;
    :cond_0
    :goto_1
    return-object v2

    .line 757
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v2    # "file":Ljava/io/File;
    goto :goto_0

    .line 766
    .restart local v0    # "created":Z
    :cond_2
    :try_start_1
    sget-object v3, Lcom/navdy/service/library/util/IOUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Created "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 769
    .end local v0    # "created":Z
    :catch_0
    move-exception v1

    .line 770
    .local v1, "e":Ljava/io/IOException;
    if-eqz p0, :cond_3

    .line 771
    const-string v3, "FileUtils:: Unable to create file. "

    invoke-virtual {p0, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 774
    :cond_3
    sget-object v3, Lcom/navdy/service/library/util/IOUtils;->TAG:Ljava/lang/String;

    const-string v4, "FileUtils:: Unable to create file. "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 779
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "file":Ljava/io/File;
    :cond_4
    if-eqz p0, :cond_5

    .line 780
    const-string v3, "FileUtils:: External Storage not mounted!"

    invoke-virtual {p0, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 786
    :goto_2
    const/4 v2, 0x0

    goto :goto_1

    .line 783
    :cond_5
    sget-object v3, Lcom/navdy/service/library/util/IOUtils;->TAG:Ljava/lang/String;

    const-string v4, "FileUtils:: External Storage not mounted!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static getTempFilename()Ljava/lang/String;
    .locals 4

    .prologue
    .line 805
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd\'_\'HH:mm:ss.SSS"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 806
    .local v1, "photoDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 807
    .local v0, "currentTime":Ljava/util/Date;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getTrashEntryPathInSameFolder(Ljava/io/File;)Ljava/lang/String;
    .locals 4
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 260
    if-eqz p0, :cond_0

    .line 261
    invoke-virtual {p0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "folder":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/util/IOUtils;->sCounter:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 264
    .end local v0    # "folder":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static hashForBitmap(Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 418
    if-eqz p0, :cond_0

    .line 420
    :try_start_0
    invoke-static {p0}, Lcom/navdy/service/library/util/IOUtils;->bitmap2ByteBuffer(Landroid/graphics/Bitmap;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->hashForBytes([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 425
    :cond_0
    :goto_0
    return-object v1

    .line 421
    :catch_0
    move-exception v0

    .line 422
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    goto :goto_0
.end method

.method public static hashForBytes([B)Ljava/lang/String;
    .locals 3
    .param p0, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 430
    if-eqz p0, :cond_0

    array-length v2, p0

    if-gtz v2, :cond_2

    .line 431
    :cond_0
    const/4 v0, 0x0

    .line 440
    :cond_1
    :goto_0
    return-object v0

    .line 434
    :cond_2
    const/4 v0, 0x0

    .line 435
    .local v0, "hash":Ljava/lang/String;
    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 436
    .local v1, "mDigest":Ljava/security/MessageDigest;
    if-eqz v1, :cond_1

    .line 437
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->update([B)V

    .line 438
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static hashForFile(Ljava/io/File;)Ljava/lang/String;
    .locals 2
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 444
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/navdy/service/library/util/IOUtils;->hashForFile(Ljava/io/File;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hashForFile(Ljava/io/File;J)Ljava/lang/String;
    .locals 19
    .param p0, "file"    # Ljava/io/File;
    .param p1, "offset"    # J

    .prologue
    .line 448
    sget-object v12, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v12}, Lcom/navdy/service/library/log/Logger;->recordStartTime()V

    .line 449
    const/4 v6, 0x0

    .line 450
    .local v6, "fis":Ljava/io/FileInputStream;
    if-eqz p0, :cond_0

    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->canRead()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->length()J

    move-result-wide v12

    cmp-long v12, p1, v12

    if-lez v12, :cond_1

    .line 451
    :cond_0
    new-instance v12, Ljava/lang/IllegalArgumentException;

    invoke-direct {v12}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v12

    .line 454
    :cond_1
    :try_start_0
    const-string v12, "MD5"

    invoke-static {v12}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v8

    .line 455
    .local v8, "mDigest":Ljava/security/MessageDigest;
    const-wide/16 v10, 0x0

    .line 456
    .local v10, "totalRead":J
    const/high16 v3, 0x100000

    .line 457
    .local v3, "bufferSize":I
    new-array v2, v3, [B

    .line 458
    .local v2, "buffer":[B
    new-instance v7, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 459
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .local v7, "fis":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    .line 461
    .local v4, "bytesRead":I
    :cond_2
    const/4 v12, 0x0

    sub-long v14, p1, v10

    int-to-long v0, v3

    move-wide/from16 v16, v0

    :try_start_1
    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    long-to-int v13, v14

    invoke-virtual {v7, v2, v12, v13}, Ljava/io/FileInputStream;->read([BII)I

    move-result v4

    .line 462
    const/4 v12, 0x0

    invoke-virtual {v8, v2, v12, v4}, Ljava/security/MessageDigest;->update([BII)V

    .line 463
    int-to-long v12, v4

    add-long/2addr v10, v12

    .line 464
    cmp-long v12, v10, p1

    if-gez v12, :cond_3

    const/4 v12, -0x1

    if-ne v4, v12, :cond_2

    .line 465
    :cond_3
    invoke-virtual {v8}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v12

    invoke-static {v12}, Lcom/navdy/service/library/util/IOUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v5

    .line 466
    .local v5, "digest":Ljava/lang/String;
    sget-object v12, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Time taken to calculate hash of the file of size :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->length()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->logTimeTaken(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 471
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v6, v7

    .line 473
    .end local v2    # "buffer":[B
    .end local v3    # "bufferSize":I
    .end local v4    # "bytesRead":I
    .end local v5    # "digest":Ljava/lang/String;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "mDigest":Ljava/security/MessageDigest;
    .end local v10    # "totalRead":J
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :goto_0
    return-object v5

    .line 468
    :catch_0
    move-exception v9

    .line 469
    .local v9, "t":Ljava/lang/Throwable;
    :goto_1
    :try_start_2
    sget-object v12, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v13, "Exception while calculating md5 checksum for the file "

    invoke-virtual {v12, v13, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 471
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 473
    const/4 v5, 0x0

    goto :goto_0

    .line 471
    .end local v9    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v12

    :goto_2
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v12

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "buffer":[B
    .restart local v3    # "bufferSize":I
    .restart local v4    # "bytesRead":I
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "mDigest":Ljava/security/MessageDigest;
    .restart local v10    # "totalRead":J
    :catchall_1
    move-exception v12

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 468
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v9

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static hashForKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 407
    if-eqz p0, :cond_0

    .line 409
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->hashForBytes([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 414
    :goto_0
    return-object v1

    .line 410
    :catch_0
    move-exception v0

    .line 411
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 414
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static varargs hashForPath(Ljava/lang/String;Z[Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "recursive"    # Z
    .param p2, "excludeFileNames"    # [Ljava/lang/String;

    .prologue
    .line 477
    sget-object v5, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5}, Lcom/navdy/service/library/log/Logger;->recordStartTime()V

    .line 480
    :try_start_0
    const-string v5, "MD5"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 481
    .local v3, "mDigest":Ljava/security/MessageDigest;
    const/high16 v1, 0x100000

    .line 482
    .local v1, "bufferSize":I
    const/high16 v5, 0x100000

    new-array v0, v5, [B

    .line 484
    .local v0, "buffer":[B
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-instance v6, Lcom/navdy/service/library/util/IOUtils$1;

    invoke-direct {v6, v0, v3}, Lcom/navdy/service/library/util/IOUtils$1;-><init>([BLjava/security/MessageDigest;)V

    invoke-static {p0, p1, v5, v6}, Lcom/navdy/service/library/util/IOUtils;->traverseFiles(Ljava/lang/String;ZLjava/util/List;Lcom/navdy/service/library/util/IOUtils$OnFileTraversal;)V

    .line 508
    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v2

    .line 509
    .local v2, "digest":Ljava/lang/String;
    sget-object v5, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Time taken to calculate hash of all files on path:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->logTimeTaken(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 514
    .end local v0    # "buffer":[B
    .end local v1    # "bufferSize":I
    .end local v2    # "digest":Ljava/lang/String;
    .end local v3    # "mDigest":Ljava/security/MessageDigest;
    :goto_0
    return-object v2

    .line 511
    :catch_0
    move-exception v4

    .line 512
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/service/library/util/IOUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Exception while calculating md5 checksum for a path "

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 514
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isAppsExternalFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 176
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isAppsInternalFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 172
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isExternalStorageReadable()Z
    .locals 2

    .prologue
    .line 739
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 740
    .local v0, "state":Ljava/lang/String;
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static readBinaryFile(Ljava/lang/String;)[B
    .locals 6
    .param p0, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 298
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v3, v4

    new-array v0, v3, [B

    .line 299
    .local v0, "buffer":[B
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 301
    .local v2, "ios":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    throw v3
.end method

.method public static readInputStreamToByteArray(Ljava/io/InputStream;)[B
    .locals 4
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 287
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 288
    .local v1, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x4000

    new-array v0, v3, [B

    .line 290
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "n":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 291
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 293
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method private static traverseFiles(Ljava/lang/String;ZLjava/util/List;Lcom/navdy/service/library/util/IOUtils$OnFileTraversal;)V
    .locals 7
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "recursive"    # Z
    .param p3, "onFileTraversal"    # Lcom/navdy/service/library/util/IOUtils$OnFileTraversal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/navdy/service/library/util/IOUtils$OnFileTraversal;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 518
    .local p2, "excludeFileNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 520
    .local v0, "containingDirectory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_1

    .line 536
    :cond_0
    return-void

    .line 523
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 524
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 527
    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 529
    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v2, v3

    .line 530
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 531
    invoke-interface {p3, v1}, Lcom/navdy/service/library/util/IOUtils$OnFileTraversal;->onFileTraversal(Ljava/io/File;)V

    .line 529
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 532
    :cond_3
    if-eqz p1, :cond_2

    .line 533
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v5, v6, p2, p3}, Lcom/navdy/service/library/util/IOUtils;->traverseFiles(Ljava/lang/String;ZLjava/util/List;Lcom/navdy/service/library/util/IOUtils$OnFileTraversal;)V

    goto :goto_1
.end method
