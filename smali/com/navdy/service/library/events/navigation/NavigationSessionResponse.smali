.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;
.super Lcom/squareup/wire/Message;
.source "NavigationSessionResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_PENDINGSESSIONSTATE:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final DEFAULT_ROUTEID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUSDETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final routeId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final statusDetail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 18
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->DEFAULT_PENDINGSESSIONSTATE:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "statusDetail"    # Ljava/lang/String;
    .param p3, "pendingSessionState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .param p4, "routeId"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 47
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->statusDetail:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 49
    iput-object p4, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->routeId:Ljava/lang/String;

    .line 50
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;

    .prologue
    .line 53
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->statusDetail:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->routeId:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 55
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 61
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    .line 62
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->statusDetail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->statusDetail:Ljava/lang/String;

    .line 63
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 64
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->routeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->routeId:Ljava/lang/String;

    .line 65
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 70
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->hashCode:I

    .line 71
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 72
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v0

    .line 73
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->statusDetail:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 74
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 75
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->routeId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->routeId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 76
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->hashCode:I

    .line 78
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 72
    goto :goto_0

    :cond_3
    move v2, v1

    .line 73
    goto :goto_1

    :cond_4
    move v2, v1

    .line 74
    goto :goto_2
.end method
