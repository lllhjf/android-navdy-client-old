.class public final Lcom/navdy/service/library/events/file/FileTransferRequest;
.super Lcom/squareup/wire/Message;
.source "FileTransferRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DESTINATIONFILENAME:Ljava/lang/String; = ""

.field public static final DEFAULT_FILEDATACHECKSUM:Ljava/lang/String; = ""

.field public static final DEFAULT_FILESIZE:Ljava/lang/Long;

.field public static final DEFAULT_FILETYPE:Lcom/navdy/service/library/events/file/FileType;

.field public static final DEFAULT_OFFSET:Ljava/lang/Long;

.field public static final DEFAULT_OVERRIDE:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORTSACKS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final destinationFileName:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final fileDataChecksum:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final fileSize:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final fileType:Lcom/navdy/service/library/events/file/FileType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final offset:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final override:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final supportsAcks:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 20
    sget-object v0, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->DEFAULT_FILETYPE:Lcom/navdy/service/library/events/file/FileType;

    .line 22
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->DEFAULT_FILESIZE:Ljava/lang/Long;

    .line 23
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->DEFAULT_OFFSET:Ljava/lang/Long;

    .line 25
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->DEFAULT_OVERRIDE:Ljava/lang/Boolean;

    .line 26
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->DEFAULT_SUPPORTSACKS:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;)V
    .locals 8
    .param p1, "builder"    # Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    .prologue
    .line 90
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileType:Lcom/navdy/service/library/events/file/FileType;

    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->destinationFileName:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileSize:Ljava/lang/Long;

    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->offset:Ljava/lang/Long;

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileDataChecksum:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->override:Ljava/lang/Boolean;

    iget-object v7, p1, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->supportsAcks:Ljava/lang/Boolean;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/events/file/FileTransferRequest;-><init>(Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 91
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/file/FileTransferRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 92
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;Lcom/navdy/service/library/events/file/FileTransferRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/file/FileTransferRequest$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/file/FileTransferRequest;-><init>(Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "fileType"    # Lcom/navdy/service/library/events/file/FileType;
    .param p2, "destinationFileName"    # Ljava/lang/String;
    .param p3, "fileSize"    # Ljava/lang/Long;
    .param p4, "offset"    # Ljava/lang/Long;
    .param p5, "fileDataChecksum"    # Ljava/lang/String;
    .param p6, "override"    # Ljava/lang/Boolean;
    .param p7, "supportsAcks"    # Ljava/lang/Boolean;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    .line 81
    iput-object p2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    .line 82
    iput-object p3, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileSize:Ljava/lang/Long;

    .line 83
    iput-object p4, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->offset:Ljava/lang/Long;

    .line 84
    iput-object p5, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileDataChecksum:Ljava/lang/String;

    .line 85
    iput-object p6, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->override:Ljava/lang/Boolean;

    .line 86
    iput-object p7, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->supportsAcks:Ljava/lang/Boolean;

    .line 87
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 96
    if-ne p1, p0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v1

    .line 97
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 98
    check-cast v0, Lcom/navdy/service/library/events/file/FileTransferRequest;

    .line 99
    .local v0, "o":Lcom/navdy/service/library/events/file/FileTransferRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    .line 100
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileSize:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileSize:Ljava/lang/Long;

    .line 101
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->offset:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->offset:Ljava/lang/Long;

    .line 102
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileDataChecksum:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileDataChecksum:Ljava/lang/String;

    .line 103
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->override:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->override:Ljava/lang/Boolean;

    .line 104
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->supportsAcks:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->supportsAcks:Ljava/lang/Boolean;

    .line 105
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 110
    iget v0, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->hashCode:I

    .line 111
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 112
    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileType;->hashCode()I

    move-result v0

    .line 113
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 114
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileSize:Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileSize:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 115
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->offset:Ljava/lang/Long;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->offset:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 116
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileDataChecksum:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileDataChecksum:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 117
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->override:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->override:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 118
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->supportsAcks:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->supportsAcks:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 119
    iput v0, p0, Lcom/navdy/service/library/events/file/FileTransferRequest;->hashCode:I

    .line 121
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 112
    goto :goto_0

    :cond_3
    move v2, v1

    .line 113
    goto :goto_1

    :cond_4
    move v2, v1

    .line 114
    goto :goto_2

    :cond_5
    move v2, v1

    .line 115
    goto :goto_3

    :cond_6
    move v2, v1

    .line 116
    goto :goto_4

    :cond_7
    move v2, v1

    .line 117
    goto :goto_5
.end method
