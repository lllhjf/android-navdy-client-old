.class public final Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;
.super Lcom/squareup/wire/Message;
.source "NavigationPreferencesUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUSDETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
    .end annotation
.end field

.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final statusDetail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 28
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "statusDetail"    # Ljava/lang/String;
    .param p3, "serial_number"    # Ljava/lang/Long;
    .param p4, "preferences"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 56
    iput-object p2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->statusDetail:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->serial_number:Ljava/lang/Long;

    .line 58
    iput-object p4, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .line 59
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;

    .prologue
    .line 62
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->statusDetail:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    iget-object v3, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V

    .line 63
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$1;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;-><init>(Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 68
    if-ne p1, p0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v1

    .line 69
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 70
    check-cast v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    .line 71
    .local v0, "o":Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->statusDetail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->statusDetail:Ljava/lang/String;

    .line 72
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->serial_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->serial_number:Ljava/lang/Long;

    .line 73
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .line 74
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 79
    iget v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->hashCode:I

    .line 80
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 81
    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v0

    .line 82
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->statusDetail:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->statusDetail:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 83
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->serial_number:Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->serial_number:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 84
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 85
    iput v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->hashCode:I

    .line 87
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 81
    goto :goto_0

    :cond_3
    move v2, v1

    .line 82
    goto :goto_1

    :cond_4
    move v2, v1

    .line 83
    goto :goto_2
.end method
