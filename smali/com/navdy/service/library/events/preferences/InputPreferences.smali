.class public final Lcom/navdy/service/library/events/preferences/InputPreferences;
.super Lcom/squareup/wire/Message;
.source "InputPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;,
        Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DIAL_SENSITIVITY:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field public static final DEFAULT_USE_GESTURES:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final use_gestures:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/InputPreferences;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    .line 31
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/InputPreferences;->DEFAULT_USE_GESTURES:Ljava/lang/Boolean;

    .line 32
    sget-object v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->DIAL_SENSITIVITY_STANDARD:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    sput-object v0, Lcom/navdy/service/library/events/preferences/InputPreferences;->DEFAULT_DIAL_SENSITIVITY:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;

    .prologue
    .line 60
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->serial_number:Ljava/lang/Long;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->use_gestures:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/preferences/InputPreferences;-><init>(Ljava/lang/Long;Ljava/lang/Boolean;Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;)V

    .line 61
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/preferences/InputPreferences;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 62
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;Lcom/navdy/service/library/events/preferences/InputPreferences$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/preferences/InputPreferences$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/preferences/InputPreferences;-><init>(Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Boolean;Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;)V
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;
    .param p2, "use_gestures"    # Ljava/lang/Boolean;
    .param p3, "dial_sensitivity"    # Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    .line 55
    iput-object p2, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    .line 56
    iput-object p3, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    .line 57
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    if-ne p1, p0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 67
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/preferences/InputPreferences;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 68
    check-cast v0, Lcom/navdy/service/library/events/preferences/InputPreferences;

    .line 69
    .local v0, "o":Lcom/navdy/service/library/events/preferences/InputPreferences;
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/InputPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    .line 70
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/InputPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/InputPreferences;->dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    .line 71
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/InputPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 76
    iget v0, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->hashCode:I

    .line 77
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 78
    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 79
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 80
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 81
    iput v0, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->hashCode:I

    .line 83
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 78
    goto :goto_0

    :cond_3
    move v2, v1

    .line 79
    goto :goto_1
.end method
