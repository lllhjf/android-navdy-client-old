.class public final Lcom/navdy/service/library/events/audio/MusicCollectionResponse;
.super Lcom/squareup/wire/Message;
.source "MusicCollectionResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CHARACTERMAP:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MUSICCOLLECTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MUSICTRACKS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final characterMap:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/audio/MusicCharacterMap;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
            ">;"
        }
    .end annotation
.end field

.field public final collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
    .end annotation
.end field

.field public final musicCollections:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final musicTracks:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/audio/MusicTrackInfo;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->DEFAULT_MUSICCOLLECTIONS:Ljava/util/List;

    .line 16
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->DEFAULT_MUSICTRACKS:Ljava/util/List;

    .line 17
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->DEFAULT_CHARACTERMAP:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "collectionInfo"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p2, "musicCollections":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .local p3, "musicTracks":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .local p4, "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 48
    invoke-static {p2}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    .line 49
    invoke-static {p3}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    .line 50
    invoke-static {p4}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    .line 51
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;

    .prologue
    .line 54
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->musicCollections:Ljava/util/List;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->musicTracks:Ljava/util/List;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->characterMap:Ljava/util/List;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 55
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 56
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;Lcom/navdy/service/library/events/audio/MusicCollectionResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicCollectionResponse$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;)V

    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 12
    invoke-static {p0}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 12
    invoke-static {p0}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 12
    invoke-static {p0}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    if-ne p1, p0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v1

    .line 61
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 62
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    .line 63
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicCollectionResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    .line 64
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    .line 65
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    .line 66
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 71
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->hashCode:I

    .line 72
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 73
    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->hashCode()I

    move-result v0

    .line 74
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v3, v1

    .line 75
    mul-int/lit8 v3, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v3, v1

    .line 76
    mul-int/lit8 v1, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    .line 77
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->hashCode:I

    .line 79
    :cond_1
    return v0

    .line 73
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v1, v2

    .line 74
    goto :goto_1

    :cond_4
    move v1, v2

    .line 75
    goto :goto_2
.end method
