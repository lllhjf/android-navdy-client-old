.class public final Lcom/navdy/service/library/events/audio/SpeechRequestStatus;
.super Lcom/squareup/wire/Message;
.source "SpeechRequestStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;,
        Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->SPEECH_REQUEST_STARTING:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->DEFAULT_STATUS:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;

    .prologue
    .line 39
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;)V

    .line 40
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;Lcom/navdy/service/library/events/audio/SpeechRequestStatus$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/SpeechRequestStatus$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;-><init>(Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "status"    # Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->id:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    if-ne p1, p0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v1

    .line 46
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 47
    check-cast v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;

    .line 48
    .local v0, "o":Lcom/navdy/service/library/events/audio/SpeechRequestStatus;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->id:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    .line 49
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54
    iget v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->hashCode:I

    .line 55
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 56
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->id:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 57
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 58
    iput v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->hashCode:I

    .line 60
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 56
    goto :goto_0
.end method
