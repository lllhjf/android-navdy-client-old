.class public Lcom/navdy/service/library/device/connection/TCPConnectionInfo;
.super Lcom/navdy/service/library/device/connection/ConnectionInfo;
.source "TCPConnectionInfo.java"


# static fields
.field public static final FORMAT_SERVICE_NAME_WITH_DEVICE_ID:Ljava/lang/String; = "Navdy %s"

.field public static final SERVICE_PORT:I = 0x5335

.field public static final SERVICE_TYPE:Ljava/lang/String; = "_navdybus._tcp."


# instance fields
.field mHostName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "address"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 3
    .param p1, "serviceInfo"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;-><init>(Landroid/net/nsd/NsdServiceInfo;)V

    .line 41
    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getHost()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "hostName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 43
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "hostname not found in service record"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 45
    :cond_0
    iput-object v0, p0, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;->mHostName:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/device/NavdyDeviceId;Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "hostName"    # Ljava/lang/String;

    .prologue
    .line 29
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {p0, p1, v0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    .line 31
    if-nez p2, :cond_0

    .line 32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "hostname required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;->mHostName:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static nsdServiceWithDeviceId(Lcom/navdy/service/library/device/NavdyDeviceId;)Landroid/net/nsd/NsdServiceInfo;
    .locals 5
    .param p0, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 20
    new-instance v0, Landroid/net/nsd/NsdServiceInfo;

    invoke-direct {v0}, Landroid/net/nsd/NsdServiceInfo;-><init>()V

    .line 21
    .local v0, "serviceInfo":Landroid/net/nsd/NsdServiceInfo;
    const-string v1, "Navdy %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/nsd/NsdServiceInfo;->setServiceName(Ljava/lang/String;)V

    .line 22
    const-string v1, "_navdybus._tcp."

    invoke-virtual {v0, v1}, Landroid/net/nsd/NsdServiceInfo;->setServiceType(Ljava/lang/String;)V

    .line 23
    const/16 v1, 0x5335

    invoke-virtual {v0, v1}, Landroid/net/nsd/NsdServiceInfo;->setPort(I)V

    .line 25
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 54
    if-ne p0, p1, :cond_1

    const/4 v1, 0x1

    .line 60
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 56
    invoke-super {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 58
    check-cast v0, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;

    .line 60
    .local v0, "that":Lcom/navdy/service/library/device/connection/TCPConnectionInfo;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;->mHostName:Ljava/lang/String;

    iget-object v2, v0, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;->mHostName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAddress()Lcom/navdy/service/library/device/connection/ServiceAddress;
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lcom/navdy/service/library/device/connection/ServiceAddress;

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;->mHostName:Ljava/lang/String;

    const/16 v2, 0x5335

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ServiceAddress;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 65
    invoke-super {p0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->hashCode()I

    move-result v0

    .line 66
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;->mHostName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 67
    return v0
.end method
