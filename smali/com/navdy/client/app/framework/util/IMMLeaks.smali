.class public Lcom/navdy/client/app/framework/util/IMMLeaks;
.super Ljava/lang/Object;
.source "IMMLeaks.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/IMMLeaks;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/IMMLeaks;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/navdy/client/app/framework/util/IMMLeaks;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static fixFocusedViewLeak(Landroid/app/Application;)V
    .locals 3
    .param p0, "application"    # Landroid/app/Application;

    .prologue
    .line 20
    :try_start_0
    new-instance v1, Lcom/navdy/client/app/framework/util/IMMLeaks$1;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/util/IMMLeaks$1;-><init>()V

    invoke-virtual {p0, v1}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 59
    sget-object v1, Lcom/navdy/client/app/framework/util/IMMLeaks;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "IMM installed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/util/IMMLeaks;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "IMMLeaks"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
