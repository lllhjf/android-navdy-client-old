.class public Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;
.super Ljava/lang/Object;
.source "TripUpdateServiceHandler.java"


# static fields
.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static singleton:Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;Lcom/navdy/service/library/events/TripUpdate;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/TripUpdate;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->handleTripUpdate(Lcom/navdy/service/library/events/TripUpdate;)V

    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;

    .line 45
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;

    return-object v0
.end method

.method private handleTripUpdate(Lcom/navdy/service/library/events/TripUpdate;)V
    .locals 12
    .param p1, "tripUpdate"    # Lcom/navdy/service/library/events/TripUpdate;

    .prologue
    .line 59
    if-eqz p1, :cond_1

    .line 60
    iget-object v7, p1, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v7, :cond_0

    .line 61
    iget-object v7, p1, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    .line 62
    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iget-object v7, p1, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    .line 63
    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    .line 61
    invoke-static {v8, v9, v10, v11}, Lcom/navdy/client/app/framework/map/MapUtils;->buildNewCoordinate(DD)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    .line 65
    .local v1, "carLocation":Lcom/navdy/service/library/events/location/Coordinate;
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v7

    new-instance v8, Lcom/navdy/client/app/framework/location/NavdyLocationManager$CarLocationChangedEvent;

    invoke-direct {v8, v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager$CarLocationChangedEvent;-><init>(Lcom/navdy/service/library/events/location/Coordinate;)V

    invoke-virtual {v7, v8}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 69
    .end local v1    # "carLocation":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_0
    iget-object v7, p1, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    if-eqz v7, :cond_1

    .line 70
    iget-object v7, p1, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    .line 71
    .local v6, "tripNumber":Ljava/lang/String;
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "TripUpdate received, trip number: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 72
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 74
    .local v0, "appContext":Landroid/content/Context;
    :try_start_0
    invoke-static {v6}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisTrip(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Trip;

    move-result-object v5

    .line 76
    .local v5, "trip":Lcom/navdy/client/app/framework/models/Trip;
    if-nez v5, :cond_2

    .line 78
    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/models/Trip;->saveStartingTripUpdate(Landroid/content/Context;Lcom/navdy/service/library/events/TripUpdate;)Landroid/net/Uri;

    move-result-object v3

    .line 79
    .local v3, "insertedEntry":Landroid/net/Uri;
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "The following trip was inserted: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 90
    .end local v0    # "appContext":Landroid/content/Context;
    .end local v3    # "insertedEntry":Landroid/net/Uri;
    .end local v5    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    .end local v6    # "tripNumber":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 82
    .restart local v0    # "appContext":Landroid/content/Context;
    .restart local v5    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    .restart local v6    # "tripNumber":Ljava/lang/String;
    :cond_2
    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/models/Trip;->saveLastTripUpdate(Landroid/content/Context;Lcom/navdy/service/library/events/TripUpdate;)I

    move-result v4

    .line 83
    .local v4, "nbUpdate":I
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Updated this many trips: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 85
    .end local v4    # "nbUpdate":I
    .end local v5    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :catch_0
    move-exception v2

    .line 86
    .local v2, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Something went wrong while trying to save trip info: "

    invoke-virtual {v7, v8, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public onTripUpdate(Lcom/navdy/service/library/events/TripUpdate;)V
    .locals 3
    .param p1, "tripUpdate"    # Lcom/navdy/service/library/events/TripUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 50
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;Lcom/navdy/service/library/events/TripUpdate;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 56
    return-void
.end method
